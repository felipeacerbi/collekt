## Collekt

The Collekt application allows everyone who's got a collection to interact, discover and find others or simply use it for yourself.

## Main Features

- Stores collections.
- Creates collection items with pictures, categories and descriptions.
- Allows to view, react and comment other users� collections and items.
- Provides a widget with a list of the collection items.

## Contact 

Owner: Felipe Acerbi

Email: feaacerbi@gmail.com 

Playstore: https://play.google.com/store/apps/details?id=br.com.felipeacerbi.collekt