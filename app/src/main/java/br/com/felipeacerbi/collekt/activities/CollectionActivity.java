package br.com.felipeacerbi.collekt.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.base.ActionModeActivity;
import br.com.felipeacerbi.collekt.adapters.CollectionItemsAdapter;
import br.com.felipeacerbi.collekt.adapters.UsersListAdapter;
import br.com.felipeacerbi.collekt.listeners.ActionModeProvider;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnCollectionItemClickListener;
import br.com.felipeacerbi.collekt.listeners.OnUserClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.models.CollectionItemsArrayList;
import br.com.felipeacerbi.collekt.models.FilterCollectionItems;
import br.com.felipeacerbi.collekt.models.FilterOrigin;
import br.com.felipeacerbi.collekt.models.Report;
import br.com.felipeacerbi.collekt.models.User;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.CalendarUtils;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import br.com.felipeacerbi.collekt.utils.NumberUtils;
import br.com.felipeacerbi.collekt.utils.SharedPrefsUtils;
import br.com.felipeacerbi.collekt.widget.CollectionWidget;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;

public class CollectionActivity extends ActionModeActivity implements OnCollectionItemClickListener, View.OnClickListener, ActionModeProvider, EmptyIconClickListener, SwipeRefreshLayout.OnRefreshListener, OnUserClickListener {

    public static final String COLLECTION_EXTRA = "collection";
    public static final int NEW_COLLECTION_ITEM = 0;
    private static final int RC_PHOTO_PICKER = 1;

    private static final int VIEW_LINEAR = 0;
    private static final int VIEW_GRID_2 = 1;
    private static final int VIEW_GRID_3 = 2;
    private int viewMode;

    private static final int OPTION_OFF = 0;
    private static final int OPTION_SHARE = 1;
    private static final int OPTION_COMPLETE = 2;
    private static final int OPTION_SORT = 3;
    private int optionsMode = OPTION_OFF;

    @BindView(R.id.nested_view)
    NestedScrollView nestedView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_created)
    TextView createdSubtitle;

    @BindView(R.id.sw_share)
    SwitchCompat privacySwitch;
    @BindView(R.id.tv_share_text)
    TextView shareText;

    @BindView(R.id.cl_first_toolbar)
    ConstraintLayout ownerToolbar;
    @BindView(R.id.cl_third_toolbar)
    ConstraintLayout optionsToolbar;
    @BindView(R.id.cl_share_options)
    ConstraintLayout shareOptions;
    @BindView(R.id.cl_sort_options)
    ConstraintLayout sortOptions;

    @BindView(R.id.civ_profile_picture)
    ImageView profilePicture;
    @BindView(R.id.tv_profile_name)
    TextView profileName;

    @BindView(R.id.iv_view)
    ImageView viewButton;
    @BindView(R.id.iv_public)
    ImageView privacyButton;
    @BindView(R.id.iv_report_icon)
    ImageView reportIcon;
    @BindView(R.id.iv_complete)
    ImageView completeButton;
    @BindView(R.id.iv_sort)
    ImageView sortButton;
    @BindView(R.id.tv_items_number)
    TextView itemsNumber;
    @BindView(R.id.tv_applied_filters)
    TextView appliedFilters;
    @BindView(R.id.tv_applied_filters_back)
    ImageView appliedFiltersBack;

    @BindView(R.id.tv_origin)
    TextView originOption;
    @BindView(R.id.sp_origin)
    ImageView originSpinner;
    @BindView(R.id.tv_sort)
    TextView sortOption;
    @BindView(R.id.sp_sort)
    ImageView sortSpinner;
    @BindView(R.id.tv_rarity)
    TextView rarityOption;
    @BindView(R.id.sp_rarity)
    ImageView raritySpinner;
    @BindView(R.id.tv_value)
    TextView valueOption;
    @BindView(R.id.sp_value)
    ImageView valueSpinner;
    @BindView(R.id.tv_clear)
    TextView clearButton;
    @BindView(R.id.tv_apply)
    TextView applyButton;

    @BindView(R.id.iv_cover_photo)
    ImageView coverPhoto;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.list)
    RecyclerView itemsList;

    @BindView(R.id.iv_edit_photo)
    ImageView editPhoto;
    @BindView(R.id.iv_edit_circle)
    ImageView editCircle;
    @BindView(R.id.et_title_edit)
    EditText editTitle;
    @BindView(R.id.tv_title)
    TextView titleView;
    @BindView(R.id.pb_progress)
    ProgressBar indefProgress;
    @BindView(R.id.cpb_progress)
    CircularProgressBar progressBar;

    @BindView(R.id.cl_empty)
    ConstraintLayout emptyView;
    @BindView(R.id.tv_empty2)
    TextView emptyMessage;
    @BindView(R.id.iv_empty)
    ImageView emptyIcon;

    @State(CollectionItemsArrayList.class)
    CollectionItemsArrayList mItems;
    @State(Collection.class)
    Collection currentCollection;
    @State(User.class)
    User currentOwner;
    @State(Report.class)
    Report currentReport;

    private DataManager dataManager;
    private String key;
    private String tempPhoto;
    private boolean mIsFollowing;
    private boolean isCompleteMode;
    private int spamCount;

    private int[] currentFilter;
    private CollectionItemsAdapter adapter;
    private AlertDialog followersDialog;
    private boolean isIncomplete = false;
    private int missingCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);
        Icepick.restoreInstanceState(this, savedInstanceState);

        ButterKnife.bind(this);

        setUpFabAnimation();

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        setTitle("");

        viewMode = SharedPrefsUtils.getCollectionItemViewMode(this);
        spamCount = SharedPrefsUtils.getCollectionItemSpamCount(this);
        dataManager = new DataManager(this);

        handleIntent();
        resetFilters();
        setUpListActionMode();
        setUpUI(false);

        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogin();
    }

    private void checkLogin() {
        dataManager.checkLogin(false, new RequestCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean logged) {
                if(!logged) {
                    Intent startLoginActivity = new Intent(CollectionActivity.this, LoginActivity.class);
                    startLoginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startLoginActivity);
                }
            }

            @Override
            public void onError(String error) {
            }
        });
    }

    private void setUpUI(final boolean force) {
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setRefreshing(true);

        if(!force && currentCollection != null) {
            setUpCollection(false);
        } else {
            dataManager.requestCollection(key, new RequestCallback<Collection>() {
                @Override
                public void onSuccess(Collection collection) {
                    if (collection != null) {
                        currentCollection = collection;
                        setUpCollection(force);
                    }
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(CollectionActivity.this, getString(R.string.collection_load_error) + error, Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }

        setTitle("");

        viewButton.setImageResource((viewMode == VIEW_LINEAR) ? R.drawable.ic_view_module_white_24dp : (viewMode == VIEW_GRID_2) ? R.drawable.ic_view_comfy_white_24dp : R.drawable.ic_view_stream_white_24dp);

        editPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });

        privacyButton.getDrawable().setTint(getResources().getColor(android.R.color.white));
        completeButton.getDrawable().setTint(getResources().getColor(android.R.color.white));
        sortButton.getDrawable().setTint(getResources().getColor(android.R.color.white));
    }

    private void checkNewFeature() {
        if(SharedPrefsUtils.getCompleteModeNewFeature(this)) {
            new GuideView.Builder(this)
                    .setTitle(getString(R.string.complete_mode_title))
                    .setContentText(getString(R.string.complete_mode_message))
                    .setGravity(GuideView.Gravity.center)
                    .setTargetView(completeButton)
                    .setContentTextSize(14)
                    .setTitleTextSize(16)
                    .setTitleTypeFace(Typeface.DEFAULT_BOLD)
                    .setDismissType(GuideView.DismissType.anywhere)
                    .setGuideListener(new GuideView.GuideListener() {
                        @Override
                        public void onDismiss(View view) {
                            SharedPrefsUtils.setCompleteModeNewFeature(CollectionActivity.this);
                        }
                    })
                    .build()
                    .show();
        }
    }

    private void setUpCollection(boolean force) {
        titleView.setText(currentCollection.getName());
        editTitle.setText(currentCollection.getName());
        createdSubtitle.setText(String.format(getString(R.string.item_created_date), CalendarUtils.convertDate(currentCollection.getCreated())));

        ImageLoader.load(
                CollectionActivity.this,
                currentCollection.getCoverPath(),
                R.drawable.collection_placeholder,
                coverPhoto);

        loadItems(force);

        ownerToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openProfile(currentCollection.getOwner(), true);
            }
        });

        coverPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(currentCollection.getCoverPath())) {
                    Toast.makeText(CollectionActivity.this, R.string.no_cover_photo_warning, Toast.LENGTH_SHORT).show();
                } else {
                    Intent startFullscreenActivity = new Intent(CollectionActivity.this, FullscreenActivity.class);
                    startFullscreenActivity.putExtra(FullscreenActivity.FULLSCREEN_PHOTO_KEY, key);
                    startFullscreenActivity.putExtra(FullscreenActivity.FULLSCREEN_TYPE, FullscreenActivity.TYPE_COLLECTION);
                    startActivity(startFullscreenActivity);
                }
            }
        });

        checkOwner(force);
        checkReport(force);
    }

    private void checkReport(boolean force) {
        if(currentCollection.getReport() != null) {
            if (currentCollection.isPublic()) {
                currentCollection.setPublic(false);
                dataManager.updateCollection(key, currentCollection);
            }

            if (!force && currentReport != null) {
                setUpReport();
            } else {
                dataManager.requestReport(currentCollection.getReport(), new RequestCallback<Report>() {
                    @Override
                    public void onSuccess(Report report) {
                        currentReport = report;
                        setUpReport();
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(CollectionActivity.this, getString(R.string.report_load_error) + error, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        } else{
            privacySwitch.setChecked(currentCollection.isPublic());
            setUpSwitch(privacySwitch.isChecked());
        }
    }

    private void setUpReport() {
        reportIcon.setVisibility(View.VISIBLE);
        setUpSwitch(false);
    }

    private void updateItemsNumber() {
        String missingItemsText = "";
        String singularText = getString(R.string.collection_items_number_singular);
        String pluralText = getString(R.string.collection_items_number_plural);

        missingCount = 0;
        for(Pair<String, CollectionItem> item : mItems) {
            if (item.second.isMissing()) {
                missingCount++;
            }
        }

        int size = currentCollection.getItems().size();

        if (missingCount > 0) {
            isIncomplete = true;
            missingItemsText = (size - missingCount) + "/";
        } else {
            if (isIncomplete) {
                Snackbar.make(nestedView, R.string.collection_completed_message, Snackbar.LENGTH_LONG)
                        .setAction(R.string.share_action, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                shareCompleted();
                            }
                        }).show();
            }
            isIncomplete = false;
        }

        itemsNumber.setText(String.format(Locale.getDefault(), currentCollection.getItems().size() == 1 ? singularText : pluralText, missingItemsText, NumberUtils.formatNumber(size)));
    }

    public void shareCompleted() {
        String shareText = String.format(Locale.getDefault(), getString(R.string.completed_share_text), currentCollection.getName(), getPackageName());

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getString(R.string.share_choose_title)));
    }

    private void setLayoutManager() {
        if (viewMode == VIEW_LINEAR) {
            itemsList.setLayoutManager(new LinearLayoutManager(this));
        } else {
            itemsList.setLayoutManager(new GridLayoutManager(this, spamCount));
        }
    }

    private void handleIntent() {
        Intent startIntent = getIntent();

        if(startIntent.hasExtra(COLLECTION_EXTRA)) {
            key = startIntent.getStringExtra(COLLECTION_EXTRA);
        }
    }

    private void loadItems(boolean force) {
        swipeRefreshLayout.setRefreshing(true);

        if(!force && mItems != null) {
            setUpItems();
        } else {
            dataManager.requestCollectionItemsList(key, new RequestCallback<List<Pair<String, CollectionItem>>>() {
                @Override
                public void onSuccess(List<Pair<String, CollectionItem>> collectionItems) {
                    mItems = new CollectionItemsArrayList(collectionItems);
                    setUpItems();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(CollectionActivity.this, getString(R.string.collection_items_load_error) + error, Toast.LENGTH_SHORT).show();
                    checkEmpty();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    private void setUpItems() {
        applyFiltersAndSort();
        updateItemsNumber();

        CollectionWidget.updateWidgets(CollectionActivity.this);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void applyFiltersAndSort() {
        List<Pair<String, CollectionItem>> filteredItems = new ArrayList<>();

        for(Pair<String, CollectionItem> item : mItems) {
            if (FilterCollectionItems.filterCollectionItem(CollectionActivity.this, currentFilter, item.second)) {
                filteredItems.add(item);
            }
        }

        FilterCollectionItems.sortCollectionItems(currentFilter, filteredItems);

        setLayoutManager();
        adapter = new CollectionItemsAdapter(CollectionActivity.this);
        itemsList.setAdapter(adapter);
        adapter.addItems(filteredItems);

        checkEmpty();
        itemsList.scheduleLayoutAnimation();
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.pick_image_title)), RC_PHOTO_PICKER);
    }

    private void openProfile(String userKey, boolean animate) {
        Intent profileActivity = new Intent(this, ProfileActivity.class);
        profileActivity.putExtra(ProfileActivity.USER_EXTRA, userKey);
        profileActivity.putExtra(ProfileActivity.USE_TRANSITION, false);

        if(animate) {
            Pair profilePair = Pair.create(profilePicture, "profile");

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this,
                    profilePair);

            ActivityCompat.startActivity(this, profileActivity, options.toBundle());
        } else {
            startActivity(profileActivity);
        }
    }

    private void openFollowersList() {
        if(currentCollection.getFollowers().size() == 0) {
            Toast.makeText(CollectionActivity.this, R.string.no_followers_message, Toast.LENGTH_SHORT).show();
        } else {
            dataManager.requestFollowersList(key, new RequestCallback<List<Pair<String, User>>>() {
                @Override
                public void onSuccess(List<Pair<String, User>> users) {
                    View listView = getLayoutInflater().inflate(R.layout.dialog_users_list, null);

                    final RecyclerView recyclerView = listView.findViewById(R.id.rv_users_list);
                    recyclerView.setAdapter(new UsersListAdapter(CollectionActivity.this, users));

                    followersDialog = new AlertDialog.Builder(CollectionActivity.this)
                            .setView(listView)
                            .setTitle(R.string.followers_dialog_title)
                            .setPositiveButton(R.string.close_button, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(CollectionActivity.this, getString(R.string.users_load_error) + error, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == NEW_COLLECTION_ITEM) {
            if(resultCode == RESULT_OK) {
                Toast.makeText(this, R.string.collection_item_created, Toast.LENGTH_SHORT).show();
                setUpUI(true);
            } else {
                Toast.makeText(this, R.string.collection_item_canceled, Toast.LENGTH_SHORT).show();
            }
        } else if(requestCode == RC_PHOTO_PICKER) {
            if(resultCode == RESULT_OK) {
                if(data != null && data.getData() != null) {
                    editPhoto.setEnabled(false);
                    editCircle.setEnabled(false);
                    progressBar.setProgressWithAnimation(0);
                    progressBar.setVisibility(View.VISIBLE);
                    indefProgress.setVisibility(View.VISIBLE);

                    ImageLoader.load(
                            this,
                            data.getData(),
                            R.drawable.collection_placeholder,
                            coverPhoto);

                    dataManager.updateCollectionPhoto(key, data.getData(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    dataManager.deletePhoto(tempPhoto);
                                    tempPhoto = uri != null ? uri.toString() : null;

                                    editPhoto.setEnabled(true);
                                    editCircle.setEnabled(true);
                                    progressBar.setVisibility(View.GONE);
                                    indefProgress.setVisibility(View.GONE);
                                    Toast.makeText(CollectionActivity.this, R.string.upload_complete, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }, new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            progressBar.setProgressWithAnimation(((float) taskSnapshot.getBytesTransferred()/(float) taskSnapshot.getTotalByteCount()*100));
                        }
                    }, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(CollectionActivity.this, R.string.upload_picture_fail, Toast.LENGTH_SHORT).show();
                            editPhoto.setEnabled(true);
                            editCircle.setEnabled(true);
                            progressBar.setVisibility(View.GONE);
                            indefProgress.setVisibility(View.GONE);
                        }
                    });
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(currentCollection != null) {
            getMenuInflater().inflate(R.menu.menu_collection, menu);

            if(dataManager.isOwner(currentCollection.getOwner())) {
                menu.findItem(R.id.action_edit).setVisible(true);
                menu.findItem(R.id.action_delete).setVisible(true);
            } else {
                menu.findItem(R.id.action_report).setVisible(true);
            }

            MenuItem item = menu.findItem(R.id.action_followers);
            int followers = currentCollection.getFollowers().size();
            if(currentCollection.isPublic()) {
                item.setTitle(String.format(Locale.getDefault(), getResources().getString(followers == 1 ? R.string.collection_followers_singular : R.string.collection_followers_plural), NumberUtils.formatNumber(followers)));
            } else {
                item.setVisible(false);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_followers:
                openFollowersList();
                return true;
            case R.id.action_edit:
                if(mActionMode != null) {
                    return false;
                }

                showActionMode();
                return true;
            case R.id.action_delete:
                AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
                deleteDialog.setMessage(R.string.remove_collection_dialog_title)
                        .setPositiveButton(R.string.delete_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dataManager.deleteCollection(key);
                                CollectionWidget.updateWidgets(CollectionActivity.this);
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
                return true;
            case R.id.action_report:

                View reportView = getLayoutInflater().inflate(R.layout.report_dialog, null);
                final RadioButton option1 = reportView.findViewById(R.id.rb_option_1);
                final RadioButton option2 = reportView.findViewById(R.id.rb_option_2);
                final EditText editText = reportView.findViewById(R.id.input_field);

                String[] options = getResources().getStringArray(R.array.report_options);

                option1.setText(options[0]);
                option2.setText(options[1]);
                option1.setChecked(true);
                option2.setChecked(false);
                option1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        option2.setChecked(!isChecked);
                    }
                });
                option2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        option1.setChecked(!isChecked);
                    }
                });

                AlertDialog.Builder reportDialog = new AlertDialog.Builder(this);
                reportDialog
                        .setTitle(R.string.report_dialog_title)
                        .setView(reportView)
                        .setPositiveButton(R.string.report_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Report report = new Report(key, "", dataManager.getCurrentUid(), editText.getText().toString(), option1.isChecked() ? Report.REPORT_TYPE_SPAM : Report.REPORT_TYPE_INAPPROPRIATE);
                                dataManager.createReport(report);
                                Toast.makeText(CollectionActivity.this, R.string.report_feedback_message, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
                return true;
        }
        return false;
    }

    @Override
    public void onCollectionItemClicked(RecyclerView.ViewHolder holder) {
        View photo = ((CollectionItemsAdapter.CollectionItemViewHolder) holder).transition;
        String itemKey = ((CollectionItemsAdapter.CollectionItemViewHolder) holder).pair.first;

        Pair coverPair = Pair.create(photo, "photo");

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                coverPair);

        Intent startCollectionItemActivity = new Intent(this, CollectionItemActivity.class);
        startCollectionItemActivity.putExtra(CollectionItemActivity.COLLECTION_ITEM_EXTRA, itemKey);
        startCollectionItemActivity.putExtra(CollectionItemActivity.COLLECTION_COVER_EXTRA, currentCollection.getCoverPath());
        startCollectionItemActivity.putExtra(CollectionItemActivity.COLLECTION_OWNER_EXTRA, currentOwner.getName());

        ActivityCompat.startActivity(this, startCollectionItemActivity, options.toBundle());
    }

    @Override
    public void onCollectionItemFlip(boolean missing) {
        if(missing) {
            missingCount++;
        } else {
            missingCount--;
        }
        updateItemsNumber();
    }

    @Override
    public boolean isCompleteMode() {
        return isCompleteMode;
    }

    @Override
    public void onUserClicked(RecyclerView.ViewHolder holder) {
        String userKey = ((UsersListAdapter.UserViewHolder) holder).pair.first;
        openProfile(userKey, false);

        if(followersDialog != null) {
            followersDialog.dismiss();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onBackPressed() {
//        supportFinishAfterTransition();
        finish();
    }

    @Override
    @OnClick({R.id.sp_sort, R.id.sp_origin, R.id.sp_value, R.id.sp_rarity,
            R.id.tv_sort, R.id.tv_origin, R.id.tv_value, R.id.tv_rarity,
            R.id.iv_view, R.id.iv_public, R.id.iv_complete, R.id.iv_sort,
            R.id.sw_share, R.id.tv_clear, R.id.tv_apply})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_view:
                if(viewMode == VIEW_LINEAR) {
                    viewMode = VIEW_GRID_2;
                    spamCount = 2;
                    ((ImageView) view).setImageResource(R.drawable.ic_view_comfy_white_24dp);
                } else if(viewMode == VIEW_GRID_2) {
                    viewMode = VIEW_GRID_3;
                    spamCount = 3;
                    ((ImageView) view).setImageResource(R.drawable.ic_view_stream_white_24dp);
                } else {
                    viewMode = VIEW_LINEAR;
                    ((ImageView) view).setImageResource(R.drawable.ic_view_module_white_24dp);
                }

                SharedPrefsUtils.setCollectionItemSpamCount(this, spamCount);
                SharedPrefsUtils.setCollectionItemViewMode(this, viewMode);

                applyFiltersAndSort();

                break;
            case R.id.iv_public:
                if(currentReport != null) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle(R.string.reported_message_title);
                    alertDialog.setIcon(R.drawable.ic_report_black_24dp);
                    alertDialog.setMessage(getResources().getStringArray(R.array.close_messages)[currentReport.getType()])
                            .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .create()
                            .show();
                    privacySwitch.setChecked(false);
                    setUpSwitch(false);
                } else {
                    pressOption(view, OPTION_SHARE);
                }

                break;
            case R.id.iv_complete:
                pressOption(view, OPTION_COMPLETE);
                isCompleteMode = !isCompleteMode;
                Toast.makeText(this, String.format(Locale.getDefault(), getString(R.string.complete_mode_toast), (isCompleteMode ? getString(R.string.mode_on) : getString(R.string.mode_off))), Toast.LENGTH_SHORT).show();

                break;
            case R.id.iv_sort:
                pressOption(view, OPTION_SORT);

                break;
            case R.id.sw_share:
                final boolean checked = ((SwitchCompat) view).isChecked();

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle(checked ? getResources().getString(R.string.private_collection_title) : getString(R.string.public_collection_title));
                alertDialog.setMessage(checked ? getString(R.string.allow_public) : getString(R.string.block_private))
                        .setPositiveButton(R.string.make_public_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                setUpSwitch(checked);
                                currentCollection.setPublic(checked);
                                dataManager.updateCollection(key, currentCollection);
                            }
                        })
                        .setNegativeButton(R.string.keep_private_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                privacySwitch.setChecked(!checked);
                                setUpSwitch(!checked);
                            }
                        })
                        .create()
                        .show();

                break;
            case R.id.tv_clear:
//                originSpinner.setSelection(0);
//                sortSpinner.setSelection(0);
//                raritySpinner.setSelection(0);
//                valueSpinner.setSelection(0);
                resetFilters();

                break;
            case R.id.tv_apply:

//                currentFilter[FilterCollectionItems.FILTER_ORIGIN] = originSpinner.getSelectedItemPosition();
//                currentFilter[FilterCollectionItems.FILTER_SORT] = sortSpinner.getSelectedItemPosition();
//                currentFilter[FilterCollectionItems.FILTER_RARITY] = raritySpinner.getSelectedItemPosition();
//                currentFilter[FilterCollectionItems.FILTER_VALUE] = valueSpinner.getSelectedItemPosition();

                int applied = 0;
                if(currentFilter[FilterCollectionItems.FILTER_ORIGIN] != 0) applied++;
                if(currentFilter[FilterCollectionItems.FILTER_VALUE] != 0) applied++;
                if(currentFilter[FilterCollectionItems.FILTER_RARITY] != 0) applied++;
                if(applied != 0) {
                    appliedFilters.setText(String.valueOf(applied));
                    appliedFilters.setVisibility(View.VISIBLE);
                    appliedFiltersBack.setVisibility(View.VISIBLE);
                } else {
                    appliedFilters.setVisibility(View.GONE);
                    appliedFiltersBack.setVisibility(View.GONE);
                }

                pressOption(null, OPTION_OFF);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        applyFiltersAndSort();
                    }
                }, 1500);

                break;
            case R.id.sp_origin:
            case R.id.tv_origin:
                openFilterOption(FilterCollectionItems.FILTER_ORIGIN, originOption);
                break;

            case R.id.sp_sort:
            case R.id.tv_sort:
                openFilterOption(FilterCollectionItems.FILTER_SORT, sortOption);
                break;

            case R.id.sp_rarity:
            case R.id.tv_rarity:
                openFilterOption(FilterCollectionItems.FILTER_RARITY, rarityOption);
                break;

            case R.id.sp_value:
            case R.id.tv_value:
                openFilterOption(FilterCollectionItems.FILTER_VALUE, valueOption);
                break;
        }
    }

    private void resetFilters() {
        sortOption.setText(getResources().getStringArray(R.array.sorts)[0]);
        valueOption.setText(getResources().getStringArray(R.array.values)[0]);
        rarityOption.setText(getResources().getStringArray(R.array.rarities)[0]);
        originOption.setText(getString(R.string.all_countries));

        currentFilter = new int[]{0, 0, 0, 0};

        appliedFilters.setVisibility(View.GONE);
        appliedFiltersBack.setVisibility(View.GONE);
    }

    private void setUpSwitch(boolean isChecked) {
        int resource = isChecked ? R.drawable.ic_cloud_queue_white_24dp : R.drawable.ic_cloud_off_white_24dp;
        String text = isChecked ? getString(R.string.public_collection_title) : getString(R.string.private_collection_title);
        privacyButton.setImageResource(resource);
        shareText.setText(text);
    }

    private void checkOwner(boolean force) {
        if(!force && currentOwner != null) {
            setUpOwner();
        } else {
            dataManager.requestProfile(currentCollection.getOwner(), new RequestCallback<User>() {
                @Override
                public void onSuccess(User user) {
                    currentOwner = user;
                    setUpOwner();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(CollectionActivity.this, getString(R.string.user_load_error) + error, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setUpOwner() {
        if (dataManager.isOwner(currentCollection.getOwner())) {
            privacyButton.setVisibility(View.VISIBLE);
            completeButton.setVisibility(View.VISIBLE);

            profileName.setText(R.string.you_text);

            fab.setImageResource(R.drawable.ic_add_white_24dp);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int limit = getResources().getInteger(R.integer.collection_items_number_limit);

                    if (currentCollection.getItems().size() == limit) {
                        Toast.makeText(CollectionActivity.this, String.format(Locale.getDefault(), getString(R.string.collection_items_limit_warning), limit), Toast.LENGTH_SHORT).show();
                    } else {
                        Intent startNewCollectionItemActivity = new Intent(CollectionActivity.this, NewCollectionItemActivity.class);
                        startNewCollectionItemActivity.putExtra(NewCollectionItemActivity.COLLECTION_EXTRA, key);
                        startActivityForResult(startNewCollectionItemActivity, NEW_COLLECTION_ITEM);
                    }
                }
            });

            checkNewFeature();
        } else {
            if (!currentCollection.isPublic()) {
                Toast.makeText(CollectionActivity.this, R.string.private_collection_warning, Toast.LENGTH_SHORT).show();
                finish();
            }

            profileName.setText(currentOwner.getName());

            mIsFollowing = currentCollection.getFollowers().containsKey(dataManager.getCurrentUid());

            fab.setImageResource(mIsFollowing ? R.drawable.ic_bookmark_white_24dp : R.drawable.ic_bookmark_border_white_24dp);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIsFollowing = !mIsFollowing;
                    fab.setImageResource(mIsFollowing ? R.drawable.ic_bookmark_white_24dp : R.drawable.ic_bookmark_border_white_24dp);
                    dataManager.switchFollowingCollection(key, currentCollection.getOwner(), mIsFollowing);
                    Toast.makeText(CollectionActivity.this, String.format("%s %s", mIsFollowing ? getString(R.string.following_message) : getString(R.string.stop_following_message), currentCollection.getName()), Toast.LENGTH_SHORT).show();
                }
            });
        }

        ImageLoader.load(
                CollectionActivity.this,
                currentOwner.getPhotoPath(),
                R.drawable.profile_image_placeholder,
                profilePicture);

        invalidateOptionsMenu();
        fab.show();
    }

    private void openFilterOption(final int option, final TextView textField) {
        String[] entries = new String[0];
        ArrayAdapter<String> adapter = null;

        switch (option) {
            case FilterCollectionItems.FILTER_ORIGIN:
                List<String> countriesFilterAsStrings = FilterOrigin.getInstance().getCountriesFilterAsStrings(this);
                entries = countriesFilterAsStrings.toArray(new String[countriesFilterAsStrings.size()]);
                adapter = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, entries);
//                originAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                spinner.setAdapter(originAdapter);
                break;
            case FilterCollectionItems.FILTER_SORT:
                entries = getResources().getStringArray(R.array.sorts);
                adapter = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, Arrays.asList(entries));
//                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                spinner.setAdapter(adapter);
                break;
            case FilterCollectionItems.FILTER_RARITY:
                entries = getResources().getStringArray(R.array.rarities);
                adapter = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, Arrays.asList(entries));
//                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                spinner.setAdapter(adapter);
                break;
            case FilterCollectionItems.FILTER_VALUE:
                entries = getResources().getStringArray(R.array.values);
                adapter = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, Arrays.asList(entries));
//                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                spinner.setAdapter(adapter);
                break;
        }

        final String[] finalEntries = entries;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentFilter[option] = which;
                textField.setText(finalEntries[which]);
            }})
                .setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void pressOption(View view, int mode) {
        boolean show = optionsMode != mode;
        int visible = (show) ? View.VISIBLE : View.GONE;

        shareOptions.setVisibility(View.GONE);
        sortOptions.setVisibility(View.GONE);

        privacyButton.getDrawable().setTint(getResources().getColor(android.R.color.white));
        completeButton.getDrawable().setTint(getResources().getColor(android.R.color.white));
        sortButton.getDrawable().setTint(getResources().getColor(android.R.color.white));

        privacyButton.setElevation(0.0f);
        completeButton.setElevation(0.0f);
        sortButton.setElevation(0.0f);

        if(mode == OPTION_SHARE) {
            shareOptions.setVisibility(visible);
        } else if(mode == OPTION_SORT) {
            sortOptions.setVisibility(visible);
        }

        if(mode != OPTION_COMPLETE) isCompleteMode = false;

        optionsMode = (show) ? mode : OPTION_OFF;

        if(mode != OPTION_OFF) {
            int color = (show) ? getResources().getColor(R.color.colorAccent) : getResources().getColor(android.R.color.white);
            float elevation = (show) ? optionsToolbar.getElevation() : 0.0f;

            ((ImageView) view).getDrawable().setTint(color);
            view.setElevation(elevation);
        }
    }

    @Override
    public void showActionMode() {
        ActionMode.Callback callback = new ActionMode.Callback() {

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.setTitle(getString(R.string.edit_collection));
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.actionmode_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                showActionModeUI(true);
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_ok:
                        String name = editTitle.getText().toString();

                        if(currentCollection != null) {
                            currentCollection.setName(name);

                            if(tempPhoto != null) {
                                String newPhoto = tempPhoto;
                                tempPhoto = currentCollection.getCoverPath();
                                currentCollection.setCoverPath(newPhoto);
                            }

                            dataManager.updateCollection(key, currentCollection);
                        }

                        titleView.setText(name);
                        CollectionWidget.updateWidgets(CollectionActivity.this);
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                dataManager.deletePhoto(tempPhoto);

                ImageLoader.load(
                        CollectionActivity.this,
                        currentCollection.getCoverPath(),
                        R.drawable.collection_placeholder,
                        coverPhoto);

                showActionModeUI(false);
                mActionMode = null;
            }
        };

        mActionMode = startSupportActionMode(callback);
    }

    private void showActionModeUI(boolean show) {
        if(show) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorAccent));

            coverPhoto.setAlpha(0.4f);
            coverPhoto.setEnabled(false);
            editPhoto.setVisibility(View.VISIBLE);
            editCircle.setVisibility(View.VISIBLE);
            editTitle.setText(titleView.getText());
            editTitle.setVisibility(View.VISIBLE);

            titleView.setVisibility(View.GONE);
            createdSubtitle.setVisibility(View.GONE);
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

            coverPhoto.setAlpha(1f);
            coverPhoto.setEnabled(true);
            editPhoto.setVisibility(View.GONE);
            editCircle.setVisibility(View.GONE);
            editTitle.setText(currentCollection.getName());
            editTitle.setVisibility(View.GONE);

            titleView.setVisibility(View.VISIBLE);
            createdSubtitle.setVisibility(View.VISIBLE);

            hideKeyboard();
        }
    }

    @Override
    public void setUpListActionMode() {
        mListActionModeCallback = new ActionMode.Callback() {

            // Called when the action mode is created; startActionMode() was called
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.setTitle(getString(R.string.action_mode_start_title));
                removeList = new ArrayList<>();

                // Inflate a menu resource providing context menu items
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.actionmode_list_menu, menu);
                return true;
            }

            // Called when the user selects a contextual menu item
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_delete:
                        adapter.actionModeConfirm("");

                        mItems = new CollectionItemsArrayList(adapter.getItems());

                        for(String collectionItemKey : removeList) {
                            currentCollection.getItems().remove(collectionItemKey);
                        }

                        updateItemsNumber();

                        Snackbar.make(nestedView, getListActionModeTitle() + (removeList.size() == 1 ? getString(R.string.item_removed_singular) : getString(R.string.item_removed_plural)), Snackbar.LENGTH_LONG)
                                .setAction(R.string.undo, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        adapter.actionModeUndo();

                                        mItems = new CollectionItemsArrayList(adapter.getItems());

                                        for(String collectionItemKey : removeList) {
                                            currentCollection.getItems().put(collectionItemKey, true);
                                        }

                                        removeList.clear();
                                        updateItemsNumber();
                                    }
                                })
                                .addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                                    @Override
                                    public void onDismissed(Snackbar transientBottomBar, int event) {
                                        super.onDismissed(transientBottomBar, event);

                                        for(String collectionItemKey : removeList) {
                                            currentCollection.getItems().remove(collectionItemKey);
                                            dataManager.deleteCollectionItem(collectionItemKey, key);
                                        }

                                        updateItemsNumber();
                                        removeList.clear();
                                        adapter.actionModeClear();
                                        CollectionWidget.updateWidgets(CollectionActivity.this);
                                    }
                                }).show();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default:
                        return false;
                }
            }

            // Called each time the action mode is shown. Always called after onCreateActionMode, but
            // may be called multiple times if the mode is invalidated.
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorAccent));
                viewButton.setVisibility(View.GONE);
                completeButton.setVisibility(View.GONE);
                sortButton.setVisibility(View.GONE);
                return true; // Return false if nothing is done
            }

            // Called when the user exits the action mode
            @Override
            public void onDestroyActionMode(ActionMode mode) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                viewButton.setVisibility(View.VISIBLE);
                completeButton.setVisibility(View.VISIBLE);
                sortButton.setVisibility(View.VISIBLE);
                mActionModeList = null;
                adapter.actionModeEnd();
            }
        };
    }

    private void setUpFabAnimation() {
        getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(Transition transition) {

            }

            @Override
            public void onTransitionEnd(Transition transition) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fab.show();
                    }
                }, getResources().getInteger(android.R.integer.config_shortAnimTime));
            }

            @Override
            public void onTransitionCancel(Transition transition) {

            }

            @Override
            public void onTransitionPause(Transition transition) {

            }

            @Override
            public void onTransitionResume(Transition transition) {

            }
        });
    }

    @Override
    public boolean supportActionMode() {
        return dataManager.isOwner(currentCollection.getOwner());
    }

    @Override
    public void onEmptyIconClicked() {
        if(dataManager.isOwner(currentCollection.getOwner())) {
            Intent startNewCollectionItemActivity = new Intent(CollectionActivity.this, NewCollectionItemActivity.class);
            startNewCollectionItemActivity.putExtra(NewCollectionItemActivity.COLLECTION_EXTRA, key);
            startActivityForResult(startNewCollectionItemActivity, NEW_COLLECTION_ITEM);
        }
    }

    @Override
    public String getEmptyMessage() {
        return dataManager.isOwner(currentCollection.getOwner()) ? getString(R.string.item_empty_message) : "";
    }

    @Override
    public boolean hasIcon() {
        return dataManager.isOwner(currentCollection.getOwner());
    }

    @Override
    public void checkEmpty() {
        if(adapter.getItemCount() == 0) {
            emptyView.setVisibility(View.VISIBLE);

            emptyMessage.setText(getEmptyMessage());
            emptyMessage.setVisibility(getEmptyMessage().isEmpty() ? View.GONE : View.VISIBLE);
            emptyIcon.setVisibility(hasIcon() ? View.VISIBLE : View.GONE);
            emptyIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onEmptyIconClicked();
                }
            });
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        setUpUI(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}
