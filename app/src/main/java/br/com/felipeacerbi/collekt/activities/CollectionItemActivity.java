package br.com.felipeacerbi.collekt.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.base.ActionModeActivity;
import br.com.felipeacerbi.collekt.adapters.CollectionItemPagerAdapter;
import br.com.felipeacerbi.collekt.listeners.ActionModeListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.models.Report;
import br.com.felipeacerbi.collekt.models.User;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.CalendarUtils;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import br.com.felipeacerbi.collekt.utils.NumberUtils;
import br.com.felipeacerbi.collekt.widget.CollectionWidget;
import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;

public class CollectionItemActivity extends ActionModeActivity {

    public static final String COLLECTION_ITEM_EXTRA = "collection_item";
    public static final String COLLECTION_COVER_EXTRA = "cover";
    public static final String COLLECTION_OWNER_EXTRA = "owner";
    public static final int GALLERY_CODE = 0;

    @BindView(R.id.iv_cover_photo)
    ImageView coverPhoto;
    @BindView(R.id.iv_item_photo)
    ImageView itemPhoto;
    @BindView(R.id.tv_item_title)
    TextView itemTitle;
    @BindView(R.id.tv_item_album)
    TextView itemAlbum;
    @BindView(R.id.tv_item_owner)
    TextView itemOwner;
    @BindView(R.id.tv_gallery_number)
    TextView galleryNumber;
    @BindView(R.id.tv_item_created)
    TextView createdView;
    @BindView(R.id.tv_item_missing)
    TextView missingView;

    @BindView(R.id.vp_item_tabs)
    ViewPager tabsPager;
    @BindView(R.id.tl_tabs)
    TabLayout tabs;

    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_item_title_edit)
    EditText editTitle;
    @BindView(R.id.iv_edit_photo)
    ImageView editPhoto;
    @BindView(R.id.iv_edit_circle)
    ImageView editCircle;

    @State(Collection.class)
    Collection currentCollection;
    @State(CollectionItem.class)
    CollectionItem currentCollectionItem;
    @State(User.class)
    User currentUser;

    private DataManager dataManager;
    private String key;
    private CollectionItemPagerAdapter adapter;
    private String cover = null;
    private String owner = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_item);
        Icepick.restoreInstanceState(this, savedInstanceState);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        setTitle("");

        dataManager = new DataManager(this);

        handleIntent();
        setUpUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogin();
    }

    private void checkLogin() {
        dataManager.checkLogin(false, new RequestCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean logged) {
                if(!logged) {
                    Intent startLoginActivity = new Intent(CollectionItemActivity.this, LoginActivity.class);
                    startLoginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startLoginActivity);
                }
            }

            @Override
            public void onError(String error) {
            }
        });
    }

    private void setUpUI() {
        if(currentCollectionItem != null) {
            setUpItem();
        } else {
            dataManager.requestCollectionItem(key, new RequestCallback<CollectionItem>() {
                @Override
                public void onSuccess(CollectionItem collectionItem) {
                    if (collectionItem != null) {
                        currentCollectionItem = collectionItem;
                        setUpItem();
                    }
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(CollectionItemActivity.this, getString(R.string.collection_item_load_error) + error, Toast.LENGTH_SHORT).show();
                }
            });
        }

        if (cover != null) {
            ImageLoader.load(
                    CollectionItemActivity.this,
                    cover,
                    R.drawable.collection_placeholder,
                    coverPhoto);
        }

        if (owner != null) itemOwner.setText(owner);

        editPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showGalleryActivity = new Intent(CollectionItemActivity.this, GalleryActivity.class);
                showGalleryActivity.putExtra(COLLECTION_ITEM_EXTRA, key);
                startActivityForResult(showGalleryActivity, GALLERY_CODE);
            }
        });
    }

    private void setUpItem() {
        if(currentCollection != null) {
            setUpCollection();
        } else {
            dataManager.requestCollection(currentCollectionItem.getParent(), new RequestCallback<Collection>() {
                @Override
                public void onSuccess(Collection collection) {
                    currentCollection = collection;
                    setUpCollection();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(CollectionItemActivity.this, getString(R.string.collection_load_error) + error, Toast.LENGTH_SHORT).show();
                }
            });
        }

        if(owner == null) {
            if(currentUser != null) {
                setUpUser();
            } else {
                dataManager.requestProfile(currentCollectionItem.getOwner(), new RequestCallback<User>() {
                    @Override
                    public void onSuccess(User newOwner) {
                        currentUser = newOwner;
                        setUpUser();
                    }

                    @Override
                    public void onError(String error) {

                    }
                });
            }
        }

        updateItemGalleryUI();

        toolbar.setTitle(currentCollectionItem.getName());
        itemTitle.setText(currentCollectionItem.getName());
        editTitle.setText(currentCollectionItem.getName());
        createdView.setText(String.format(getString(R.string.item_created_date), CalendarUtils.convertDate(currentCollectionItem.getCreated())));
        missingView.setText(currentCollectionItem.isMissing() ? getString(R.string.item_missing_status) : getString(R.string.item_owned_status));

        adapter = new CollectionItemPagerAdapter(getSupportFragmentManager(), key);
        tabsPager.setAdapter(adapter);
        tabsPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(tabsPager));

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
                } else {
                    toolbar.setTitleTextColor(getResources().getColor(android.R.color.transparent));
                }
            }
        });

        itemPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentCollectionItem.getExtraPhotoPaths().size() == 0) {
                    Toast.makeText(CollectionItemActivity.this, R.string.no_photos_warning, Toast.LENGTH_SHORT).show();
                } else {
                    Intent startFullscreenActivity = new Intent(CollectionItemActivity.this, FullscreenActivity.class);
                    startFullscreenActivity.putExtra(FullscreenActivity.FULLSCREEN_PHOTO_KEY, key);
                    startFullscreenActivity.putExtra(FullscreenActivity.FULLSCREEN_TYPE, FullscreenActivity.TYPE_COLLECTION_ITEM);
                    startActivity(startFullscreenActivity);
                }
            }
        });

        CollectionWidget.updateWidgets(CollectionItemActivity.this);
    }

    private void setUpUser() {
        itemOwner.setText(currentUser.getName());
        owner = currentUser.getName();
    }

    private void setUpCollection() {
        itemAlbum.setText(currentCollection.getName());

        if(cover == null) {
            ImageLoader.load(
                    CollectionItemActivity.this,
                    currentCollection.getCoverPath(),
                    R.drawable.collection_placeholder,
                    coverPhoto);
            cover = currentCollection.getCoverPath();
        }

        invalidateOptionsMenu();
    }

    private void updateItemGalleryUI() {
        ImageLoader.load(
                CollectionItemActivity.this,
                currentCollectionItem.getCoverPhoto(),
                R.drawable.collection_placeholder,
                itemPhoto);

        galleryNumber.setText(NumberUtils.formatNumber(currentCollectionItem.getExtraPhotoPaths().size()));
    }

    private void handleIntent() {
        Intent startIntent = getIntent();

        if(startIntent.hasExtra(COLLECTION_ITEM_EXTRA)) {
            key = startIntent.getStringExtra(COLLECTION_ITEM_EXTRA);
            cover = startIntent.getStringExtra(COLLECTION_COVER_EXTRA);
            owner = startIntent.getStringExtra(COLLECTION_OWNER_EXTRA);
        }
    }

    @Override
    public void showActionMode() {
        ActionMode.Callback callback = new ActionMode.Callback() {

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.setTitle(getString(R.string.edit_collection_item));
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.actionmode_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                showActionModeUI(true);
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_ok:

                        currentCollectionItem.setName(editTitle.getText().toString());
                        itemTitle.setText(currentCollectionItem.getName());

                        Fragment fragment = adapter.getItem(0);
                        if(fragment instanceof ActionModeListener) {
                            ((ActionModeListener) fragment).actionModeConfirm(currentCollectionItem);
                        }
                        mode.finish();
                        CollectionWidget.updateWidgets(CollectionItemActivity.this);
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                showActionModeUI(false);
                mActionMode = null;
            }
        };

        mActionMode = startSupportActionMode(callback);
    }

    private void showActionModeUI(boolean show) {
        if(show) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorAccent));

            itemPhoto.setAlpha(0.4f);
            itemPhoto.setEnabled(false);
            editPhoto.setVisibility(View.VISIBLE);
            editCircle.setVisibility(View.VISIBLE);
            editTitle.setText(itemTitle.getText());
            editTitle.setVisibility(View.VISIBLE);

            itemTitle.setVisibility(View.INVISIBLE);

            Fragment fragment = adapter.getItem(0);
            if(fragment instanceof ActionModeListener) {
                ((ActionModeListener) fragment).actionModeStart();
            }
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

            itemPhoto.setAlpha(1f);
            itemPhoto.setEnabled(true);
            editPhoto.setVisibility(View.GONE);
            editCircle.setVisibility(View.GONE);
            editTitle.setText(currentCollectionItem.getName());
            editTitle.setVisibility(View.INVISIBLE);

            itemTitle.setVisibility(View.VISIBLE);

            hideKeyboard();

            Fragment fragment = adapter.getItem(0);
            if(fragment instanceof ActionModeListener) {
                ((ActionModeListener) fragment).actionModeEnd();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_CODE && resultCode == RESULT_OK) {
            dataManager.requestCollectionItem(key, new RequestCallback<CollectionItem>() {
                @Override
                public void onSuccess(CollectionItem collectionItem) {
                    currentCollectionItem = collectionItem;
                    updateItemGalleryUI();
                }

                @Override
                public void onError(String error) {
                }
            });
        } else {
            Fragment fragment = adapter.getItem(0);
            if(fragment instanceof ActionModeListener) {
                ((ActionModeListener) fragment).actionModeStart();
            }
        }
    }

    @Override
    public void setUpListActionMode() {
        // No lists here
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(currentCollection != null) {
            getMenuInflater().inflate(R.menu.menu_collection_item, menu);

            if (dataManager.isOwner(currentCollection.getOwner())) {
                menu.findItem(R.id.action_edit).setVisible(true);
                menu.findItem(R.id.action_delete).setVisible(true);
            } else {
                menu.findItem(R.id.action_report).setVisible(true);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                if(mActionMode != null) {
                    return false;
                }

                showActionMode();
                return true;
            case R.id.action_delete:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setMessage(R.string.remove_item_dialog_title)
                        .setPositiveButton(getString(R.string.delete_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dataManager.deleteCollectionItem(key, currentCollectionItem.getParent());
                                CollectionWidget.updateWidgets(CollectionItemActivity.this);
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
                return true;
            case R.id.action_report:
                View reportView = getLayoutInflater().inflate(R.layout.report_dialog, null);
                final RadioButton option1 = reportView.findViewById(R.id.rb_option_1);
                final RadioButton option2 = reportView.findViewById(R.id.rb_option_2);
                final EditText editText = reportView.findViewById(R.id.input_field);

                String[] options = getResources().getStringArray(R.array.report_options);

                option1.setText(options[0]);
                option2.setText(options[1]);
                option1.setChecked(true);
                option2.setChecked(false);
                option1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        option2.setChecked(!isChecked);
                    }
                });
                option2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        option1.setChecked(!isChecked);
                    }
                });

                AlertDialog.Builder reportDialog = new AlertDialog.Builder(this);
                reportDialog
                        .setTitle(R.string.report_dialog_title)
                        .setView(reportView)
                        .setPositiveButton(R.string.report_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Report report = new Report(currentCollectionItem.getParent(), key, dataManager.getCurrentUid(), editText.getText().toString(), option1.isChecked() ? Report.REPORT_TYPE_SPAM : Report.REPORT_TYPE_INAPPROPRIATE);
                                dataManager.createReport(report);
                                Toast.makeText(CollectionItemActivity.this, R.string.report_feedback_message, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
//        supportFinishAfterTransition();
        finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}
