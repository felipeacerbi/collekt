package br.com.felipeacerbi.collekt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.adapters.GalleryPagerAdapter;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.repository.DataManager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FullscreenActivity extends AppCompatActivity {

    public static final String FULLSCREEN_PHOTO_KEY = "fullscreen";
    public static final String FULLSCREEN_TYPE = "type";

    public static final int TYPE_COLLECTION = 0;
    public static final int TYPE_COLLECTION_ITEM = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.vp_photos)
    ViewPager photosView;
    @BindView(R.id.tv_photos_count)
    TextView photosCount;

    DataManager dataManager;
    String key;
    CollectionItem currentItem;
    Collection currentCollection;
    private int type;

    List<String> galleryItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);

        ButterKnife.bind(this);

        overridePendingTransition(R.anim.item_slide_in, R.anim.item_hold);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        setTitle("");

        handleIntent();

        dataManager = new DataManager(this);

        switch (type) {
            case TYPE_COLLECTION:
                dataManager.requestCollection(key, new RequestCallback<Collection>() {
                    @Override
                    public void onSuccess(Collection collection) {
                        currentCollection = collection;
                        galleryItems = Collections.singletonList(collection.getCoverPath());

                        setTitle(collection.getName());
                        updateCount(0);

                        photosView.setAdapter(new GalleryPagerAdapter(getSupportFragmentManager(), galleryItems));
                    }

                    @Override
                    public void onError(String error) {

                    }
                });
                break;
            case TYPE_COLLECTION_ITEM:
                dataManager.requestCollectionItem(key, new RequestCallback<CollectionItem>() {
                    @Override
                    public void onSuccess(CollectionItem collectionItem) {
                        currentItem = collectionItem;
                        galleryItems = collectionItem.getExtraPhotoPaths();

                        setTitle(collectionItem.getName());
                        updateCount(0);

                        photosView.setAdapter(new GalleryPagerAdapter(getSupportFragmentManager(), galleryItems));
                    }

                    @Override
                    public void onError(String error) {

                    }
                });
                break;
        }

//        photosView.setPageTransformer(true, new DepthPagerTransformation());
        photosView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateCount(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void updateCount(int position) {
        photosCount.setText(String.format(Locale.getDefault(),"%d/%d", ++position, galleryItems.size()));
    }

    private void handleIntent() {
        Intent startIntent = getIntent();

        if(startIntent.hasExtra(FULLSCREEN_PHOTO_KEY) && startIntent.hasExtra(FULLSCREEN_TYPE)) {
            key = startIntent.getStringExtra(FULLSCREEN_PHOTO_KEY);
            type = startIntent.getIntExtra(FULLSCREEN_TYPE, TYPE_COLLECTION_ITEM);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, R.anim.item_slide_out);
    }
}
