package br.com.felipeacerbi.collekt.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.base.ActionModeActivity;
import br.com.felipeacerbi.collekt.adapters.GalleryAdapter;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnPhotoClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.repository.DataManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.State;

public class GalleryActivity extends ActionModeActivity implements OnPhotoClickListener, EmptyIconClickListener {

    private static final int RC_PHOTO_PICKER = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.list)
    RecyclerView photosList;
    @BindView(R.id.nested_view)
    NestedScrollView nestedView;

    @BindView(R.id.cl_empty)
    ConstraintLayout emptyView;
    @BindView(R.id.tv_empty2)
    TextView emptyMessage;
    @BindView(R.id.iv_empty)
    ImageView emptyIcon;

    @State(CollectionItem.class)
    CollectionItem currentItem;

    private String key;
    private DataManager dataManager;
    private GalleryAdapter adapter;
    private List<String> mPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        ButterKnife.bind(this);

        overridePendingTransition(R.anim.item_slide_in, R.anim.item_hold);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        setTitle("");
        toolbar.setSubtitle(R.string.tap_cover_subtitle);

        handleIntent();
        setUpListActionMode();

        dataManager = new DataManager(this);

        if(currentItem != null) {
            setUpItem();
        } else {
            dataManager.requestCollectionItem(key, new RequestCallback<CollectionItem>() {
                @Override
                public void onSuccess(CollectionItem collectionItem) {
                    currentItem = collectionItem;
                    setUpItem();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(GalleryActivity.this, getString(R.string.collection_item_load_error) + error, Toast.LENGTH_SHORT).show();
                }
            });
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });

        setResult(RESULT_CANCELED);
    }

    private void setUpItem() {
        setTitle(String.format(Locale.getDefault(), getString(R.string.gallery_title), currentItem.getName()));

        adapter = new GalleryAdapter(GalleryActivity.this);
        photosList.setAdapter(adapter);

        if(mPhotos != null) {
            setUpPhotos();
        } else {
            dataManager.requestCollectionItemPhotos(key, new RequestCallback<List<String>>() {
                @Override
                public void onSuccess(List<String> photos) {
                    mPhotos = photos;
                    setUpPhotos();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(GalleryActivity.this, getString(R.string.item_photos_load_error) + error, Toast.LENGTH_SHORT).show();
                    checkEmpty();
                }
            });
        }
    }

    private void setUpPhotos() {
        adapter.addItems(mPhotos);
        checkEmpty();
    }

    private void handleIntent() {
        Intent startIntent = getIntent();

        if(startIntent.hasExtra(CollectionItemActivity.COLLECTION_ITEM_EXTRA)) {
            key = startIntent.getStringExtra(CollectionItemActivity.COLLECTION_ITEM_EXTRA);
        }
    }

    private void pickImage() {
        int limit = getResources().getInteger(R.integer.collection_items_gallery_limit);

        if(currentItem.getExtraPhotoPaths().size() == limit) {
            Toast.makeText(GalleryActivity.this, String.format(Locale.getDefault(), getString(R.string.collection_items_gallery_limit_warning), limit), Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.pick_image_title)), RC_PHOTO_PICKER);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK) {
            if(data != null && data.getData() != null) {
                String newPhoto = data.getData().toString();

                currentItem.getExtraPhotoPaths().add(newPhoto);
                dataManager.updateCollectionItem(key, currentItem);

                mPhotos.add(newPhoto);
                adapter.addItem(newPhoto);
                checkEmpty();
                setResult(RESULT_OK);
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, R.anim.item_slide_out);
    }

    @Override
    public void showActionMode() {
        // No need
    }

    @Override
    public void setUpListActionMode() {
        mListActionModeCallback = new ActionMode.Callback() {

            // Called when the action mode is created; startActionMode() was called
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.setTitle(getString(R.string.action_mode_start_title));
                removeList = new ArrayList<>();

                // Inflate a menu resource providing context menu items
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.actionmode_list_menu, menu);
                return true;
            }

            // Called when the user selects a contextual menu item
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        adapter.actionModeConfirm("");

                        mPhotos = new ArrayList<>();

                        Snackbar.make(nestedView, getListActionModeTitle() + (removeList.size() == 1 ? getString(R.string.item_removed_singular) : getString(R.string.item_removed_plural)), Snackbar.LENGTH_LONG)
                                .setAction(getString(R.string.undo), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        adapter.actionModeUndo();
                                        mPhotos.addAll(adapter.getItems());
                                        removeList.clear();
                                    }
                                })
                                .addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                                    @Override
                                    public void onDismissed(Snackbar transientBottomBar, int event) {
                                        super.onDismissed(transientBottomBar, event);
                                        currentItem.getExtraPhotoPaths().removeAll(removeList);

                                        if(removeList.contains(currentItem.getCoverPhoto())) {
                                            if(currentItem.getExtraPhotoPaths().isEmpty()) {
                                                currentItem.setCoverPhoto("");
                                            } else {
                                                currentItem.setCoverPhoto(CollectionItem.removeEmpties(currentItem.getExtraPhotoPaths()).get(0));
                                            }
                                        }

                                        dataManager.updateCollectionItem(key, currentItem);
                                        removeList.clear();
                                        adapter.actionModeClear();
                                        setResult(RESULT_OK);
                                    }
                                }).show();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default:
                        return false;
                }
            }

            // Called each time the action mode is shown. Always called after onCreateActionMode, but
            // may be called multiple times if the mode is invalidated.
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorAccent));
                return true; // Return false if nothing is done
            }

            // Called when the user exits the action mode
            @Override
            public void onDestroyActionMode(ActionMode mode) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                mActionModeList = null;
                adapter.actionModeEnd();
            }
        };
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void onPhotoClicked(RecyclerView.ViewHolder holder) {
        dataManager.updateCollectionItem(key, currentItem);
    }

    @Override
    public boolean isCoverPath(String path) {
        return currentItem.getCoverPhoto().equals(path);
    }

    @Override
    public void setCoverPath(String path) {
        if(!currentItem.getCoverPhoto().equals(path)) {
            currentItem.setCoverPhoto(path);
            setResult(RESULT_OK);
        }
    }

    @Override
    public void setPhoto(RecyclerView.ViewHolder holder, String path) {
        if(path.isEmpty()) {
            currentItem.getExtraPhotoPaths().remove(holder.getAdapterPosition());
        } else {
            currentItem.getExtraPhotoPaths().set(holder.getAdapterPosition(), path);
        }
        dataManager.updateCollectionItem(key, currentItem);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onEmptyIconClicked() {
        pickImage();
    }

    @Override
    public String getEmptyMessage() {
        return getString(R.string.gallery_empty_message);
    }

    @Override
    public boolean hasIcon() {
        return true;
    }

    @Override
    public void checkEmpty() {
        if(adapter.getItemCount() == 0) {
            emptyView.setVisibility(View.VISIBLE);

            emptyMessage.setText(getEmptyMessage());
            emptyMessage.setVisibility(getEmptyMessage().isEmpty() ? View.GONE : View.VISIBLE);
            emptyIcon.setVisibility(hasIcon() ? View.VISIBLE : View.GONE);
            emptyIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onEmptyIconClicked();
                }
            });
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }
}
