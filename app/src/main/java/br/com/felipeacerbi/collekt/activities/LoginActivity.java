package br.com.felipeacerbi.collekt.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.transition.Scene;
import androidx.transition.Slide;
import androidx.transition.TransitionManager;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Arrays;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.repository.DataManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements OnClickListener, FacebookCallback<LoginResult> {

    private static final int RC_SIGN_IN = 0;
    public static final String USER_EXTRA = "user";

    // UI references.
    @Nullable @BindView(R.id.actv_email)
    AutoCompleteTextView mEmailView;
    @Nullable @BindView(R.id.actv_password)
    EditText mPasswordView;
    @Nullable @BindView(R.id.pb_login_progress)
    View mProgressView;
    @Nullable @BindView(R.id.pb_skip_progress)
    View mProgressSkipView;
    @Nullable @BindView(R.id.cl_email_login_form)
    ConstraintLayout mLoginForm;

    // Firebase
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mCallbackManager;
    private DataManager dataManager;

    // UI Buttons
    @Nullable @BindView(R.id.iv_close_login)
    ImageView mCloseLogin;
    @BindView(R.id.scene_rootview)
    ViewGroup mSceneRoot;

    // Transition Scenes
    private Scene loginScene;
    private Scene firstScene;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(checkGoogleServicesAvailability()) {
            refreshLoginUI();
            createScenes();

            dataManager = new DataManager(this);
            mAuth = dataManager.getAuth();

            setUpGoogleSignIn();
            setUpFaceSignIn();
        } else {
            mSceneRoot = findViewById(R.id.scene_rootview);
            mSceneRoot.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPlayServicesWarning();
                }
            });
            showPlayServicesWarning();
        }
    }

    private void showPlayServicesWarning() {
        Toast.makeText(this, R.string.play_services_warning, Toast.LENGTH_LONG).show();
    }

    private boolean checkGoogleServicesAvailability() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        return googleApiAvailability.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS;
    }

    private void refreshLoginUI() {
        ButterKnife.bind(this);

        if (mEmailView != null && mPasswordView != null) {

            mEmailView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    if (actionId == EditorInfo.IME_ACTION_NEXT) {
                        mPasswordView.requestFocus();
                        return true;
                    }
                    return false;
                }
            });

            mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                        attemptLogin();
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Toast.makeText(
                        LoginActivity.this,
                        getString(R.string.google_signin_error) + e.getMessage(),
                        Toast.LENGTH_LONG).show();
                showProgress(false);
            }
        }
    }

    private void checkSignedIn() {
        dataManager.checkLogin(true, new RequestCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean logged) {
                if(logged) {
                    finish();
                } else {
                    showProgress(false);
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void setUpGoogleSignIn() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            checkSignedIn();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(
                                    LoginActivity.this,
                                    getString(R.string.google_signin_error) + task.getException(),
                                    Toast.LENGTH_LONG).show();
                            showProgress(false);
                        }
                    }
                });
    }

    private void setUpFaceSignIn() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        firebaseAuthWithFace(loginResult.getAccessToken());
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onError(FacebookException error) {
        Toast.makeText(
                LoginActivity.this,
                getString(R.string.facebook_signin_error) + error.getMessage(),
                Toast.LENGTH_LONG).show();
        Log.e("Login", error.getMessage());
        showProgress(false);
    }

    private void firebaseAuthWithFace(AccessToken accessToken) {
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            checkSignedIn();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(
                                    LoginActivity.this,
                                    getString(R.string.facebook_signin_error) + task.getException(),
                                    Toast.LENGTH_LONG).show();
                            showProgress(false);
                        }
                    }
                });
    }

    private void firebaseAuthAnonymously() {
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (mProgressSkipView != null) {
                            mProgressSkipView.setVisibility(View.INVISIBLE);
                        }
                        if (task.isSuccessful()) {
                            checkSignedIn();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(
                                    LoginActivity.this,
                                    getString(R.string.anonymous_signin_error) + task.getException(),
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        if (mEmailView != null && mPasswordView != null) {
            mEmailView.setError(null);
            mPasswordView.setError(null);

            // Store values at the time of the login attempt.
            final String email = mEmailView.getText().toString();
            final String password = mPasswordView.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
                mPasswordView.setError(getString(R.string.error_invalid_password) + getString(R.string.min_pass_length));
                focusView = mPasswordView;
                cancel = true;
            }

            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                mEmailView.setError(getString(R.string.error_field_required));
                focusView = mEmailView;
                cancel = true;
            } else if (!isEmailValid(email)) {
                mEmailView.setError(getString(R.string.error_invalid_email));
                focusView = mEmailView;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                showProgress(true);

                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()) {
                                    checkSignedIn();
                                } else {
                                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                        Toast.makeText(
                                                LoginActivity.this,
                                                R.string.wrong_password,
                                                Toast.LENGTH_LONG).show();
                                        showProgress(false);
                                    } else {
                                        mAuth.createUserWithEmailAndPassword(email, password)
                                                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                                        if (task.isSuccessful()) {
                                                            checkSignedIn();
                                                        } else {
                                                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                                                Toast.makeText(
                                                                        LoginActivity.this,
                                                                        R.string.email_in_use,
                                                                        Toast.LENGTH_LONG).show();
                                                            } else {
                                                                Toast.makeText(
                                                                        LoginActivity.this,
                                                                        R.string.signup_error,
                                                                        Toast.LENGTH_LONG).show();
                                                            }
                                                            showProgress(false);
                                                        }
                                                    }
                                                });
                                    }
                                }
                            }
                        });
            }
        }
    }

    private void resetPassword() {
        final String email = mEmailView != null ? mEmailView.getText().toString() : null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            mEmailView.requestFocus();
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            mEmailView.requestFocus();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.reset_password)
                    .setMessage(R.string.send_email)
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mAuth.sendPasswordResetEmail(email)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(
                                                        LoginActivity.this,
                                                        R.string.email_sent,
                                                        Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(
                                                        LoginActivity.this,
                                                        R.string.send_email_error,
                                                        Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create()
                    .show();
        }

    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        if(mLoginForm != null && mCloseLogin != null && mProgressView != null) {

            mLoginForm.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
            mLoginForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    if(mLoginForm != null) mLoginForm.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
                }
            });

            mCloseLogin.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    if(mCloseLogin != null) mCloseLogin.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    if(mProgressView != null) mProgressView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
                }
            });
        }
    }

    private void updateLoginUI(boolean show) {
        if(show) {
            TransitionManager.go(loginScene, new Slide(Gravity.BOTTOM));
        } else {
            TransitionManager.go(firstScene, new Slide(Gravity.BOTTOM));
        }

        refreshLoginUI();
    }

    private void createScenes() {
        loginScene = Scene.getSceneForLayout(mSceneRoot, R.layout.slide_login, this);
        firstScene = Scene.getSceneForLayout(mSceneRoot, R.layout.first_login, this);
    }

    @Optional @OnClick({R.id.svdb_email_button, R.id.svdb_facebook_button, R.id.svdb_google_button, R.id.iv_close_login, R.id.tv_sign_in_button, R.id.tv_skip_button, R.id.tv_reset_pass})
    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.svdb_email_button:
                attemptLogin();
                break;
            case R.id.svdb_google_button:
                showProgress(true);
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.svdb_facebook_button:
                showProgress(true);
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
                break;
            case R.id.tv_sign_in_button:
                updateLoginUI(true);
                break;
            case R.id.tv_skip_button:
                if (mProgressSkipView != null) {
                    mProgressSkipView.setVisibility(View.VISIBLE);
                }
                firebaseAuthAnonymously();
                break;
            case R.id.iv_close_login:
                updateLoginUI(false);
                break;
            case R.id.tv_reset_pass:
                resetPassword();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(mCloseLogin != null) {
            updateLoginUI(false);
        } else {
            super.onBackPressed();
            finish();
        }
    }
}

