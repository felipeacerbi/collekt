package br.com.felipeacerbi.collekt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.fragments.CollectionListFragment;
import br.com.felipeacerbi.collekt.fragments.ExploreFragment;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.listeners.SwipeRefreshProvider;
import br.com.felipeacerbi.collekt.models.FilterCollections;
import br.com.felipeacerbi.collekt.models.User;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.SharedPrefsUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements EmptyIconClickListener, SwipeRefreshLayout.OnRefreshListener, SwipeRefreshProvider {

    private static final int FRAGMENT_COLLECTIONS = R.id.navigation_collections;
    private static final int FRAGMENT_EXPLORE = R.id.navigation_explore;
    private static final int NEW_COLLECTION = 0;
    private static final int VIEW_LINEAR = 0;
    private static final int VIEW_GRID = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    private int viewMode;
    private int currentFragment = FRAGMENT_COLLECTIONS;
    private DataManager dataManager;
    private int spamCount;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case FRAGMENT_COLLECTIONS:
                    currentFragment = FRAGMENT_COLLECTIONS;
                    transactFragment(CollectionListFragment.newInstance(dataManager.getCurrentUid(), (viewMode == VIEW_LINEAR) ? 1 : spamCount, false, false, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_OWNED, ""));
                    toolbar.setTitle(item.getTitle());
                    invalidateOptionsMenu();
                    return true;
                case FRAGMENT_EXPLORE:
                    currentFragment = FRAGMENT_EXPLORE;
                    transactFragment(ExploreFragment.newInstance(dataManager.getCurrentUid()));
                    toolbar.setTitle(item.getTitle());
                    invalidateOptionsMenu();
                    return true;
            }
            return false;
        }
    };

    private void transactFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fl_container, fragment);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        viewMode = SharedPrefsUtils.getCollectionViewMode(this);
        dataManager = new DataManager(this);
        spamCount = getResources().getInteger(R.integer.grid_list_spam_count);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));

        setSupportActionBar(toolbar);
        setUpFAB();

        startRefresh();

        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        if(currentFragment == FRAGMENT_COLLECTIONS) {
            transactFragment(CollectionListFragment.newInstance(dataManager.getCurrentUid(), (viewMode == VIEW_LINEAR) ? 1 : spamCount, false, false, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_OWNED, ""));
        } else {
            transactFragment(ExploreFragment.newInstance(dataManager.getCurrentUid()));
        }
    }

    private void setUpFAB() {
        fab.setImageResource(R.drawable.ic_search_white_24dp);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchActivity = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(searchActivity);
            }
        });
    }

    private void startNewCollection() {
        final int limit = getResources().getInteger(R.integer.collections_number_limit);

        dataManager.requestProfile(dataManager.getCurrentUid(), new RequestCallback<User>() {
            @Override
            public void onSuccess(User user) {
                if(user.getOwned().size() == limit) {
                    Toast.makeText(MainActivity.this, String.format(Locale.getDefault(), getString(R.string.collections_limit_warning), limit), Toast.LENGTH_SHORT).show();
                } else {
                    Intent newCollectionActivity = new Intent(MainActivity.this, NewCollectionActivity.class);
                    startActivityForResult(newCollectionActivity, NEW_COLLECTION);
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(MainActivity.this, R.string.user_load_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == NEW_COLLECTION) {
            if(resultCode == RESULT_OK) {
                Toast.makeText(this, R.string.collection_created, Toast.LENGTH_SHORT).show();
                transactFragment(CollectionListFragment.newInstance(dataManager.getCurrentUid(), (viewMode == VIEW_LINEAR) ? 1 : spamCount, false, false, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_OWNED, ""));
            } else {
                Toast.makeText(this, R.string.collection_canceled, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_views);
        if(item != null) item.setIcon((viewMode == VIEW_LINEAR) ? R.drawable.ic_view_module_black_24dp : R.drawable.ic_view_stream_black_24dp);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(currentFragment == FRAGMENT_COLLECTIONS) {
            getMenuInflater().inflate(R.menu.main_menu, menu);
        } else {
            getMenuInflater().inflate(R.menu.main_menu_explore, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                startNewCollection();
                break;
            case R.id.action_views:
                if(viewMode == VIEW_GRID) {
                    viewMode = VIEW_LINEAR;
                    item.setIcon(R.drawable.ic_view_module_black_24dp);
                    transactFragment(CollectionListFragment.newInstance(dataManager.getCurrentUid(), 1, false, false, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_OWNED, ""));
                } else if(viewMode == VIEW_LINEAR) {
                    viewMode = VIEW_GRID;
                    item.setIcon(R.drawable.ic_view_stream_black_24dp);
                    transactFragment(CollectionListFragment.newInstance(dataManager.getCurrentUid(), spamCount, false, false, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_OWNED, ""));
                }
                SharedPrefsUtils.setCollectionViewMode(this, viewMode);
                return true;
            case R.id.action_profile:
                Intent profileActivity = new Intent(MainActivity.this, ProfileActivity.class);
                profileActivity.putExtra(ProfileActivity.USER_EXTRA, dataManager.getCurrentUid());
                startActivity(profileActivity);
                return true;
        }
        return false;
    }

    @Override
    public void onEmptyIconClicked() {
        startNewCollection();
    }

    @Override
    public String getEmptyMessage() {
        return getString(R.string.collection_empty_message);
    }

    @Override
    public boolean hasIcon() {
        return true;
    }

    @Override
    public void checkEmpty() {
        // No need
    }

    @Override
    public void startRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        onRefresh();
    }

    @Override
    public void endRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }
}
