package br.com.felipeacerbi.collekt.activities;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentStatePagerAdapter;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.base.ViewPagerActivity;
import br.com.felipeacerbi.collekt.adapters.NewCollectionPagerAdapter;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.CollectionCreatorHelper;

public class NewCollectionActivity extends ViewPagerActivity implements CollectionCreatorHelper {

    private Collection newCollection;
    private DataManager dataManager;
    private String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newCollection = new Collection();
        dataManager = new DataManager(this);
        key = dataManager.getNewCollectionKey();
    }

    @Override
    public void setName(String name) {
        newCollection.setName(name);
    }

    @Override
    public void setCoverPhoto(String photoPath) {
        newCollection.setCoverPath(photoPath);
    }

    @Override
    public String getCoverPhoto() {
        return newCollection.getCoverPath();
    }

    @Override
    public void setIsShared(Boolean shared) {
        newCollection.setPublic(shared);
    }

    @Override
    public void createCollection() {
        dataManager.createCollection(key, newCollection);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void next() {
        nextScreen();
    }

    @Override
    public void close() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(R.string.discard_changes)
                .setPositiveButton(R.string.discard_dialog_option, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dataManager.deletePhoto(newCollection.getCoverPath());
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                })
                .setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }

    @Override
    public FragmentStatePagerAdapter getViewPagerAdapter() {
        return new NewCollectionPagerAdapter(getSupportFragmentManager());
    }

    @Override
    public void onBackPressed() {
        if(getCurrentPosition() == 0) close();
        super.onBackPressed();
    }
}
