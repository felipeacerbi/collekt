package br.com.felipeacerbi.collekt.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.base.ViewPagerActivity;
import br.com.felipeacerbi.collekt.adapters.NewCollectionItemPagerAdapter;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.CollectionItemCreatorHelper;
import br.com.felipeacerbi.collekt.widget.CollectionWidget;

public class NewCollectionItemActivity extends ViewPagerActivity implements CollectionItemCreatorHelper {

    public static final String COLLECTION_EXTRA = "collection";

    private CollectionItem newCollectionItem;
    private DataManager dataManager;
    private String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newCollectionItem = new CollectionItem();

        newCollectionItem.setExtraPhotoPaths(new ArrayList<>(Arrays.asList("", "", "", "")));

        handleIntent();

        dataManager = new DataManager(this);
        key = dataManager.getNewCollectionItemKey();
        newCollectionItem.setOwner(dataManager.getCurrentUid());
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogin();
    }

    private void handleIntent() {
        Intent startIntent = getIntent();

        if(startIntent.hasExtra(COLLECTION_EXTRA)) {
            String key = startIntent.getStringExtra(COLLECTION_EXTRA);
            newCollectionItem.setParent(key);
        }
    }

    private void checkLogin() {
        dataManager.checkLogin(false, new RequestCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean logged) {
                if(!logged) {
                    Intent startLoginActivity = new Intent(NewCollectionItemActivity.this, LoginActivity.class);
                    startLoginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startLoginActivity);
                }
            }

            @Override
            public void onError(String error) {
            }
        });
    }

    @Override
    public void setName(String name) {
        newCollectionItem.setName(name);
    }

    @Override
    public void setDescription(String description) {
        newCollectionItem.setDescription(description);
    }

    @Override
    public void setOrigin(String origin) {
        newCollectionItem.setOrigin(origin);
    }

    @Override
    public void setValue(int value) {
        newCollectionItem.setValue(value);
    }

    @Override
    public void setRarity(int rarity) {
        newCollectionItem.setRarity(rarity);
    }

    @Override
    public void addPhoto(String photo, int place) {
        newCollectionItem.getExtraPhotoPaths().set(place, photo);
    }

    @Override
    public void setMissing(boolean missing) {
        newCollectionItem.setMissing(missing);
    }

    @Override
    public String getPhoto(int position) {
        return newCollectionItem.getExtraPhotoPaths().get(position);
    }

    @Override
    public void createCollectionItem() {
        newCollectionItem.setCoverPhoto(newCollectionItem.getExtraPhotoPaths().get(0));
        dataManager.createCollectionItem(key, newCollectionItem);
        CollectionWidget.updateWidgets(this);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void next() {
        nextScreen();
    }

    @Override
    public void close() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(getString(R.string.discard_changes))
                .setPositiveButton(R.string.discard_dialog_option, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for(String photo : newCollectionItem.getExtraPhotoPaths()) {
                            dataManager.deletePhoto(photo);
                        }
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.keep_editing), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }

    @Override
    public FragmentStatePagerAdapter getViewPagerAdapter() {
        return new NewCollectionItemPagerAdapter(getSupportFragmentManager());
    }

    @Override
    public void onBackPressed() {
        if(getCurrentPosition() == 0) close();
        super.onBackPressed();
    }
}
