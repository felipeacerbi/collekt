package br.com.felipeacerbi.collekt.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentStatePagerAdapter;

import org.parceler.Parcels;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.base.ViewPagerActivity;
import br.com.felipeacerbi.collekt.adapters.NewUserPagerAdapter;
import br.com.felipeacerbi.collekt.models.User;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.ProfileCreatorHelper;

public class NewUserActivity extends ViewPagerActivity implements ProfileCreatorHelper {

    private User originalUser;
    private User newUser;
    private DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleIntent();

        dataManager = new DataManager(this);
    }

    private void handleIntent() {
        Intent startIntent = getIntent();

        if(startIntent.hasExtra(LoginActivity.USER_EXTRA)) {
            originalUser = Parcels.unwrap(startIntent.getParcelableExtra(LoginActivity.USER_EXTRA));
            newUser = new User(originalUser.getName(), "", originalUser.getEmail(), originalUser.getPhotoPath());
        }
    }

    @Override
    public void setUserName(String name) {
        newUser.setName(name);
    }

    @Override
    public void setUserDescription(String description) {
        newUser.setDescription(description);
    }

    @Override
    public String getUserName() {
        return newUser.getName();
    }

    @Override
    public String getUserPhoto() {
        return newUser.getPhotoPath();
    }

    @Override
    public void createUser(User user) {
        if(user == null) user = newUser;

        dataManager.createUser(user);

        Intent startMainActivity = new Intent(this, MainActivity.class);
        startActivity(startMainActivity);

        finish();
    }

    @Override
    public void setUserPhoto(String photoPath) {
        newUser.setPhotoPath(photoPath);
    }

    @Override
    public void next() {
        nextScreen();
    }

    @Override
    public void close() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(getString(R.string.discard_changes))
                .setPositiveButton(R.string.discard_dialog_option, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dataManager.deletePhoto(newUser.getPhotoPath());
                        if(TextUtils.isEmpty(originalUser.getName())) originalUser.setName(getString(R.string.anonymous_name));
                        createUser(originalUser);
                    }
                })
                .setNegativeButton(getString(R.string.keep_editing), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }

    @Override
    public FragmentStatePagerAdapter getViewPagerAdapter() {
        return new NewUserPagerAdapter(getSupportFragmentManager());
    }

    @Override
    public void onBackPressed() {
        if(getCurrentPosition() == 0) close();
        super.onBackPressed();
    }
}
