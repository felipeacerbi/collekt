package br.com.felipeacerbi.collekt.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.Arrays;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.base.ActionModeActivity;
import br.com.felipeacerbi.collekt.fragments.CollectionListFragment;
import br.com.felipeacerbi.collekt.listeners.ActionModeListener;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.listeners.SwipeRefreshProvider;
import br.com.felipeacerbi.collekt.models.FilterCollections;
import br.com.felipeacerbi.collekt.models.User;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import br.com.felipeacerbi.collekt.utils.NumberUtils;
import br.com.felipeacerbi.collekt.utils.SharedPrefsUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;

public class ProfileActivity extends ActionModeActivity implements EmptyIconClickListener, FacebookCallback<LoginResult>, SwipeRefreshLayout.OnRefreshListener, SwipeRefreshProvider {

    public static final String USER_EXTRA = "user";
    public static final String USE_TRANSITION = "transition";
    public static final String FRAGMENT_TAG = "collections_list";
    private static final int VIEW_LINEAR = 0;
    private static final int VIEW_GRID = 1;
    private static final int RC_SIGN_IN = 0;
    private static final int RC_PHOTO_PICKER = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_profile_name)
    TextView nameView;
    @BindView(R.id.tv_profile_description)
    TextView descView;
    @BindView(R.id.tv_collections_number)
    TextView collectionsView;
    @BindView(R.id.tv_following_number)
    TextView followingView;
    @BindView(R.id.tv_followers_number)
    TextView followersView;
    @BindView(R.id.iv_view)
    ImageView viewButton;
    @BindView(R.id.civ_profile_picture)
    ImageView photoView;
    @BindView(R.id.civ_profile_picture_overlay)
    ImageView photoOverlay;

    @BindView(R.id.iv_edit_photo)
    ImageView photoEdit;
    @BindView(R.id.iv_edit_circle)
    ImageView editCircle;
    @BindView(R.id.tv_profile_name_edit)
    TextView nameEdit;
    @BindView(R.id.tv_profile_description_edit)
    TextView descEdit;
    @BindView(R.id.cpb_progress)
    CircularProgressBar progressBar;
    @BindView(R.id.pb_progress)
    ProgressBar indefProgressBar;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.fl_container)
    FrameLayout fragContainer;

    @State(User.class)
    User currentUser;

    private int viewMode;

    private DataManager dataManager;
    private String key;

    private String tempPhoto;
    private int spamCount;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mCallbackManager;
    private boolean useTransition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Icepick.restoreInstanceState(this, savedInstanceState);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        setTitle("");

        dataManager = new DataManager(this);
        spamCount = getResources().getInteger(R.integer.grid_list_spam_count);
        viewMode = SharedPrefsUtils.getCollectionViewMode(this);

        handleIntent();
        setUpUI(false);
        setUpListActionMode();

        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void handleIntent() {
        Intent startIntent = getIntent();

        if(startIntent.hasExtra(USER_EXTRA)) {
            key = startIntent.getStringExtra(USER_EXTRA);
            useTransition = startIntent.getBooleanExtra(USE_TRANSITION, true);
        }
    }

    private void setUpUI(boolean force) {
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        viewButton.setImageResource((viewMode == VIEW_LINEAR) ? R.drawable.ic_view_module_black_24dp : R.drawable.ic_view_stream_black_24dp);
        transactFragment(CollectionListFragment.newInstance(key, (viewMode == VIEW_LINEAR) ? 1 : spamCount, true, false, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_OWNED, ""));

        if(!force && currentUser != null) {
            setUpUser();
        } else {
            dataManager.requestProfile(key, new RequestCallback<User>() {
                @Override
                public void onSuccess(User user) {
                    if (user != null) {
                        currentUser = user;
                        setUpUser();
                    }
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(ProfileActivity.this, getString(R.string.profile_load_error) + error, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setUpUser() {
        nameView.setText(currentUser.getName());
        descView.setText(currentUser.getDescription());

        collectionsView.setText(NumberUtils.formatNumber(currentUser.getOwned().size()));
        followingView.setText(NumberUtils.formatNumber(currentUser.getFollowing().size()));
        followersView.setText(NumberUtils.formatNumber(currentUser.getFollowers()));

        photoEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });

        if (useTransition) {
            ImageLoader.load(
                    ProfileActivity.this,
                    currentUser.getPhotoPath(),
                    R.drawable.profile_image_placeholder,
                    photoView);
        } else {
            ImageLoader.loadNoTransition(
                    ProfileActivity.this,
                    currentUser.getPhotoPath(),
                    R.drawable.profile_image_placeholder,
                    photoView);
        }

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewMode == VIEW_GRID) {
                    viewButton.setImageResource(R.drawable.ic_view_module_black_24dp);
                    transactFragment(CollectionListFragment.newInstance(key, 1, true, false, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_OWNED, ""));
                    viewMode = VIEW_LINEAR;
                } else if (viewMode == VIEW_LINEAR) {
                    viewButton.setImageResource(R.drawable.ic_view_stream_black_24dp);
                    transactFragment(CollectionListFragment.newInstance(key, spamCount, true, false, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_OWNED, ""));
                    viewMode = VIEW_GRID;
                }
                SharedPrefsUtils.setCollectionViewMode(ProfileActivity.this, viewMode);
            }
        });

        invalidateOptionsMenu();
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.pick_image_title)), RC_PHOTO_PICKER);
    }

    private void transactFragment(Fragment fragment) {
        if(getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG) == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction
                    .replace(R.id.fl_container, fragment, FRAGMENT_TAG)
                    .commit();
        }
    }

    private ActionModeListener getListFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if(fragment instanceof ActionModeListener) return (ActionModeListener) fragment;
        return null;
    }

    private void setUpGoogleSignIn() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void setUpFaceSignIn() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        linkWithNewCredential(FacebookAuthProvider.getCredential(loginResult.getAccessToken().getToken()));
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onError(FacebookException error) {
        Toast.makeText(
                ProfileActivity.this,
                getString(R.string.facebook_signin_error) + error.getMessage(),
                Toast.LENGTH_LONG).show();
        Log.e("Login", error.getMessage());
    }

    public void signOut() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();

        Intent startLoginActivity = new Intent(this, LoginActivity.class);
        startLoginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startLoginActivity);
    }

    @Override
    public void showActionMode() {
        ActionMode.Callback callback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.setTitle(getString(R.string.edit_profile));
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.actionmode_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                showActionModeUI(true);
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_ok:
                        String newName = nameEdit.getText().toString();
                        String newDesc = descEdit.getText().toString();

                        if(currentUser != null) {
                            currentUser.setName(newName);
                            currentUser.setDescription(newDesc);

                            if(tempPhoto != null) {
                                String newPhoto = tempPhoto;
                                tempPhoto = currentUser.getPhotoPath();
                                currentUser.setPhotoPath(newPhoto);
                            }

                            dataManager.updateUser(currentUser);
                        }

                        nameView.setText(newName);
                        descView.setText(newDesc);
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                dataManager.deletePhoto(tempPhoto);

                showActionModeUI(false);
                mActionMode = null;
            }
        };

        mActionMode = startSupportActionMode(callback);
    }

    private void showActionModeUI(boolean show) {
        if(show) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorAccent));

            photoView.setAlpha(0.4f);
            photoEdit.setVisibility(View.VISIBLE);
            photoOverlay.setVisibility(View.VISIBLE);
            editCircle.setVisibility(View.VISIBLE);
            nameEdit.setText(nameView.getText());
            nameEdit.setVisibility(View.VISIBLE);
            descEdit.setText(descView.getText());
            descEdit.setVisibility(View.VISIBLE);

            nameView.setVisibility(View.INVISIBLE);
            descView.setVisibility(View.INVISIBLE);
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

            ImageLoader.load(
                    ProfileActivity.this,
                    currentUser.getPhotoPath(),
                    R.drawable.profile_image_placeholder,
                    photoView);

            photoView.setAlpha(1f);
            photoOverlay.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            photoEdit.setVisibility(View.INVISIBLE);
            editCircle.setVisibility(View.INVISIBLE);
            nameEdit.setVisibility(View.INVISIBLE);
            descEdit.setVisibility(View.INVISIBLE);

            nameView.setVisibility(View.VISIBLE);
            descView.setVisibility(View.VISIBLE);

            hideKeyboard();
        }
    }

    @Override
    public void setUpListActionMode() {
        mListActionModeCallback = new ActionMode.Callback() {

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.setTitle(getString(R.string.action_mode_start_title));
                removeList = new ArrayList<>();

                // Inflate a menu resource providing context menu items
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.actionmode_list_menu, menu);
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        if(getListFragment() != null) {
                            getListFragment().actionModeConfirm(null);

                            Snackbar.make(fragContainer, getListActionModeTitle() + (removeList.size() == 1 ? getString(R.string.item_removed_singular) : getString(R.string.item_removed_plural)), Snackbar.LENGTH_LONG)
                                    .setAction(getString(R.string.undo), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            removeList.clear();
                                            getListFragment().actionModeUndo();
                                        }
                                    })
                                    .addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                                        @Override
                                        public void onDismissed(Snackbar transientBottomBar, int event) {
                                            super.onDismissed(transientBottomBar, event);

                                            for (String collectionKey : removeList) {
                                                currentUser.getOwned().remove(collectionKey);
                                                dataManager.deleteCollection(collectionKey);
                                            }

                                            removeList.clear();

                                            if(getListFragment() != null) getListFragment().actionModeClear();
                                        }
                                    }).show();
                        }
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorAccent));
                viewButton.setVisibility(View.GONE);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                viewButton.setVisibility(View.VISIBLE);
                mActionModeList = null;

                if(getListFragment() != null) {
                    getListFragment().actionModeEnd();
                }
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (!dataManager.isFacebookSignedIn()) {
            // Pass the activity result back to the Facebook SDK
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (resultCode == Activity.RESULT_OK && requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                linkWithNewCredential(GoogleAuthProvider.getCredential(account.getIdToken(), null));
            } catch (ApiException e) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Toast.makeText(
                        ProfileActivity.this,
                        getString(R.string.google_signin_error) + e.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        }
        if(resultCode == Activity.RESULT_OK && requestCode == RC_PHOTO_PICKER) {
            if(data != null && data.getData() != null) {
                photoEdit.setEnabled(false);
                editCircle.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgressWithAnimation(0);
                indefProgressBar.setVisibility(View.VISIBLE);

                ImageLoader.load(
                        this,
                        data.getData(),
                        R.drawable.profile_image_placeholder,
                        photoView);

                dataManager.updateUserPhoto(data.getData(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                dataManager.deletePhoto(tempPhoto);
                                tempPhoto = uri != null ? uri.toString() : null;

                                photoEdit.setEnabled(true);
                                editCircle.setEnabled(true);
                                indefProgressBar.setVisibility(View.GONE);
                                Toast.makeText(ProfileActivity.this, getString(R.string.upload_complete), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }, new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        progressBar.setProgressWithAnimation(((float) taskSnapshot.getBytesTransferred()/(float) taskSnapshot.getTotalByteCount()*100));
                    }
                }, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ProfileActivity.this, getString(R.string.upload_picture_fail), Toast.LENGTH_SHORT).show();
                        photoEdit.setEnabled(true);
                        editCircle.setEnabled(true);
                        indefProgressBar.setVisibility(View.GONE);
                    }
                });
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(currentUser != null && dataManager.isOwner(key)) {
            getMenuInflater().inflate(R.menu.menu_profile, menu);

            if(dataManager.isUserAnonymous()) {
                menu.findItem(R.id.action_sign_out).setVisible(false);
            }

            if (dataManager.isGoogleSignedIn()) {
                menu.findItem(R.id.action_google_sign_in).setVisible(false);
            } else {
                setUpGoogleSignIn();
            }

            if (dataManager.isFacebookSignedIn()) {
                menu.findItem(R.id.action_facebook_sign_in).setVisible(false);
            } else {
                setUpFaceSignIn();
            }

            if(currentUser.getEmail() != null && currentUser.getEmail().equals("feaacerbi@gmail.com")) {
                menu.findItem(R.id.action_reports).setVisible(true);
            }

            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                if(mActionMode != null) {
                    return false;
                }

                showActionMode();
                break;
            case R.id.action_google_sign_in:
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.action_facebook_sign_in:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
                break;
            case R.id.action_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                break;
            case R.id.action_sign_out:
                signOut();
                return true;
            case R.id.action_reports:
                Intent reportsIntent = new Intent(this, ReportsActivity.class);
                startActivity(reportsIntent);
                return true;
        }
        return false;
    }

    public void linkWithNewCredential(AuthCredential credential) {
        dataManager.linkNewUserCredential(this, credential, new RequestCallback<AuthResult>() {
            @Override
            public void onSuccess(AuthResult object) {
                Toast.makeText(ProfileActivity.this, R.string.account_linked, Toast.LENGTH_SHORT).show();
                invalidateOptionsMenu();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(ProfileActivity.this, getString(R.string.account_link_fail) + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean supportActionMode() {
        return dataManager.isOwner(key);
    }

    @Override
    public void onEmptyIconClicked() {
        // Nothing
    }

    @Override
    public String getEmptyMessage() {
        return "";
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    public void checkEmpty() {
        // No need
    }

    @Override
    public void onRefresh() {
        setUpUI(true);
    }

    @Override
    public void startRefresh() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void endRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}
