package br.com.felipeacerbi.collekt.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.adapters.ReportsListAdapter;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnReportClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Report;
import br.com.felipeacerbi.collekt.repository.DataManager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportsActivity extends AppCompatActivity implements OnReportClickListener, EmptyIconClickListener {

    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.cl_empty)
    ConstraintLayout emptyView;

    private DataManager dataManager;
    private ReportsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        ButterKnife.bind(this);

        setUpActionBar();

        dataManager = new DataManager(this);
        adapter = new ReportsListAdapter(this);
        recyclerView.setAdapter(adapter);

        dataManager.requestReportsList(new RequestCallback<List<Pair<String, Report>>>() {
            @Override
            public void onSuccess(List<Pair<String, Report>> reports) {
                adapter.addItems(reports);
                checkEmpty();
            }

            @Override
            public void onError(String error) {
                checkEmpty();
            }
        });
    }

    private void setUpActionBar() {
        ActionBar actionBar = getSupportActionBar();

        if(actionBar != null) {
            actionBar.setTitle(R.string.reports_toolbar_title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEmptyIconClicked() {

    }

    @Override
    public String getEmptyMessage() {
        return null;
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    public void checkEmpty() {
        emptyView.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onReportClicked(RecyclerView.ViewHolder holder) {
        // No need
    }

    @Override
    public Context getContext() {
        return this;
    }
}
