package br.com.felipeacerbi.collekt.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.fragments.CollectionListFragment;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.SwipeRefreshProvider;
import br.com.felipeacerbi.collekt.models.FilterCollections;
import br.com.felipeacerbi.collekt.utils.SharedPrefsUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH;

public class SearchActivity extends AppCompatActivity implements EmptyIconClickListener, SwipeRefreshLayout.OnRefreshListener, SwipeRefreshProvider {

    private static final String QUERY_FRAGMENT_TAG = "query_fragment";
    private static final int VIEW_LINEAR = 0;
    private static final int VIEW_GRID = 1;

    private int viewMode;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    private String mQuery = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        viewMode = SharedPrefsUtils.getSearchViewMode(this);

        setTitle("");

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(this);

        handleIntent(getIntent(), false);
    }

    private void transactFragment(Fragment fragment, boolean force) {
        if(force || getSupportFragmentManager().findFragmentByTag(QUERY_FRAGMENT_TAG) == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction
                    .replace(R.id.fl_container, fragment, QUERY_FRAGMENT_TAG)
                    .commit();
        }
    }

    private void handleIntent(Intent intent, boolean force) {
        mQuery = (intent.getStringExtra(SearchManager.QUERY) != null) ? intent.getStringExtra(SearchManager.QUERY) : "";
        transactFragment(CollectionListFragment.newInstance("", (viewMode == VIEW_LINEAR) ? 1 : 2, false, true, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_QUERY, mQuery), force);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView actionView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        actionView.setSearchableInfo(searchManager != null ? searchManager.getSearchableInfo(getComponentName()) : null);
        actionView.setImeOptions(IME_ACTION_SEARCH);
        actionView.setQueryHint(getString(R.string.search_hint));
//        actionView.setOnCloseListener(new SearchView.OnCloseListener() {
//            @Override
//            public boolean onClose() {
//                mQuery = "";
//                transactFragment(CollectionListFragment.newInstance("", (viewMode == VIEW_LINEAR) ? 1 : 2, false, true, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_QUERY, mQuery));
//                return false;
//            }
//        });
        actionView.setBackgroundColor(getResources().getColor(android.R.color.transparent));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                actionView.setIconified(false);
            }
        }, 800);

        ImageView searchButton = actionView.findViewById(R.id.search_button);
        searchButton.setImageResource(R.drawable.ic_search_black_24dp);
        ImageView searchCloseButton = actionView.findViewById(R.id.search_close_btn);
        searchCloseButton.setImageResource(R.drawable.ic_close_black_24dp);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_views:
                if(viewMode == VIEW_GRID) {
                    viewMode = VIEW_LINEAR;
                    item.setIcon(R.drawable.ic_view_module_black_24dp);
                    transactFragment(CollectionListFragment.newInstance("", 1, false, true, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_QUERY, mQuery), true);
                } else if(viewMode == VIEW_LINEAR) {
                    viewMode = VIEW_GRID;
                    item.setIcon(R.drawable.ic_view_stream_black_24dp);
                    transactFragment(CollectionListFragment.newInstance("", 2, false, true, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_QUERY, mQuery), true);
                }
                SharedPrefsUtils.setSearchViewMode(this, viewMode);
                return true;
        }
        return false;
    }

    @Override
    public void onEmptyIconClicked() {
        // Nothing
    }

    @Override
    public String getEmptyMessage() {
        return getString(R.string.search_empty_message);
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    public void checkEmpty() {
        // No need
    }

    @Override
    public void onRefresh() {
        transactFragment(CollectionListFragment.newInstance("", (viewMode == VIEW_LINEAR) ? 1 : 2, false, true, LinearLayoutManager.VERTICAL, FilterCollections.FILTER_QUERY, mQuery), true);
    }

    @Override
    public void startRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        onRefresh();
    }

    @Override
    public void endRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }
}
