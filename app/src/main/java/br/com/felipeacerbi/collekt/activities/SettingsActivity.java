package br.com.felipeacerbi.collekt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import br.com.felipeacerbi.collekt.BuildConfig;
import br.com.felipeacerbi.collekt.R;
import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUpActionBar();

        Element versionElement = new Element();
        versionElement.setTitle(String.format(getString(R.string.version_preference), BuildConfig.VERSION_NAME));

        Element shareElement = new Element();
        shareElement.setTitle(getString(R.string.share_element_title));
        shareElement.setIntent(createShareIntent());
        shareElement.setIconDrawable(R.drawable.ic_share_black_24dp);
        shareElement.setIconTint(R.color.about_item_icon_color);

        View aboutPage = new AboutPage(this)
                .isRTL(false)
                .setImage(R.drawable.ic_launcher_collekt_large)
                .setDescription(getString(R.string.about_description))
                .addEmail(getString(R.string.contact_email))
                .addItem(shareElement)
                .addPlayStore(getPackageName())
                .addWebsite(getString(R.string.privacy_policy_link), getString(R.string.privacy_policy_title))
                .addItem(versionElement)
                .create();

        setContentView(aboutPage);
    }

    private void setUpActionBar() {
        ActionBar actionBar = getSupportActionBar();

        if(actionBar != null) {
            actionBar.setTitle(R.string.about_toolbar_title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private Intent createShareIntent() {
        String text = getString(R.string.share_app_text);
        text = text + "\nhttps://play.google.com/store/apps/details?id=" + getPackageName();

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        share.putExtra(Intent.EXTRA_TEXT, text);

        return share;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}