package br.com.felipeacerbi.collekt.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.repository.DataManager;

public class SplashscreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        DataManager dataManager = new DataManager(this);

        dataManager.checkLogin(true, new RequestCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean logged) {
                if(logged) {
                    finish();
                } else {
                    Intent startLoginActivity = new Intent(SplashscreenActivity.this, LoginActivity.class);
                    startActivity(startLoginActivity);
                    finish();
                }
            }

            @Override
            public void onError(String error) {

            }
        });

    }
}
