package br.com.felipeacerbi.collekt.activities;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.adapters.CollectionsAdapter;
import br.com.felipeacerbi.collekt.fragments.CollectionWidgetListFragment;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnCollectionClickListener;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.SharedPrefsUtils;
import br.com.felipeacerbi.collekt.widget.CollectionWidget;

public class StackWidgetConfigureActivity extends AppCompatActivity implements OnCollectionClickListener, EmptyIconClickListener {

    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    public StackWidgetConfigureActivity() {
        super();
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_stack_widget_configure);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        DataManager dataManager = new DataManager(this);
        int spamCount = getResources().getInteger(R.integer.grid_list_spam_count);

        transactFragment(CollectionWidgetListFragment.newInstance(dataManager.getCurrentUid(), spamCount));
    }

    private void transactFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fl_container, fragment);
        transaction.commit();
    }

    @Override
    public void onCollectionClicked(RecyclerView.ViewHolder holder) {
        SharedPrefsUtils.storeCollectionId(this, mAppWidgetId, ((CollectionsAdapter.CollectionViewHolder) holder).pair.first);

        // It is the responsibility of the configuration activity to update the app widget
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());
        CollectionWidget.updateAppWidget(this, appWidgetManager, mAppWidgetId);

        // Make sure we pass back the original appWidgetId
        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_OK, resultValue);
        finish();
    }

    @Override
    public void onProfileClicked(RecyclerView.ViewHolder holder) {
        // No need
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onEmptyIconClicked() {
        // No need
    }

    @Override
    public String getEmptyMessage() {
        return getString(R.string.widget_config_empty_message);
    }

    @Override
    public boolean hasIcon() {
        return true;
    }

    @Override
    public void checkEmpty() {

    }
}
