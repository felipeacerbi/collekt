package br.com.felipeacerbi.collekt.activities.base;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import java.util.List;
import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.ActionModeProvider;
import br.com.felipeacerbi.collekt.utils.NumberUtils;

public abstract class ActionModeActivity extends AppCompatActivity implements ActionModeProvider {

    public ActionMode mActionModeList;
    public ActionMode mActionMode;
    public ActionMode.Callback mListActionModeCallback;
    public List<String> removeList;

    @Override
    public void actionModeStart() {
        mActionModeList = startSupportActionMode(mListActionModeCallback);
    }

    @Override
    public void actionModeStop() {
        mActionModeList.finish();
    }

    @Override
    public void addItem(String item) {
        removeList.add(item);
        mActionModeList.setTitle(getListActionModeTitle());
    }

    @Override
    public void removeItem(String item) {
        removeList.remove(item);
        if(removeList.size() == 0) {
            mActionModeList.finish();
        } else {
            mActionModeList.setTitle(getListActionModeTitle());
        }
    }

    @Override
    public boolean isActionMode() {
        return mActionModeList != null || mActionMode != null;
    }

    @Override
    public boolean isListActionMode() {
        return mActionModeList != null;
    }

    @Override
    public boolean supportActionMode() {
        return true;
    }

    public String getListActionModeTitle() {
        return String.format(Locale.getDefault(), removeList.size() == 1 ? getString(R.string.collection_items_number_singular) : getString(R.string.collection_items_number_plural), "", NumberUtils.formatNumber(removeList.size()));
    }

    public void hideKeyboard() {
        View view = getWindow().getDecorView();
        try {
            InputMethodManager imm = (InputMethodManager) this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public abstract void showActionMode();
    public abstract void setUpListActionMode();

}
