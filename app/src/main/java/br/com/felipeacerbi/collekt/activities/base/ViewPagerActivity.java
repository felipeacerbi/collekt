package br.com.felipeacerbi.collekt.activities.base;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import br.com.felipeacerbi.collekt.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class ViewPagerActivity extends AppCompatActivity {

    @BindView(R.id.vp_new_user_viewpager)
    public ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        ButterKnife.bind(this);

        viewPager.setAdapter(getViewPagerAdapter());
    }

    @Override
    public void onBackPressed() {
        if(viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
        }
    }

    public void nextScreen() {
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }

    public int getCurrentPosition() {
        return viewPager.getCurrentItem();
    }

    public abstract FragmentStatePagerAdapter getViewPagerAdapter();
}
