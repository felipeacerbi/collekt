package br.com.felipeacerbi.collekt.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

import br.com.felipeacerbi.collekt.fragments.AdFragment;
import br.com.felipeacerbi.collekt.models.AdItem;

public class AdsPagerAdapter extends FragmentStatePagerAdapter {

    private List<AdItem> adItems;

    public AdsPagerAdapter(FragmentManager fm, List<AdItem> adItems) {
        super(fm);
        this.adItems = adItems;
    }

    @Override
    public Fragment getItem(int position) {
        return AdFragment.newInstance(adItems.get(position));
    }

    @Override
    public int getCount() {
        return adItems.size();
    }

}
