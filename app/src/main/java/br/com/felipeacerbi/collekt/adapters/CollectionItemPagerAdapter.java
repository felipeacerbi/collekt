package br.com.felipeacerbi.collekt.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import br.com.felipeacerbi.collekt.fragments.ItemDescriptionFragment;
import br.com.felipeacerbi.collekt.fragments.ReactionsFragment;

public class CollectionItemPagerAdapter extends FragmentPagerAdapter {

    private ItemDescriptionFragment itemDescriptionFragment;
    private ReactionsFragment reactionsFragment;

    public CollectionItemPagerAdapter(FragmentManager fm, String key) {
        super(fm);
        itemDescriptionFragment = ItemDescriptionFragment.newInstance(key);
        reactionsFragment = ReactionsFragment.newInstance(key);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return itemDescriptionFragment;
            case 1:
                return reactionsFragment;
            default:
                return itemDescriptionFragment;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
