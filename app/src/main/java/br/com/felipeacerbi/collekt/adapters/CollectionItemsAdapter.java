package br.com.felipeacerbi.collekt.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.FullscreenActivity;
import br.com.felipeacerbi.collekt.listeners.ActionModeListener;
import br.com.felipeacerbi.collekt.listeners.ActionModeProvider;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnCollectionItemClickListener;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.FlipAnimation;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import br.com.felipeacerbi.collekt.utils.NumberUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CollectionItemsAdapter extends RecyclerView.Adapter<CollectionItemsAdapter.CollectionItemViewHolder>
        implements ActionModeListener, GestureDetector.OnGestureListener {

    private List<Pair<String, CollectionItem>> mItems;
    private OnCollectionItemClickListener mListener;

    private List<Pair<String, CollectionItem>> removeList;
    private int selected = 0;
    private DataManager mDataManager;

    public CollectionItemsAdapter(OnCollectionItemClickListener listener) {
        mItems = new ArrayList<>();
        mListener = listener;
        mDataManager = new DataManager(listener.getContext());
    }

    @Override
    public void onBindViewHolder(final CollectionItemViewHolder holder, int position) {
        holder.pair = mItems.get(position);

        setUpItem(holder, false);

        holder.select.setSelected(false);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mListener instanceof ActionModeProvider) {
                    ActionModeProvider listener = (ActionModeProvider) mListener;

                    if (listener.supportActionMode() && !listener.isActionMode()) {
                        listener.actionModeStart();
                        removeList = new ArrayList<>();
                        selectCard(listener, holder);
                    }
                }
                return true;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener instanceof ActionModeProvider) {
                    ActionModeProvider listener = (ActionModeProvider) mListener;

                    if (listener.supportActionMode() && listener.isListActionMode()) {
                        selectCard(listener, holder);
                    } else if(mListener.isCompleteMode()){
                        flip(holder);
                    } else {
                        mListener.onCollectionItemClicked(holder);
                    }
                } else {
                    mListener.onCollectionItemClicked(holder);
                }
            }
        });
    }

    private void flip(CollectionItemViewHolder holder) {
        if(holder.canAnimate) {
            setUpItem(holder, true);
        }
    }

    private void setUpItem(final CollectionItemViewHolder holder, boolean flip) {

        if(flip) {
            startFlipAnimation(holder);
            setItemMissing(holder);
        } else if(holder.pair.second.isMissing()) {
            holder.front.setVisibility(View.GONE);
            holder.back.setVisibility(View.VISIBLE);
        } else {
            holder.front.setVisibility(View.VISIBLE);
            holder.back.setVisibility(View.GONE);
        }

        holder.titleBack.setText(holder.pair.second.getName());

        ImageLoader.load(
                mListener.getContext(),
                holder.pair.second.getCoverPhoto(),
                R.drawable.collection_placeholder,
                holder.coverBack);

        holder.galleryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.pair.second.getExtraPhotoPaths().size() == 0) {
                    Toast.makeText(mListener.getContext(), R.string.no_photos_warning, Toast.LENGTH_SHORT).show();
                } else {
                    Intent startFullscreenActivity = new Intent(mListener.getContext(), FullscreenActivity.class);
                    startFullscreenActivity.putExtra(FullscreenActivity.FULLSCREEN_PHOTO_KEY, holder.pair.first);
                    startFullscreenActivity.putExtra(FullscreenActivity.FULLSCREEN_TYPE, FullscreenActivity.TYPE_COLLECTION_ITEM);
                    mListener.getContext().startActivity(startFullscreenActivity);
                }
            }
        });

        holder.title.setText(holder.pair.second.getName());

        ImageLoader.load(
                mListener.getContext(),
                holder.pair.second.getCoverPhoto(),
                R.drawable.collection_placeholder,
                holder.cover);

        String commentsText = String.format(Locale.getDefault(), "%s", NumberUtils.formatNumber(holder.pair.second.getReactions().size()));
        holder.comments.setText(commentsText);
        holder.commentsBack.setText(commentsText);

        holder.gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.pair.second.getExtraPhotoPaths().size() == 0) {
                    Toast.makeText(mListener.getContext(), R.string.no_photos_warning, Toast.LENGTH_SHORT).show();
                } else {
                    Intent startFullscreenActivity = new Intent(mListener.getContext(), FullscreenActivity.class);
                    startFullscreenActivity.putExtra(FullscreenActivity.FULLSCREEN_PHOTO_KEY, holder.pair.first);
                    startFullscreenActivity.putExtra(FullscreenActivity.FULLSCREEN_TYPE, FullscreenActivity.TYPE_COLLECTION_ITEM);
                    mListener.getContext().startActivity(startFullscreenActivity);
                }
            }
        });
    }

    private int getMediumDuration(Context context) {
        return context.getResources().getInteger(android.R.integer.config_mediumAnimTime);
    }

    private void startFlipAnimation(final CollectionItemViewHolder holder) {
        holder.flipAnimation = new FlipAnimation(holder.front, holder.back, getMediumDuration(mListener.getContext()));

        if(holder.pair.second.isMissing()) holder.flipAnimation.reverse();

        holder.flipAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                holder.canAnimate = false;
            }

            public void onAnimationEnd(Animation animation) {
                holder.canAnimate = true;
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });

        holder.itemView.startAnimation(holder.flipAnimation);
    }

    private void setItemMissing(CollectionItemViewHolder holder) {
        holder.pair.second.setMissing(!holder.pair.second.isMissing());
        mDataManager.updateCollectionItem(holder.pair.first, holder.pair.second);
        mListener.onCollectionItemFlip(holder.pair.second.isMissing());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void addItems(List<Pair<String, CollectionItem>> collectionItems) {
        mItems.clear();
        mItems = collectionItems;
        notifyItemRangeInserted(0, mItems.size());
    }

    public List<Pair<String, CollectionItem>> getItems() {
        return mItems;
    }

    @Override
    public CollectionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(
                        R.layout.collection_item_card,
                        parent,
                        false);
        return new CollectionItemsAdapter.CollectionItemViewHolder(view);
    }

    private void selectCard(ActionModeProvider listener, CollectionItemViewHolder holder) {
        holder.select.setSelected(!holder.select.isSelected());

        if(holder.select.isSelected()) {
            listener.addItem(holder.pair.first);
            removeList.add(holder.pair);
            selected++;
        } else {
            listener.removeItem(holder.pair.first);
            removeList.remove(holder.pair);
            selected--;
        }
    }

    private void removeItems() {
        for(Pair<String, CollectionItem> item : removeList) {
            notifyItemRemoved(mItems.indexOf(item));
            mItems.remove(item);
        }
        selected = 0;
    }

    private void restoreItems() {
        for(Pair<String, CollectionItem> item : removeList) {
            mItems.add(item);
            notifyItemInserted(mItems.indexOf(item));
        }
        removeList.clear();
    }

    public void checkEmpty() {
        if(mListener instanceof EmptyIconClickListener) {
            ((EmptyIconClickListener) mListener).checkEmpty();
        }
    }

    @Override
    public void actionModeStart() {
        // No need
    }

    @Override
    public void actionModeEnd() {
        if(selected > 0) notifyDataSetChanged();
        checkEmpty();
    }

    @Override
    public void actionModeConfirm(Object item) {
        removeItems();
    }

    @Override
    public void actionModeUndo() {
        restoreItems();
        checkEmpty();
    }

    @Override
    public void actionModeClear() {
        removeList.clear();
        notifyDataSetChanged();
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return true;
    }

    public class CollectionItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.transition)
        public View transition;
        @BindView(R.id.v_select_view)
        View select;
        @BindView(R.id.front_layout)
        View front;
        @BindView(R.id.back_layout)
        View back;

        @BindView(R.id.iv_item_cover)
        ImageView cover;
        @BindView(R.id.tv_item_title)
        TextView title;
        @BindView(R.id.tv_item_comments)
        TextView comments;
        @BindView(R.id.iv_gallery_button)
        ImageView gallery;

        @BindView(R.id.iv_item_cover_back)
        ImageView coverBack;
        @BindView(R.id.tv_item_title_back)
        TextView titleBack;
        @BindView(R.id.iv_gallery_button_back)
        ImageView galleryBack;
        @BindView(R.id.tv_item_comments_back)
        TextView commentsBack;

        FlipAnimation flipAnimation;
        boolean canAnimate = true;

        public Pair<String, CollectionItem> pair;

        CollectionItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
