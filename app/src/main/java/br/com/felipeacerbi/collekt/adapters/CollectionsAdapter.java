package br.com.felipeacerbi.collekt.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.ActionModeListener;
import br.com.felipeacerbi.collekt.listeners.ActionModeProvider;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnCollectionClickListener;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import br.com.felipeacerbi.collekt.utils.NumberUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CollectionsAdapter extends RecyclerView.Adapter<CollectionsAdapter.CollectionViewHolder> implements ActionModeListener {

    private OnCollectionClickListener mListener;
    private List<Pair<String, Collection>> mCollections;

    private List<Pair<String, Collection>> removeList;
    private int selected = 0;

    public CollectionsAdapter(OnCollectionClickListener listener) {
        mCollections = new ArrayList<>();
        this.mListener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull final CollectionViewHolder holder, int position) {
        holder.pair = mCollections.get(position);

        holder.title.setText(holder.pair.second.getName());
        holder.number.setText(NumberUtils.formatNumber(holder.pair.second.getItems().size()));

        if(holder.pair.second.isPublic()) {
            holder.followers.setText(String.format(Locale.getDefault(), holder.pair.second.getFollowers().size() == 1 ? mListener.getContext().getString(R.string.collection_followers_singular) : mListener.getContext().getString(R.string.collection_followers_plural) , NumberUtils.formatNumber(holder.pair.second.getFollowers().size())));
            holder.followers.setVisibility(View.VISIBLE);
            holder.icon.setImageResource(R.drawable.ic_rss_feed_white_24dp);
        } else {
            holder.followers.setVisibility(View.INVISIBLE);
            holder.icon.setImageResource(R.drawable.ic_cloud_off_white_24dp);
        }

        ImageLoader.load(
                mListener.getContext(),
                holder.pair.second.getCoverPath(),
                R.drawable.collection_placeholder,
                holder.cover);

        holder.card.setSelected(false);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(mListener instanceof ActionModeProvider) {
                    ActionModeProvider listener = (ActionModeProvider) mListener;

                    if(listener.supportActionMode() && !listener.isActionMode()) {
                        listener.actionModeStart();
                        removeList = new ArrayList<>();
                        selectCard(listener, holder);
                    }
                }
                return true;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener instanceof ActionModeProvider) {
                    ActionModeProvider listener = (ActionModeProvider) mListener;

                    if(listener.supportActionMode() && listener.isListActionMode()) {
                        selectCard(listener, holder);
                    } else {
                        mListener.onCollectionClicked(holder);
                    }
                } else {
                    mListener.onCollectionClicked(holder);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCollections.size();
    }

    @Override
    public CollectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.collection_list_item, parent, false);
        return new CollectionViewHolder(view);
    }

    public void addItems(List<Pair<String, Collection>> collections) {
        mCollections = collections;
        notifyItemRangeInserted(0, mCollections.size());
    }

    public List<Pair<String, Collection>> getItems() {
        return mCollections;
    }

    private void selectCard(ActionModeProvider listener, CollectionViewHolder holder) {
        holder.card.setSelected(!holder.card.isSelected());

        if(holder.card.isSelected()) {
            listener.addItem(holder.pair.first);
            removeList.add(holder.pair);
            selected++;
        } else {
            listener.removeItem(holder.pair.first);
            removeList.remove(holder.pair);
            selected--;
        }
    }

    private void removeItems() {
        for(Pair<String, Collection> item : removeList) {
            notifyItemRemoved(mCollections.indexOf(item));
            mCollections.remove(item);
        }
        selected = 0;
    }

    private void restoreItems() {
        for(Pair<String, Collection> item : removeList) {
            mCollections.add(item);
            notifyItemInserted(mCollections.indexOf(item));
        }
        removeList.clear();
    }

    public void checkEmpty() {
        if(mListener instanceof EmptyIconClickListener) {
            ((EmptyIconClickListener) mListener).checkEmpty();
        }
    }

    @Override
    public void actionModeStart() {
        // No need
    }

    @Override
    public void actionModeEnd() {
        if(selected > 0) notifyDataSetChanged();
        checkEmpty();
    }

    @Override
    public void actionModeConfirm(Object item) {
        removeItems();
    }

    @Override
    public void actionModeUndo() {
        restoreItems();
        checkEmpty();
    }

    @Override
    public void actionModeClear() {
        removeList.clear();
        notifyDataSetChanged();
    }

    public class CollectionViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card)
        public View card;
        @BindView(R.id.iv_collection_cover)
        ImageView cover;
        @BindView(R.id.tv_collection_title)
        TextView title;
        @BindView(R.id.tv_collection_followers)
        TextView followers;
        @BindView(R.id.tv_collection_number)
        TextView number;
        @BindView(R.id.iv_collection_icon)
        ImageView icon;

        public Pair<String, Collection> pair;

        CollectionViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
