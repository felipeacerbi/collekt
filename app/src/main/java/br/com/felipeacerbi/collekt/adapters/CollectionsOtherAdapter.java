package br.com.felipeacerbi.collekt.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnCollectionClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.User;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import br.com.felipeacerbi.collekt.utils.NumberUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CollectionsOtherAdapter extends RecyclerView.Adapter<CollectionsOtherAdapter.CollectionOtherViewHolder> {

    private List<Pair<String, Collection>> mCollections;
    private OnCollectionClickListener mListener;

    private int mOrientation;
    private DataManager dataManager;

    public CollectionsOtherAdapter(OnCollectionClickListener listener, int mOrientation) {
        mCollections = new ArrayList<>();
        this.mListener = listener;
        this.mOrientation = mOrientation;
        dataManager = new DataManager(listener.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull final CollectionOtherViewHolder holder, int position) {
        holder.pair = mCollections.get(position);

        dataManager.requestProfile(holder.pair.second.getOwner(), new RequestCallback<User>() {
            @Override
            public void onSuccess(User user) {
                holder.name.setText(user.getName());

                ImageLoader.load(
                        mListener.getContext(),
                        user.getPhotoPath(),
                        R.drawable.profile_image_placeholder,
                        holder.profile);
            }

            @Override
            public void onError(String error) {

            }
        });

        holder.title.setText(holder.pair.second.getName());
        holder.number.setText(NumberUtils.formatNumber(holder.pair.second.getItems().size()));

        if(holder.pair.second.isPublic()) {
            holder.followers.setText(String.format(Locale.getDefault(), holder.pair.second.getFollowers().size() == 1 ? mListener.getContext().getString(R.string.collection_followers_singular) : mListener.getContext().getString(R.string.collection_followers_plural) , NumberUtils.formatNumber(holder.pair.second.getFollowers().size())));
            holder.followers.setVisibility(View.VISIBLE);
            holder.icon.setImageResource(R.drawable.ic_rss_feed_white_24dp);
        } else {
            holder.followers.setVisibility(View.INVISIBLE);
            holder.icon.setImageResource(R.drawable.ic_cloud_off_white_24dp);
        }

        ImageLoader.load(
                mListener.getContext(),
                holder.pair.second.getCoverPath(),
                R.drawable.collection_placeholder,
                holder.cover);

        holder.profileBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onProfileClicked(holder);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onCollectionClicked(holder);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCollections.size();
    }

    @Override
    public CollectionOtherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(mOrientation == LinearLayoutManager.HORIZONTAL) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.collection_other_list_item_horizontal, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.collection_other_list_item_vertical, parent, false);
        }
        return new CollectionOtherViewHolder(view);
    }

    public void addItems(List<Pair<String, Collection>> collections) {
        mCollections = collections;
        notifyItemRangeInserted(0, mCollections.size());
    }

    public List<Pair<String, Collection>> getItems() {
        return mCollections;
    }

    public void checkEmpty() {
        if(mListener instanceof EmptyIconClickListener) {
            ((EmptyIconClickListener) mListener).checkEmpty();
        }
    }

    public class CollectionOtherViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card)
        public View card;
        @BindView(R.id.iv_collection_cover)
        ImageView cover;
        @BindView(R.id.tv_collection_title)
        TextView title;
        @BindView(R.id.tv_collection_followers)
        TextView followers;
        @BindView(R.id.tv_collection_number)
        TextView number;
        @BindView(R.id.iv_collection_icon)
        ImageView icon;

        @BindView(R.id.cl_profile_bar)
        ConstraintLayout profileBar;
        @BindView(R.id.civ_profile_picture)
        public CircleImageView profile;
        @BindView(R.id.tv_profile_name)
        TextView name;

        public Pair<String, Collection> pair;

        CollectionOtherViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
