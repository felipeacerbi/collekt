package br.com.felipeacerbi.collekt.adapters;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Binder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.ActionModeListener;
import br.com.felipeacerbi.collekt.listeners.ActionModeProvider;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnPhotoClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import butterknife.BindView;
import butterknife.ButterKnife;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.PhotoViewHolder> implements ActionModeListener {

    private List<String> mPaths;
    private OnPhotoClickListener mListener;
    private DataManager dataManager;

    private List<String> removeList;
    private int selected = 0;

    public GalleryAdapter(OnPhotoClickListener listener) {
        mPaths = new ArrayList<>();
        this.mListener = listener;
        dataManager = new DataManager(listener.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull final PhotoViewHolder holder, int position) {
        holder.path = mPaths.get(position);

        if(!holder.path.contains("http")) {
            holder.coverIcon.setVisibility(View.GONE);
            holder.select.setSelected(false);
            holder.itemView.setOnLongClickListener(null);
            holder.itemView.setOnClickListener(null);
            holder.photo.setImageResource(R.drawable.collection_placeholder);

            holder.indefProgress.setVisibility(View.VISIBLE);
            holder.progressBar.setVisibility(View.VISIBLE);

            Uri imageUri = Uri.parse(holder.path);

            int uriPermission = mListener.getContext().checkUriPermission(imageUri, "android.permission.READ_EXTERNAL_STORAGE", null,
                    Binder.getCallingPid(), Binder.getCallingUid(),
                    Intent.FLAG_GRANT_READ_URI_PERMISSION);

            if(uriPermission == PackageManager.PERMISSION_DENIED) {
                int index = mPaths.indexOf(holder.path);
                mPaths.remove(index);
            } else {
                dataManager.updateCollectionItemPhoto(mListener.getKey(), imageUri, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(final Uri uri) {
                                dataManager.requestCollectionItem(mListener.getKey(), new RequestCallback<CollectionItem>() {
                                    @Override
                                    public void onSuccess(CollectionItem collectionItem) {
                                        mListener.setPhoto(holder, uri.toString());
                                        if (mPaths.isEmpty()) mListener.setCoverPath(uri.toString());

                                        mPaths.set(holder.getAdapterPosition(), uri.toString());
                                        notifyItemChanged(holder.getAdapterPosition());
                                    }

                                    @Override
                                    public void onError(String error) {
                                        Toast.makeText(mListener.getContext(), mListener.getContext().getString(R.string.collection_item_load_error), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });
                    }
                }, new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        holder.progressBar.setProgressWithAnimation(((float) taskSnapshot.getBytesTransferred() * 100 / (float) taskSnapshot.getTotalByteCount()));
                    }
                }, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(mListener.getContext(), mListener.getContext().getString(R.string.upload_picture_fail), Toast.LENGTH_SHORT).show();
                    }
                });
            }

        } else {
            holder.indefProgress.setVisibility(View.GONE);
            holder.progressBar.setVisibility(View.GONE);
            holder.progressBar.setProgressWithAnimation(0);

            ImageLoader.load(
                    mListener.getContext(),
                    holder.path,
                    R.drawable.collection_placeholder,
                    holder.photo);

            if(mPaths.size() == 1) {
                mListener.setCoverPath(holder.path);
                mListener.onPhotoClicked(holder);
            }
            holder.coverIcon.setVisibility(mListener.isCoverPath(holder.path) ? View.VISIBLE : View.GONE);

            holder.select.setSelected(false);

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mListener instanceof ActionModeProvider) {
                        ActionModeProvider listener = (ActionModeProvider) mListener;

                        if (listener.supportActionMode() && !listener.isActionMode()) {
                            listener.actionModeStart();
                            removeList = new ArrayList<>();
                            selectCard(listener, holder);
                        }
                    }
                    return true;
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener instanceof ActionModeProvider) {
                        ActionModeProvider listener = (ActionModeProvider) mListener;

                        if (listener.supportActionMode() && listener.isListActionMode()) {
                            selectCard(listener, holder);
                        } else {
                            changeCover(holder);
                        }
                    } else {
                        changeCover(holder);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mPaths.size();
    }

    private void changeCover(PhotoViewHolder holder) {
        mListener.setCoverPath(holder.path);
        notifyItemRangeChanged(0, getItemCount());
        mListener.onPhotoClicked(holder);
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.photo_item, parent, false);
        return new PhotoViewHolder(view);
    }

    public void addItems(List<String> paths) {
        mPaths.addAll(paths);
        notifyItemRangeInserted(0, mPaths.size());
    }

    public List<String> getItems() {
        return mPaths;
    }

    public void addItem(String path) {
        mPaths.add(getItemCount(), path);
        notifyItemInserted(mPaths.indexOf(path));
    }

    private void selectCard(ActionModeProvider listener, PhotoViewHolder holder) {
        holder.select.setSelected(!holder.select.isSelected());

        if(holder.select.isSelected()) {
            listener.addItem(holder.path);
            removeList.add(holder.path);
            selected++;
        } else {
            listener.removeItem(holder.path);
            removeList.remove(holder.path);
            selected--;
        }
    }

    private void removeItems() {
        for(String path : removeList) {
            notifyItemRemoved(mPaths.indexOf(path));
            mPaths.remove(path);
        }
        selected = 0;
    }

    private void restoreItems() {
        for(String path : removeList) {
            mPaths.add(path);
            notifyItemInserted(mPaths.indexOf(path));
        }
        removeList.clear();
    }

    public void checkEmpty() {
        if(mListener instanceof EmptyIconClickListener) {
            ((EmptyIconClickListener) mListener).checkEmpty();
        }
    }

    @Override
    public void actionModeStart() {
        // No need
    }

    @Override
    public void actionModeEnd() {
        if(selected > 0) notifyDataSetChanged();
        checkEmpty();
    }

    @Override
    public void actionModeConfirm(Object item) {
        removeItems();
    }

    @Override
    public void actionModeUndo() {
        restoreItems();
        checkEmpty();
    }

    @Override
    public void actionModeClear() {
        removeList.clear();
        notifyDataSetChanged();
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_photo)
        ImageView photo;
        @BindView(R.id.iv_cover_icon)
        ImageView coverIcon;
        @BindView(R.id.v_select_view)
        View select;

        @BindView(R.id.pb_progress)
        ProgressBar indefProgress;
        @BindView(R.id.cpb_progress)
        CircularProgressBar progressBar;

        public String path;

        PhotoViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
