package br.com.felipeacerbi.collekt.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

import br.com.felipeacerbi.collekt.fragments.FullscreenFragment;

public class GalleryPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> photos;

    public GalleryPagerAdapter(FragmentManager fm, List<String> photos) {
        super(fm);
        this.photos = photos;
    }

    @Override
    public Fragment getItem(int position) {
        return FullscreenFragment.newInstance(photos.get(position));
    }

    @Override
    public int getCount() {
        return photos.size();
    }
}
