package br.com.felipeacerbi.collekt.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import br.com.felipeacerbi.collekt.fragments.NewCollectionItemFragment;
import br.com.felipeacerbi.collekt.fragments.NewCollectionItemOwnedFragment;
import br.com.felipeacerbi.collekt.fragments.NewCollectionItemPhotosFragment;
import br.com.felipeacerbi.collekt.fragments.NewCollectionItemStoryFragment;

public class NewCollectionItemPagerAdapter extends FragmentStatePagerAdapter {

    private NewCollectionItemFragment collectionItemFragment;
    private NewCollectionItemStoryFragment collectionItemStoryFragment;
    private NewCollectionItemPhotosFragment collectionItemPhotosFragment;
    private NewCollectionItemOwnedFragment collectionItemOwnedFragment;

    public NewCollectionItemPagerAdapter(FragmentManager fm) {
        super(fm);
        collectionItemFragment = new NewCollectionItemFragment();
        collectionItemStoryFragment = new NewCollectionItemStoryFragment();
        collectionItemPhotosFragment = new NewCollectionItemPhotosFragment();
        collectionItemOwnedFragment = new NewCollectionItemOwnedFragment();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return collectionItemPhotosFragment;
            case 1:
                return collectionItemFragment;
            case 2:
                return collectionItemStoryFragment;
            case 3:
                return collectionItemOwnedFragment;
            default:
                return collectionItemFragment;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

}
