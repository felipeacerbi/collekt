package br.com.felipeacerbi.collekt.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import br.com.felipeacerbi.collekt.fragments.NewCollectionCoverFragment;
import br.com.felipeacerbi.collekt.fragments.NewCollectionFragment;

public class NewCollectionPagerAdapter extends FragmentStatePagerAdapter {

    private NewCollectionFragment collectionFragment;
    private NewCollectionCoverFragment collectionCoverFragment;

    public NewCollectionPagerAdapter(FragmentManager fm) {
        super(fm);
        collectionFragment = new NewCollectionFragment();
        collectionCoverFragment = new NewCollectionCoverFragment();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return collectionFragment;
            case 1:
                return collectionCoverFragment;
            default:
                return collectionFragment;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

}
