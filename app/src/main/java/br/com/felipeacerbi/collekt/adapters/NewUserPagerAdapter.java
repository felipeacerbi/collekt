package br.com.felipeacerbi.collekt.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import br.com.felipeacerbi.collekt.fragments.NewUserFragment;
import br.com.felipeacerbi.collekt.fragments.NewUserPhotoFragment;

public class NewUserPagerAdapter extends FragmentStatePagerAdapter {

    private NewUserFragment userFragment;
    private NewUserPhotoFragment userPhotoFragment;

    public NewUserPagerAdapter(FragmentManager fm) {
        super(fm);
        userFragment = new NewUserFragment();
        userPhotoFragment = new NewUserPhotoFragment();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return userFragment;
            case 1:
                return userPhotoFragment;
            default:
                return userFragment;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

}
