package br.com.felipeacerbi.collekt.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnCollectionClickListener;
import br.com.felipeacerbi.collekt.listeners.OnReactionClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.models.Reaction;
import br.com.felipeacerbi.collekt.models.User;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.CalendarUtils;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import br.com.felipeacerbi.collekt.utils.NumberUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Collection} and makes a call to the
 * specified {@link OnCollectionClickListener}.
 */
public class ReactionsListAdapter extends RecyclerView.Adapter<ReactionsListAdapter.ReactionViewHolder> {

    private List<Pair<String, Reaction>> mItems;
    private OnReactionClickListener mListener;

    private DataManager dataManager;

    public ReactionsListAdapter(OnReactionClickListener listener) {
        mItems = new ArrayList<>();
        mListener = listener;
        dataManager = new DataManager(mListener.getContext());
    }

    @Override
    public ReactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reaction_list_item, parent, false);
        return new ReactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ReactionViewHolder holder, int position) {
        holder.pair = mItems.get(position);

        dataManager.requestProfile(holder.pair.second.getReacter(), new RequestCallback<User>() {
            @Override
            public void onSuccess(User user) {
                holder.name.setText(user.getName());

                ImageLoader.load(
                        mListener.getContext(),
                        user.getPhotoPath(),
                        R.drawable.profile_image_placeholder,
                        holder.profile);
            }

            @Override
            public void onError(String error) {

            }
        });

        holder.comment.setText(holder.pair.second.getComment());
        holder.time.setText(CalendarUtils.convertDateTime(mListener.getContext(), holder.pair.second.getCreated()));

        boolean isLiked = holder.pair.second.getLikes().containsKey(dataManager.getCurrentUid());
        holder.likeButton.setImageResource(isLiked ? R.drawable.ic_favorite_black_24dp : R.drawable.ic_favorite_border_black_24dp);

        holder.likes.setText(String.format(Locale.getDefault(), holder.pair.second.getLikes().size() == 1 ? mListener.getContext().getString(R.string.likes_number_singular) : mListener.getContext().getString(R.string.likes_number_plural), NumberUtils.formatNumber(holder.pair.second.getLikes().size())));
        holder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean switchLike = !holder.pair.second.getLikes().containsKey(dataManager.getCurrentUid());
                holder.likeButton.setImageResource(switchLike ? R.drawable.ic_favorite_black_24dp : R.drawable.ic_favorite_border_black_24dp);
                dataManager.switchLikeReaction(holder.pair.first, switchLike);

                if(switchLike) {
                    holder.pair.second.getLikes().put(dataManager.getCurrentUid(), true);
                } else {
                    holder.pair.second.getLikes().remove(dataManager.getCurrentUid());
                }
                holder.likes.setText(String.format(Locale.getDefault(), holder.pair.second.getLikes().size() == 1 ? mListener.getContext().getString(R.string.likes_number_singular) : mListener.getContext().getString(R.string.likes_number_plural), NumberUtils.formatNumber(holder.pair.second.getLikes().size())));
            }
        });

        if(dataManager.isOwner(holder.pair.second.getReacter())) {
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    mListener.onReactionClicked(holder, true);
                    return true;
                }
            });
        } else {
            holder.itemView.setOnLongClickListener(null);
        }

        dataManager.requestCollectionItem(holder.pair.second.getItem(), new RequestCallback<CollectionItem>() {
            @Override
            public void onSuccess(CollectionItem collectionItem) {
                if(dataManager.isOwner(collectionItem.getOwner())) {
                    holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            mListener.onReactionClicked(holder, false);
                            return true;
                        }
                    });
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public void addItems(List<Pair<String, Reaction>> reactions) {
        mItems.clear();
        mItems.addAll(reactions);
        notifyDataSetChanged();
    }

    public List<Pair<String, Reaction>> getItems() {
        return mItems;
    }

    public void addItem(Pair<String, Reaction> reaction) {
        mItems.add(reaction);
        notifyItemInserted(mItems.indexOf(reaction));
    }

    public void removeItem(Pair<String, Reaction> item) {
        notifyItemRemoved(mItems.indexOf(item));
        mItems.remove(item);
    }

    public void editItem(Pair<String, Reaction> item) {
        mItems.set(mItems.indexOf(item), item);
        notifyItemChanged(mItems.indexOf(item));
    }

    public void checkEmpty() {
        if(mListener instanceof EmptyIconClickListener) {
            ((EmptyIconClickListener) mListener).checkEmpty();
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ReactionViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.poster_profile_photo)
        ImageView profile;
        @BindView(R.id.poster_name)
        TextView name;
        @BindView(R.id.comment_message)
        TextView comment;
        @BindView(R.id.comment_timestamp)
        TextView time;
        @BindView(R.id.comment_likes)
        TextView likes;
        @BindView(R.id.comment_like_button)
        ImageView likeButton;

        public Pair<String, Reaction> pair;

        ReactionViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
