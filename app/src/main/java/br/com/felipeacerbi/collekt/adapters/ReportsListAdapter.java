package br.com.felipeacerbi.collekt.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.CollectionActivity;
import br.com.felipeacerbi.collekt.activities.CollectionItemActivity;
import br.com.felipeacerbi.collekt.activities.ProfileActivity;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnReportClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.models.Report;
import br.com.felipeacerbi.collekt.models.User;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Report} and makes a call to the
 * specified {@link OnReportClickListener}.
 */
public class ReportsListAdapter extends RecyclerView.Adapter<ReportsListAdapter.ReportViewHolder> {

    private List<Pair<String, Report>> mItems;
    private OnReportClickListener mListener;

    private DataManager dataManager;

    public ReportsListAdapter(OnReportClickListener listener) {
        mItems = new ArrayList<>();
        mListener = listener;
        dataManager = new DataManager(mListener.getContext());
    }

    @Override
    public ReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_list_item, parent, false);
        return new ReportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ReportViewHolder holder, int position) {
        holder.pair = mItems.get(position);

        holder.type.setText(mListener.getContext().getResources().getStringArray(R.array.report_options)[holder.pair.second.getType()]);
        holder.message.setText(holder.pair.second.getReporterMessage());

        dataManager.requestProfile(holder.pair.second.getReporter(), new RequestCallback<User>() {
            @Override
            public void onSuccess(User user) {
                holder.name.setText(user.getName());

                ImageLoader.load(
                        mListener.getContext(),
                        user.getPhotoPath(),
                        R.drawable.profile_image_placeholder,
                        holder.profile);
            }

            @Override
            public void onError(String error) {

            }
        });

        dataManager.requestCollection(holder.pair.second.getReported(), new RequestCallback<Collection>() {
            @Override
            public void onSuccess(Collection collection) {
                if(holder.pair.second.getReportedExtra().isEmpty()) {
                    holder.reportedName.setText(collection.getName());
                    holder.extraName.setVisibility(View.GONE);

                    ImageLoader.load(
                            mListener.getContext(),
                            collection.getCoverPath(),
                            R.drawable.collection_placeholder,
                            holder.cover);

                    holder.cover.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent collectionIntent = new Intent(mListener.getContext(), CollectionActivity.class);
                            collectionIntent.putExtra(CollectionActivity.COLLECTION_EXTRA, holder.pair.second.getReported());
                            mListener.getContext().startActivity(collectionIntent);
                        }
                    });
                } else {
                    holder.extraName.setText(collection.getName());
                    holder.extraName.setVisibility(View.VISIBLE);
                    holder.extraName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent collectionIntent = new Intent(mListener.getContext(), CollectionActivity.class);
                            collectionIntent.putExtra(CollectionActivity.COLLECTION_EXTRA, holder.pair.second.getReported());
                            mListener.getContext().startActivity(collectionIntent);
                        }
                    });

                    holder.cover.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent collectionItemIntent = new Intent(mListener.getContext(), CollectionItemActivity.class);
                            collectionItemIntent.putExtra(CollectionItemActivity.COLLECTION_ITEM_EXTRA, holder.pair.second.getReportedExtra());
                            mListener.getContext().startActivity(collectionItemIntent);
                        }
                    });

                    dataManager.requestCollectionItem(holder.pair.second.getReportedExtra(), new RequestCallback<CollectionItem>() {
                        @Override
                        public void onSuccess(CollectionItem collectionItem) {
                            holder.reportedName.setText(collectionItem.getName());

                            ImageLoader.load(
                                    mListener.getContext(),
                                    collectionItem.getCoverPhoto(),
                                    R.drawable.collection_placeholder,
                                    holder.cover);
                        }

                        @Override
                        public void onError(String error) {

                        }
                    });
                }
            }

            @Override
            public void onError(String error) {

            }
        });

        holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(mListener.getContext(), ProfileActivity.class);
                profileIntent.putExtra(ProfileActivity.USER_EXTRA, holder.pair.second.getReporter());
                mListener.getContext().startActivity(profileIntent);
            }
        });

        holder.apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataManager.changeReportStatus(holder.pair.first, Report.REPORT_STATUS_APPLIED, holder.pair.second.getReported());
                notifyItemRemoved(mItems.indexOf(holder.pair));
                mItems.remove(holder.pair);
                checkEmpty();
            }
        });

        holder.ignore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataManager.changeReportStatus(holder.pair.first, Report.REPORT_STATUS_IGNORED, holder.pair.second.getReported());
                notifyItemRemoved(mItems.indexOf(holder.pair));
                mItems.remove(holder.pair);
                checkEmpty();
            }
        });
    }

    public void addItems(List<Pair<String, Report>> reports) {
        mItems = reports;
        notifyItemRangeInserted(0, reports.size());
    }

    public void checkEmpty() {
        if(mListener instanceof EmptyIconClickListener) {
            ((EmptyIconClickListener) mListener).checkEmpty();
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ReportViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_report_type)
        TextView type;
        @BindView(R.id.civ_profile_picture)
        CircleImageView profile;
        @BindView(R.id.tv_profile_name)
        TextView name;
        @BindView(R.id.tv_reporter_message)
        TextView message;
        @BindView(R.id.iv_reported_cover)
        ImageView cover;
        @BindView(R.id.tv_reported_name)
        TextView reportedName;
        @BindView(R.id.tv_extra_name)
        TextView extraName;
        @BindView(R.id.bt_apply)
        TextView apply;
        @BindView(R.id.bt_ignore)
        TextView ignore;

        Pair<String, Report> pair;

        ReportViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
