package br.com.felipeacerbi.collekt.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;

import java.util.List;

public class SpinnerItemsAdapter<T> extends ArrayAdapter<T> {

    private final boolean wrapSizeToSelection;

    public SpinnerItemsAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
        this.wrapSizeToSelection = false;
    }

    public SpinnerItemsAdapter(Context context, int resource, List<T> objects, boolean wrapSizeToSelection) {
        super(context, resource, objects);
        this.wrapSizeToSelection = wrapSizeToSelection;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if(wrapSizeToSelection) {
            return super.getView(((Spinner) parent).getSelectedItemPosition(), convertView, parent);
        }
        return super.getView(position, convertView, parent);
    }
}