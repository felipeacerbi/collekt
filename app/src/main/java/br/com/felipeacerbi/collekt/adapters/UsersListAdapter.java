package br.com.felipeacerbi.collekt.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.OnCollectionClickListener;
import br.com.felipeacerbi.collekt.listeners.OnUserClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.User;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Collection} and makes a call to the
 * specified {@link OnCollectionClickListener}.
 */
public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.UserViewHolder> {

    private List<Pair<String, User>> mItems;

    private DataManager dataManager;
    private OnUserClickListener mListener;

    public UsersListAdapter(OnUserClickListener listener, List<Pair<String, User>> users) {
        mListener = listener;
        mItems = users;
        dataManager = new DataManager(listener.getContext());
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_item, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, int position) {
        holder.pair = mItems.get(position);

        dataManager.requestProfile(holder.pair.first, new RequestCallback<User>() {
            @Override
            public void onSuccess(User user) {
                holder.name.setText(user.getName());

                ImageLoader.load(
                        mListener.getContext(),
                        user.getPhotoPath(),
                        R.drawable.profile_image_placeholder,
                        holder.profile);
            }

            @Override
            public void onError(String error) {

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onUserClicked(holder);
            }
        });
    }

    public void addItems(List<Pair<String, User>> users) {
        mItems = users;
        notifyItemRangeInserted(0, users.size());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.civ_profile_picture)
        ImageView profile;
        @BindView(R.id.tv_profile_name)
        TextView name;

        public Pair<String, User> pair;

        UserViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
