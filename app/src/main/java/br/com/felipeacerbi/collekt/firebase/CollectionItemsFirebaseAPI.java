package br.com.felipeacerbi.collekt.firebase;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.CollectionItem;

// API for CollectionItem Database
class CollectionItemsFirebaseAPI {

   private static final String DATABASE_COLLECTION_ITEMS_PATH = "items/";
   private static final String STORAGE_COLLECTION_ITEMS_PATH = "items";

   private FirebaseManager firebaseManager;

   public CollectionItemsFirebaseAPI(FirebaseManager firebaseManager) {
       this.firebaseManager = firebaseManager;
   }

   void getCollectionItemInfo(final Context context, String key, final RequestCallback<CollectionItem> callback) {
       getCollectionItemsReference()
               .orderByKey()
               .equalTo(key)
               .addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                           for (DataSnapshot collectionItemSnapshot : dataSnapshot.getChildren()) {
                               CollectionItem collectionItem = new CollectionItem(collectionItemSnapshot);
                               callback.onSuccess(collectionItem);
                           }
                       } else {
                           callback.onError(context.getString(R.string.item_not_found));
                       }
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {
                       callback.onError(context.getString(R.string.collection_item_canceled));
                   }
               });
   }

   void addCollectionItem(String itemKey, CollectionItem collectionItem) {
       collectionItem.setCreated(Calendar.getInstance().getTimeInMillis());

       Map<String, Object> childUpdates = new HashMap<>();
       childUpdates.put(getCollectionItemPath(itemKey), collectionItem.toMap());

       firebaseManager.addCollectionItemToCollection(collectionItem.getParent(), itemKey);
       firebaseManager.updateDatabase(childUpdates);
   }

   void updateCollectionItem(String key, final CollectionItem collectionItem) {

       getCollectionItemsReference()
               .orderByKey()
               .equalTo(key)
               .addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                           for (DataSnapshot collectionSnapshot : dataSnapshot.getChildren()) {

                               String key = collectionSnapshot.getKey();

                               Map<String, Object> childUpdates = new HashMap<>();
                               childUpdates.put(getCollectionItemPath(key) + CollectionItem.DATABASE_NAME_CHILD, collectionItem.getName());
                               childUpdates.put(getCollectionItemPath(key) + CollectionItem.DATABASE_DESC_CHILD, collectionItem.getDescription());
                               childUpdates.put(getCollectionItemPath(key) + CollectionItem.DATABASE_COVER_CHILD, collectionItem.getCoverPhoto());
                               childUpdates.put(getCollectionItemPath(key) + CollectionItem.DATABASE_ORIGIN_CHILD, collectionItem.getOrigin());
                               childUpdates.put(getCollectionItemPath(key) + CollectionItem.DATABASE_RARITY_CHILD, collectionItem.getRarity());
                               childUpdates.put(getCollectionItemPath(key) + CollectionItem.DATABASE_VALUE_CHILD, collectionItem.getValue());
                               childUpdates.put(getCollectionItemPath(key) + CollectionItem.DATABASE_MISSING_CHILD, collectionItem.isMissing());
                               childUpdates.put(getCollectionItemPath(key) + CollectionItem.DATABASE_EXTRA_CHILD, collectionItem.getExtraPhotoPaths());

                               firebaseManager.updateDatabase(childUpdates);
                           }

                       }
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }
               });
   }

    String getNewKey() {
        return getCollectionItemsReference().push().getKey();
    }

   void deleteCollectionItem(String itemKey, String collectionKey) {
       firebaseManager.deleteCollectionItemFromCollection(collectionKey, itemKey);
       getCollectionItemReference(itemKey).setValue(null);
   }

    void addReaction(String reactionKey, String itemKey) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(getCollectionItemPath(itemKey) + CollectionItem.DATABASE_REACTIONS_CHILD + "/" + reactionKey, true);

        firebaseManager.updateDatabase(childUpdates);
    }

    void deleteReaction(String reactionKey, String itemKey) {
        getCollectionItemReference(itemKey).child(CollectionItem.DATABASE_REACTIONS_CHILD).child(reactionKey).setValue(null);
    }

   private String getCollectionItemPath(String key) {
       return DATABASE_COLLECTION_ITEMS_PATH + key + "/";
   }

   public DatabaseReference getCollectionItemsReference() {
       return firebaseManager.getDatabaseReference(DATABASE_COLLECTION_ITEMS_PATH);
   }

   public DatabaseReference getCollectionItemReference(String key) {
       return getCollectionItemsReference().child(key);
   }

   private StorageReference getCollectionItemsStorageReference() {
       return firebaseManager.getStorageReference(STORAGE_COLLECTION_ITEMS_PATH);
   }

   private StorageReference getCollectionItemStorageReference(String key) {
       return getCollectionItemsStorageReference().child(key);
   }

   void uploadCollectionItemPhoto(Context context, String key, Uri path,
                                  @Nullable OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                                  @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                                  @Nullable OnFailureListener failureListener) {
       firebaseManager.uploadPhoto(context, getCollectionItemStorageReference(key), path, successListener, progressListener, failureListener);
   }
}
