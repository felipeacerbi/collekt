package br.com.felipeacerbi.collekt.firebase;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;

// API for Collection Database
class CollectionsFirebaseAPI {

   private static final String DATABASE_COLLECTIONS_PATH = "collections/";
   private static final String STORAGE_COLLECTIONS_PATH = "collections";
    private static final String DATABASE_FEATURED_COLLECTIONS_PATH = "featured/";

   private FirebaseManager firebaseManager;

   public CollectionsFirebaseAPI(FirebaseManager firebaseManager) {
       this.firebaseManager = firebaseManager;
   }

   void getCollectionInfo(final Context context, String key, final RequestCallback<Collection> callback) {
       getCollectionsReference()
               .orderByKey()
               .equalTo(key)
               .addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                           for (DataSnapshot collectionSnapshot : dataSnapshot.getChildren()) {
                               Collection collection = new Collection(collectionSnapshot);
                               callback.onSuccess(collection);
                           }
                       } else {
                           callback.onError(context.getString(R.string.collection_not_found));
                       }
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {
                       callback.onError(context.getString(R.string.collection_canceled));
                   }
               });
   }

   void addCollection(String key, Collection collection) {
       collection.setOwner(firebaseManager.getCurrentUid());
       collection.setCreated(Calendar.getInstance().getTimeInMillis());

       Map<String, Object> childUpdates = new HashMap<>();
       childUpdates.put(getCollectionPath(key), collection.toMap());

       firebaseManager.addCollectionToUser(key);
       firebaseManager.updateDatabase(childUpdates);
   }

    void addView(String key) {
       getCollectionReference(key).child(Collection.DATABASE_VIEWS_CHILD).runTransaction(new Transaction.Handler() {
           @Override
           public Transaction.Result doTransaction(MutableData mutableData) {
               long views = (long) mutableData.getValue();
               views++;
               mutableData.setValue(views);
               return Transaction.success(mutableData);
           }

           @Override
           public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                // No need
           }
       });
    }

   void updateCollection(String key, final Collection collection) {

       getCollectionsReference()
               .orderByKey()
               .equalTo(key)
               .addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                           for (DataSnapshot collectionSnapshot : dataSnapshot.getChildren()) {

                               String key = collectionSnapshot.getKey();

                               Map<String, Object> childUpdates = new HashMap<>();
                               childUpdates.put(getCollectionPath(key) + Collection.DATABASE_NAME_CHILD, collection.getName());
                               childUpdates.put(getCollectionPath(key) + Collection.DATABASE_COVER_CHILD, collection.getCoverPath());
                               childUpdates.put(getCollectionPath(key) + Collection.DATABASE_PUBLIC_CHILD, collection.isPublic());

                               firebaseManager.updateDatabase(childUpdates);
                           }

                       }
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }
               });
   }

   String getNewKey() {
       return getCollectionsReference().push().getKey();
   }

   void deleteCollection(String collectionKey) {
       firebaseManager.deleteCollectionFromUser(collectionKey);
       getCollectionReference(collectionKey).setValue(null);
   }

    void addCollectionItem(String collectionKey, String itemKey) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(getCollectionPath(collectionKey) + Collection.DATABASE_ITEMS_CHILD + "/" + itemKey, true);

        firebaseManager.updateDatabase(childUpdates);
    }

   void deleteCollectionItem(String collectionKey, String itemKey) {
       getCollectionReference(collectionKey).child(Collection.DATABASE_ITEMS_CHILD).child(itemKey).setValue(null);
   }

   void switchFollowing(String collectionKey, boolean add) {
       if(add) {
           Map<String, Object> childUpdates = new HashMap<>();
           childUpdates.put(getCollectionPath(collectionKey) + Collection.DATABASE_FOLLOWERS_CHILD + "/" + firebaseManager.getCurrentUid(), true);
           firebaseManager.updateDatabase(childUpdates);
       } else {
           getCollectionReference(collectionKey).child(Collection.DATABASE_FOLLOWERS_CHILD).child(firebaseManager.getCurrentUid()).setValue(null);
       }
   }

   void reportCollection(String key, String reportKey) {
       Map<String, Object> childUpdates = new HashMap<>();
       childUpdates.put(getCollectionPath(key) + Collection.DATABASE_REPORT_CHILD, reportKey);
       childUpdates.put(getCollectionPath(key) + Collection.DATABASE_PUBLIC_CHILD, false);
       firebaseManager.updateDatabase(childUpdates);
   }

   private String getCollectionPath(String key) {
       return DATABASE_COLLECTIONS_PATH + key + "/";
   }

   public DatabaseReference getCollectionsReference() {
       return firebaseManager.getDatabaseReference(DATABASE_COLLECTIONS_PATH);
   }

    public DatabaseReference getFeaturedCollectionsReference() {
        return firebaseManager.getDatabaseReference(DATABASE_FEATURED_COLLECTIONS_PATH);
    }

   public DatabaseReference getCollectionReference(String key) {
       return getCollectionsReference().child(key);
   }

   private StorageReference getCollectionsStorageReference() {
       return firebaseManager.getStorageReference(STORAGE_COLLECTIONS_PATH);
   }

   private StorageReference getCollectionStorageReference(String key) {
       return getCollectionsStorageReference().child(key);
   }

   void uploadCollectionPhoto(Context context, String key, Uri path,
                              @Nullable OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                              @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                              @Nullable OnFailureListener failureListener) {
       firebaseManager.uploadPhoto(context, getCollectionStorageReference(key), path, successListener, progressListener, failureListener);
   }
}
