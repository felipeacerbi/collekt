package br.com.felipeacerbi.collekt.firebase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.exifinterface.media.ExifInterface;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.parceler.Parcels;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.LoginActivity;
import br.com.felipeacerbi.collekt.activities.MainActivity;
import br.com.felipeacerbi.collekt.activities.NewUserActivity;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.AdItem;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.models.FilterCollections;
import br.com.felipeacerbi.collekt.models.Reaction;
import br.com.felipeacerbi.collekt.models.Report;
import br.com.felipeacerbi.collekt.models.User;

public class FirebaseManager extends FirebaseInstanceIdService {

    private static FirebaseManager firebaseManager;

    FirebaseDatabase firebaseDB;
    FirebaseAuth firebaseAuth;
    FirebaseInstanceId firebaseInstanceID;
    FirebaseStorage firebaseStorage;

    UsersFirebaseAPI usersAPI;
    CollectionsFirebaseAPI collectionsAPI;
    CollectionItemsFirebaseAPI collectionItemsAPI;
    ReactionsFirebaseAPI reactionsAPI;
    ReportsFirebaseAPI reportsAPI;

    private ValueEventListener reactionsListener;

    public static FirebaseManager getInstance() {
        if(firebaseManager == null) {
            firebaseManager = new FirebaseManager();
        }
        return firebaseManager;
    }

    public FirebaseManager() {
        startFirebase();
    }

    private void startFirebase() {
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        firebaseDB = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseInstanceID = FirebaseInstanceId.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();

        usersAPI = new UsersFirebaseAPI(this);
        collectionsAPI = new CollectionsFirebaseAPI(this);
        collectionItemsAPI = new CollectionItemsFirebaseAPI(this);
        reactionsAPI = new ReactionsFirebaseAPI(this);
        reportsAPI = new ReportsFirebaseAPI(this);
    }

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        if(getCurrentUid() != null) {
            usersAPI.getUserInfo(this, getCurrentUid(), new RequestCallback<User>() {
                @Override
                public void onSuccess(User object) {
                    usersAPI.updateUserIdToken();
                }

                @Override
                public void onError(String error) {
                    Log.w(getClass().getName(), "User not found on token refresh: " + error);
                }
            });
        }
    }

    public FirebaseAuth getAuth() {
        return FirebaseAuth.getInstance();
    }

    DatabaseReference getDatabaseReference(String path) {
        return firebaseDB.getReference(path);
    }

    StorageReference getStorageReference(String path) {
        return firebaseStorage.getReference(path);
    }

    StorageReference getStorageReferenceUrl(Context context, String path) {
        if(path != null && !path.isEmpty()) {
            try {
                return FirebaseStorage.getInstance().getReferenceFromUrl(path);
            } catch (IllegalArgumentException exception) {
                Log.w(getClass().getSimpleName(), context.getString(R.string.photo_not_on_storage));
            }
        }

        return null;
    }

    void updateDatabase(Map<String, Object> childUpdates) {
        getDatabaseReference("").updateChildren(childUpdates);
    }

    public String getAppIdToken() {
        return firebaseInstanceID.getToken();
    }

    void uploadPhoto(Context context, final StorageReference reference, Uri path,
                     @Nullable final OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                     @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                     @Nullable OnFailureListener failureListener) {
        try {
            if(path.getLastPathSegment() != null) {
                UploadTask uploadTask = reference.child(path.getLastPathSegment()).putBytes(prepareImage(context, path));
                uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return reference.getDownloadUrl();
                    }
                });
                if (successListener != null) uploadTask.addOnSuccessListener(successListener);
                if (progressListener != null) uploadTask.addOnProgressListener(progressListener);
                if (failureListener != null) uploadTask.addOnFailureListener(failureListener);
            } else {
                Toast.makeText(context, R.string.fail_photo_upload_message, Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
    }

    private byte[] prepareImage(Context context, Uri path) throws IOException {
        InputStream inputStream = context.getContentResolver().openInputStream(path);
        String orientation = null;

        if(inputStream != null) {
            ExifInterface exifInterface = new ExifInterface(inputStream);
            orientation = exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION);
            inputStream.close();
        }

        Matrix matrix = new Matrix();

        if(orientation != null) {

            if (orientation.equals(String.valueOf(ExifInterface.ORIENTATION_NORMAL))) {
                // Do nothing. The original image is fine.
            } else if (orientation.equals(String.valueOf(ExifInterface.ORIENTATION_ROTATE_90))) {
                matrix.postRotate(90);
            } else if (orientation.equals(String.valueOf(ExifInterface.ORIENTATION_ROTATE_180))) {
                matrix.postRotate(180);
            } else if (orientation.equals(String.valueOf(ExifInterface.ORIENTATION_ROTATE_270))) {
                matrix.postRotate(270);
            }
        }

        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), path);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);

        byte[] bytes = outputStream.toByteArray();
        outputStream.flush();
        outputStream.close();

        return bytes;
    }

    public void getCollectionsList(final Context context, String userKey, final int filter, final String query, final RequestCallback<List<Pair<String, Collection>>> collectionsRequest) {
        Query returnQuery = null;

        switch (filter) {
            case FilterCollections.FILTER_QUERY:
                returnQuery = filterCollections(filter, Collection.DATABASE_VIEWS_CHILD, "");
                break;
            case FilterCollections.FILTER_OWNED:
                returnQuery = filterCollections(filter, Collection.DATABASE_OWNER_CHILD, userKey);
                break;
            case FilterCollections.FILTER_FOLLOWING:
                returnQuery = filterCollections(filter, User.DATABASE_FOLLOWING_CHILD, userKey);
                break;
            case FilterCollections.FILTER_TRENDING:
                returnQuery = filterCollections(filter, Collection.DATABASE_VIEWS_CHILD, "");
                break;
            case FilterCollections.FILTER_NEWEST:
                returnQuery = filterCollections(filter, Collection.DATABASE_CREATED_CHILD, "");
                break;
        }

        if(returnQuery != null) {
            returnQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(final DataSnapshot dataSnapshot) {
                    final List<Pair<String, Collection>> collections = new ArrayList<>();

                    if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {

                        if (filter == FilterCollections.FILTER_FOLLOWING) {
                            final int[] i = {0};

                            for (DataSnapshot collectionSnapshot : dataSnapshot.getChildren()) {

                                final String key = collectionSnapshot.getKey();

                                requestCollection(context, key, new RequestCallback<Collection>() {
                                    @Override
                                    public void onSuccess(Collection collection) {
                                        collections.add(new Pair<>(key, collection));

                                        i[0]++;
                                        if (i[0] == dataSnapshot.getChildrenCount()) {
                                            collectionsRequest.onSuccess(collections);
                                        }
                                    }

                                    @Override
                                    public void onError(String error) {
                                        collectionsRequest.onError(context.getString(R.string.collection_load_error) + error);
                                    }
                                });
                            }
                        } else {
                            for (DataSnapshot collectionSnapshot : dataSnapshot.getChildren()) {
                                final String key = collectionSnapshot.getKey();
                                Collection collection = new Collection(collectionSnapshot);

                                if (queryCollection(query, collection, filter == FilterCollections.FILTER_OWNED || filter == FilterCollections.FILTER_QUERY)) {
                                    collections.add(new Pair<>(key, collection));
                                }
                            }
                            collectionsRequest.onSuccess(collections);
                        }
                    } else {
                        collectionsRequest.onSuccess(collections);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    collectionsRequest.onError(context.getString(R.string.collections_load_error) + databaseError);
                }
            });
        } else {
            collectionsRequest.onError(context.getString(R.string.collections_load_error_query_null));
        }
    }

    private boolean queryCollection(String query, Collection collection, boolean includeOwned) {
        return (query.isEmpty() && (collection.getOwner().equals(getCurrentUid()) && includeOwned || collection.isPublic())) ||
                (collection.getName().toLowerCase().contains(query.toLowerCase()) && (collection.getOwner().equals(getCurrentUid()) && includeOwned || collection.isPublic()));
    }

    private Query filterCollections(int filter, String childReference, String extra) {
        Query filtered = collectionsAPI.getCollectionsReference();

        switch (filter) {
            case FilterCollections.FILTER_QUERY:
                filtered = filtered.orderByChild(childReference);
                break;
            case FilterCollections.FILTER_OWNED:
                filtered = filtered.orderByChild(childReference).equalTo(extra);
                break;
            case FilterCollections.FILTER_FOLLOWING:
                filtered = usersAPI.getUserReference(extra).child(childReference).orderByKey();
                break;
            case FilterCollections.FILTER_TRENDING:
            case FilterCollections.FILTER_NEWEST:
                filtered = filtered.orderByChild(childReference).limitToLast(20);
                break;
        }

        return filtered;
    }
    public void getFeaturedCollectionsList(final Context context, final RequestCallback<List<AdItem>> adsRequest) {
        collectionsAPI.getFeaturedCollectionsReference().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<AdItem> adItems = new ArrayList<>();

                if(dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot adItemSnapshot : dataSnapshot.getChildren()) {
                        AdItem adItem = new AdItem(adItemSnapshot);
                        adItems.add(adItem);
                    }
                    adsRequest.onSuccess(adItems);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                adsRequest.onError(context.getString(R.string.ad_item_load_fail) + databaseError);
            }
        });

    }

    public void getCollectionItemsList(final Context context, String collectionKey, final RequestCallback<List<Pair<String, CollectionItem>>> collectionItemsRequest) {
        collectionItemsAPI
                .getCollectionItemsReference()
                .orderByChild(CollectionItem.DATABASE_PARENT_CHILD)
                .equalTo(collectionKey)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Pair<String, CollectionItem>> collectionItems = new ArrayList<>();

                if(dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot collectionItemSnapshot : dataSnapshot.getChildren()) {

                        CollectionItem collectionItem = new CollectionItem(collectionItemSnapshot);
                        String key = collectionItemSnapshot.getKey();
                        collectionItems.add(new Pair<>(key, collectionItem));
                    }
                }
                collectionItemsRequest.onSuccess(collectionItems);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                collectionItemsRequest.onError(context.getString(R.string.collection_item_load_error) + databaseError);
            }
        });
    }

    public void getCollectionItemPhotos(final Context context, String collectionItemKey, final RequestCallback<List<String>> photosRequest) {
        requestCollectionItem(context, collectionItemKey, new RequestCallback<CollectionItem>() {
            @Override
            public void onSuccess(CollectionItem collectionItem) {
                photosRequest.onSuccess(collectionItem.getExtraPhotoPaths());
            }

            @Override
            public void onError(String error) {
                photosRequest.onError(context.getString(R.string.item_photos_load_error) + error);
            }
        });

    }

    public void getItemReactionsList(final Context context, String collectionItemKey, final RequestCallback<List<Pair<String, Reaction>>> reactionsRequest) {

        reactionsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Pair<String, Reaction>> reactions = new ArrayList<>();

                if(dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot reactionSnapshot : dataSnapshot.getChildren()) {

                        Reaction reaction = new Reaction(reactionSnapshot);
                        String key = reactionSnapshot.getKey();

                        reactions.add(new Pair<>(key, reaction));
                    }
                }
                reactionsRequest.onSuccess(reactions);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                reactionsRequest.onError(context.getString(R.string.reaction_load_fail) + databaseError);
            }
        };

        reactionsAPI.getReactionsReference()
                .orderByChild(Reaction.DATABASE_ITEM_CHILD)
                .equalTo(collectionItemKey)
                .addValueEventListener(reactionsListener);
    }

    public void getReportsList(final Context context, final RequestCallback<List<Pair<String, Report>>> reportsRequest) {
        reportsAPI.getReportsReference().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Pair<String, Report>> reports = new ArrayList<>();

                if(dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot reportSnapshot : dataSnapshot.getChildren()) {

                        Report report = new Report(reportSnapshot);
                        String key = reportSnapshot.getKey();

                        if(report.getStatus() == Report.REPORT_STATUS_PENDING) reports.add(new Pair<>(key, report));
                    }
                }
                reportsRequest.onSuccess(reports);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                reportsRequest.onError(context.getString(R.string.report_load_fail) + databaseError);
            }
        });
    }

    public void stopReactions() {
        if(reactionsListener != null) {
            reactionsAPI.getReactionsReference().removeEventListener(reactionsListener);
        }
    }

    public void getFollowersList(final Context context, String collectionKey, final RequestCallback<List<Pair<String, User>>> followersRequest) {

        collectionsAPI.getCollectionReference(collectionKey)
                .child(Collection.DATABASE_FOLLOWERS_CHILD)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {
                        final List<Pair<String, User>> users = new ArrayList<>();
                        final int[] i = {0};

                        if(dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {

                                final String key = userSnapshot.getKey();

                                requestUser(context, key, new RequestCallback<User>() {
                                    @Override
                                    public void onSuccess(User user) {
                                        users.add(new Pair<>(key, user));

                                        i[0]++;
                                        if (i[0] == dataSnapshot.getChildrenCount()) {
                                            followersRequest.onSuccess(users);
                                        }
                                    }

                                    @Override
                                    public void onError(String error) {
                                        followersRequest.onError(context.getString(R.string.user_load_error) + error);
                                    }
                                });
                            }
                        } else {
                            followersRequest.onSuccess(users);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        followersRequest.onError(context.getString(R.string.user_load_canceled) + databaseError);
                    }
                });
    }

    public void requestUser(Context context, String uid, RequestCallback<User> callback) {
        if(uid == null || uid.isEmpty()) uid = usersAPI.getCurrentUserUID();
        usersAPI.getUserInfo(context, uid, callback);
    }

    public void checkLogin(final Context context, boolean validateUser, final RequestCallback<Boolean> callback) {
        final FirebaseUser firebaseUser = usersAPI.getCurrentUser();

        if(firebaseUser != null) {
            if(validateUser) {
                final User newUser = new User(firebaseUser.getDisplayName(), "", firebaseUser.getEmail(), "");

                if (firebaseUser.getPhotoUrl() != null) {
                    newUser.setPhotoPath(firebaseUser.getPhotoUrl().toString());
                }

                requestUser(context, getCurrentUid(), new RequestCallback<User>() {
                    @Override
                    public void onSuccess(User user) {
                        onTokenRefresh();

                        Intent startMainActivity = new Intent(context, MainActivity.class);
                        context.startActivity(startMainActivity);

                        callback.onSuccess(true);
                    }

                    @Override
                    public void onError(String error) {
                        Intent startNewUserActivity = new Intent(context, NewUserActivity.class);
                        startNewUserActivity.putExtra(LoginActivity.USER_EXTRA, Parcels.wrap(newUser));
                        context.startActivity(startNewUserActivity);

                        callback.onSuccess(true);
                    }
                });
            } else {
                callback.onSuccess(true);
            }

        } else {
            callback.onSuccess(false);
        }
    }

    public boolean isUserAnonymous() {
        return usersAPI.isUserAnonymous();
    }

    public List<? extends UserInfo> getUserProviders() {
        return usersAPI.getUserProviders();
    }

    public void linkNewUserCredential(Activity activity, AuthCredential credential, RequestCallback<AuthResult> resultRequestCallback) {
        usersAPI.linkWithNewCredential(activity, credential, resultRequestCallback);
    }

    public void createUser(User user) {
        usersAPI.registerUser(user);
    }

    public void updateUserInfo(User user) {
        usersAPI.updateUser(user);
    }

    public void updateUserPhoto(Context context, Uri photo,
                                @Nullable OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                                @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                                @Nullable OnFailureListener failureListener) {
        usersAPI.uploadUserPhoto(context, photo, successListener, progressListener, failureListener);
    }

    public String getCurrentUid() {
        return usersAPI.getCurrentUserUID();
    }

    public void addCollectionToUser(String key) {
        usersAPI.addCollection(key);
    }

    public void deleteCollectionFromUser(String key) {
        usersAPI.deleteCollection(key);
    }

    public void requestCollection(Context context, String key, RequestCallback<Collection> callback) {
        collectionsAPI.getCollectionInfo(context, key, callback);
    }

    public String getNewCollectionKey() {
        return collectionsAPI.getNewKey();
    }

    public void createCollection(String key, Collection collection) {
        collectionsAPI.addCollection(key, collection);
    }

    public void deleteCollection(String key) {
        collectionsAPI.deleteCollection(key);
    }

    public void updateCollectionInfo(String key, Collection collection) {
        collectionsAPI.updateCollection(key, collection);
    }

    public void switchFollowing(String collectionKey, String ownerKey, boolean add) {
        collectionsAPI.switchFollowing(collectionKey, add);
        usersAPI.switchFollowing(collectionKey, ownerKey, add);
    }

    public void switchLike(String reactionKey, boolean add) {
        reactionsAPI.switchLike(reactionKey, add);
        usersAPI.switchLike(reactionKey, add);
    }

    public void addView(String collectionKey) {
        collectionsAPI.addView(collectionKey);
    }

    public void updateCollectionPhoto(Context context, String key, Uri photo,
                                      @Nullable OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                                      @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                                      @Nullable OnFailureListener failureListener) {
        collectionsAPI.uploadCollectionPhoto(context, key, photo, successListener, progressListener, failureListener);
    }

    public void addCollectionItemToCollection(String collectionKey, String itemKey) {
        collectionsAPI.addCollectionItem(collectionKey, itemKey);
    }

    public void deleteCollectionItemFromCollection(String collectionKey, String itemKey) {
        collectionsAPI.deleteCollectionItem(collectionKey, itemKey);
    }

    public void addReactionToCollectionItem(String reactionKey, String itemKey) {
        collectionItemsAPI.addReaction(reactionKey, itemKey);
    }

    public void deleteReactionFromCollectionItem(String reactionKey, String itemKey) {
        collectionItemsAPI.deleteReaction(reactionKey, itemKey);
    }

    public void requestCollectionItem(Context context, String key, RequestCallback<CollectionItem> callback) {
        collectionItemsAPI.getCollectionItemInfo(context, key, callback);
    }

    public String getNewCollectionItemKey() {
        return collectionItemsAPI.getNewKey();
    }

    public void createCollectionItem(String key, CollectionItem collectionItem) {
        collectionItemsAPI.addCollectionItem(key, collectionItem);
    }

    public void deleteCollectionItem(String itemKey, String collectionKey) {
        collectionItemsAPI.deleteCollectionItem(itemKey, collectionKey);
    }

    public void updateCollectionItemInfo(String key, CollectionItem collectionItem) {
        collectionItemsAPI.updateCollectionItem(key, collectionItem);
    }

    public void updateCollectionItemPhoto(Context context, String key, Uri photo,
                                          @Nullable OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                                          @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                                          @Nullable OnFailureListener failureListener) {
        collectionItemsAPI.uploadCollectionItemPhoto(context, key, photo, successListener, progressListener, failureListener);
    }

    public String getNewReactionKey() {
        return reactionsAPI.getNewKey();
    }

    public void createReaction(String key, Reaction reaction) {
        reactionsAPI.addReaction(key, reaction);
    }

    public void deleteReaction(String reactionKey, String collectionItemKey) {
        reactionsAPI.deleteReaction(reactionKey, collectionItemKey);
    }

    public void updateReactionInfo(String key, Reaction reaction) {
        reactionsAPI.updateReaction(key, reaction);
    }

    public void deletePhoto(Context context, String path) {
        StorageReference reference = getStorageReferenceUrl(context, path);
        if(reference != null) reference.delete();
    }

    public void requestReport(Context context, String key, RequestCallback<Report> callback) {
        reportsAPI.getReportInfo(context, key, callback);
    }

    public void createReport(Report report) {
        reportsAPI.addReport(report);
    }

    public void changeReportStatus(String key, int status, String collectionKey) {
        if(status == Report.REPORT_STATUS_APPLIED) collectionsAPI.reportCollection(collectionKey, key);
        reportsAPI.changeReportStatus(key, status);
    }
}
