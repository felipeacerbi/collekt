package br.com.felipeacerbi.collekt.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.CollectionActivity;
import br.com.felipeacerbi.collekt.activities.CollectionItemActivity;
import br.com.felipeacerbi.collekt.activities.MainActivity;

public class FirebaseMessaging extends FirebaseMessagingService {

    public static final String COLLECTION_ID_EXTRA = "collectionId";
    public static final String COLLECTION_NAME_EXTRA = "collection";
    public static final String FOLLOWER_NAME_EXTRA = "follower";
    public static final String ITEM_ID_EXTRA = "itemId";
    public static final String ITEM_NAME_EXTRA = "item";
    public static final String REACTER_NAME_EXTRA = "reacter";
    public static final String TYPE_EXTRA = "type";

    public static final String TYPE_FOLLOW = "follow";
    public static final String TYPE_REACTION = "reaction";

    private static final String FOLLOW_CHANNEL_ID = "follow_notifications";
    private static final String REACT_CHANNEL_ID = "reaction_notifications";

    private static final int FOLLOW_NOTI_ID = 0;
    private static final int REACT_NOTI_ID = 1;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String type = remoteMessage.getData().get(TYPE_EXTRA);

        switch (type) {
            case TYPE_FOLLOW:
                String collectionId = remoteMessage.getData().get(COLLECTION_ID_EXTRA);
                String collectionName = remoteMessage.getData().get(COLLECTION_NAME_EXTRA);
                String followerName = remoteMessage.getData().get(FOLLOWER_NAME_EXTRA);

                String followTitle = getString(R.string.follow_noti_title);
                String followMessage = String.format(getString(R.string.follow_noti_message), followerName, collectionName);

                createFollowNotification(followTitle, followMessage, collectionId);
                break;
            case TYPE_REACTION:
                String itemId = remoteMessage.getData().get(ITEM_ID_EXTRA);
                String itemName = remoteMessage.getData().get(ITEM_NAME_EXTRA);
                String reacterName = remoteMessage.getData().get(REACTER_NAME_EXTRA);

                String reactTitle = getString(R.string.reaction_noti_title);
                String reactMessage = String.format(getString(R.string.reaction_noti_message), reacterName, itemName);

                createReactNotification(reactTitle, reactMessage, itemId);
                break;
        }

    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotiChannel(String name, String description, String id) {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
        // Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.enableLights(true);
        // Sets the notification light color for notifications posted to this
        // channel, if the device supports this feature.
        mChannel.enableVibration(true);

        if (mNotificationManager != null) {
            mNotificationManager.createNotificationChannel(mChannel);
        }
    }

    private void createFollowNotification(String title, String message, String idExtra) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotiChannel(getString(R.string.follow_channel_name), getString(R.string.follow_channel_description), FOLLOW_CHANNEL_ID);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, FOLLOW_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setDefaults(Notification.DEFAULT_ALL);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, CollectionActivity.class);
        resultIntent.putExtra(CollectionActivity.COLLECTION_EXTRA, idExtra);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your app to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // mNotificationId is a unique integer your app uses to identify the
        // notification. For example, to cancel the notification, you can pass its ID
        // number to NotificationManager.cancel().
        if (mNotificationManager != null) {
            mNotificationManager.notify(FOLLOW_NOTI_ID, mBuilder.build());
        }
    }

    private void createReactNotification(String title, String message, String idExtra) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotiChannel(getString(R.string.reaction_channel_name), getString(R.string.reaction_channel_description), REACT_CHANNEL_ID);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, REACT_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_ALL);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, CollectionItemActivity.class);
        resultIntent.putExtra(CollectionItemActivity.COLLECTION_ITEM_EXTRA, idExtra);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your app to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        1,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // mNotificationId is a unique integer your app uses to identify the
        // notification. For example, to cancel the notification, you can pass its ID
        // number to NotificationManager.cancel().
        if (mNotificationManager != null) {
            mNotificationManager.notify(REACT_NOTI_ID, mBuilder.build());
        }
    }
}
