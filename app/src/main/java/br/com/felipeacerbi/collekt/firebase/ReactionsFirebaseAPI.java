package br.com.felipeacerbi.collekt.firebase;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Reaction;

// API for CollectionItem Database
class ReactionsFirebaseAPI {

   private static final String DATABASE_REACTIONS_PATH = "reactions/";

   private FirebaseManager firebaseManager;

   public ReactionsFirebaseAPI(FirebaseManager firebaseManager) {
       this.firebaseManager = firebaseManager;
   }

   void getReactionInfo(final Context context, String key, final RequestCallback<Reaction> callback) {
       getReactionReference(key).addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                           for (DataSnapshot reactionSnapshot : dataSnapshot.getChildren()) {
                               Reaction reaction = new Reaction(reactionSnapshot);
                               callback.onSuccess(reaction);
                           }
                       } else {
                           callback.onError(context.getString(R.string.reaction_not_found));
                       }
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {
                       callback.onError(context.getString(R.string.reaction_canceled));
                   }
               });
   }

   void addReaction(String key, Reaction reaction) {
       Map<String, Object> childUpdates = new HashMap<>();
       childUpdates.put(getReactionPath(key), reaction.toMap());

       firebaseManager.addReactionToCollectionItem(key, reaction.getItem());
       firebaseManager.updateDatabase(childUpdates);
   }

   void updateReaction(String key, final Reaction reaction) {

       getReactionsReference()
               .orderByKey()
               .equalTo(key)
               .addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                           for (DataSnapshot reactionSnapshot : dataSnapshot.getChildren()) {

                               String key = reactionSnapshot.getKey();

                               Map<String, Object> childUpdates = new HashMap<>();
                               childUpdates.put(getReactionPath(key) + Reaction.DATABASE_COMMENT_CHILD, reaction.getComment());

                               firebaseManager.updateDatabase(childUpdates);
                           }

                       }
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }
               });
   }

    String getNewKey() {
        return getReactionsReference().push().getKey();
    }

   void deleteReaction(String reactionKey, String collectionItemKey) {
       firebaseManager.deleteReactionFromCollectionItem(reactionKey, collectionItemKey);
       getReactionReference(reactionKey).setValue(null);
   }

    void switchLike(String reactionKey, boolean add) {
        if(add) {
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(getReactionPath(reactionKey) + Reaction.DATABASE_LIKES_CHILD + "/" + firebaseManager.getCurrentUid(), true);
            firebaseManager.updateDatabase(childUpdates);
        } else {
            getReactionReference(reactionKey).child(Reaction.DATABASE_LIKES_CHILD).child(firebaseManager.getCurrentUid()).setValue(null);
        }
    }

   private String getReactionPath(String key) {
       return DATABASE_REACTIONS_PATH + key + "/";
   }

   public DatabaseReference getReactionsReference() {
       return firebaseManager.getDatabaseReference(DATABASE_REACTIONS_PATH);
   }

   public DatabaseReference getReactionReference(String key) {
       return getReactionsReference().child(key);
   }
}
