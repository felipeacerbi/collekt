package br.com.felipeacerbi.collekt.firebase;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Report;

public class ReportsFirebaseAPI {

    private static final String DATABASE_REPORTS_PATH = "reports/";

    private FirebaseManager firebaseManager;

    public ReportsFirebaseAPI(FirebaseManager firebaseManager) {
        this.firebaseManager = firebaseManager;
    }

    void getReportInfo(final Context context, String key, final RequestCallback<Report> callback) {
        getReportsReference()
                .orderByKey()
                .equalTo(key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot reportSnapshot : dataSnapshot.getChildren()) {
                                Report report = new Report(reportSnapshot);
                                callback.onSuccess(report);
                            }
                        } else {
                            callback.onError(context.getString(R.string.report_not_found));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.onError(context.getString(R.string.report_canceled));
                    }
                });
    }

    void addReport(Report report) {
        report.setCreated(Calendar.getInstance().getTimeInMillis());
        report.setStatus(Report.REPORT_STATUS_PENDING);

        String key = getReportsReference().push().getKey();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(getReportPath(key), report.toMap());

        firebaseManager.updateDatabase(childUpdates);
    }

    void changeReportStatus(String key, int status) {
        getReportReference(key).child(Report.DATABASE_STATUS_CHILD).setValue(status);
    }

    public DatabaseReference getReportsReference() {
        return firebaseManager.getDatabaseReference(DATABASE_REPORTS_PATH);
    }

    public DatabaseReference getReportReference(String key) {
        return getReportsReference().child(key);
    }

    private String getReportPath(String key) {
        return DATABASE_REPORTS_PATH + key + "/";
    }
}
