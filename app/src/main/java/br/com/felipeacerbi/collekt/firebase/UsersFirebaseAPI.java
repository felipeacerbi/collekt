package br.com.felipeacerbi.collekt.firebase;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.User;

// API for Users Database
class UsersFirebaseAPI {

    private static final String DATABASE_USERS_PATH = "users/";
    private static final String STORAGE_USERS_PATH = "users";

    private FirebaseManager firebaseManager;

    public UsersFirebaseAPI(FirebaseManager firebaseManager) {
        this.firebaseManager = firebaseManager;
    }

    void getUserInfo(final Context context, String key, final RequestCallback<User> callback) {
        getUsersReference()
                .orderByKey()
                .equalTo(key.trim())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                                User user = new User(userSnapshot);
                                callback.onSuccess(user);
                            }
                        } else {
                            callback.onError(context.getString(R.string.user_not_found));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.onError(context.getString(R.string.user_load_canceled));
                    }
                });
    }

    boolean isUserAnonymous() {
        return firebaseManager.firebaseAuth.getCurrentUser().isAnonymous();
    }

    List<? extends UserInfo> getUserProviders() {
        return getCurrentUser().getProviderData();
    }

    void linkWithNewCredential(final Activity activity, AuthCredential credential, final RequestCallback<AuthResult> resultRequestCallback) {
        getCurrentUser().linkWithCredential(credential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            resultRequestCallback.onSuccess(task.getResult());
                        } else {
                            resultRequestCallback.onError(activity.getString(R.string.account_link_fail) + (task.getException() != null ? task.getException().getMessage() : ""));
                        }
                    }
                });
    }

    void registerUser(User user) {
        user.setIdToken(firebaseManager.getAppIdToken());
        user.setCreated(Calendar.getInstance().getTimeInMillis());

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(getUserPath(getCurrentUserUID()), user.toMap());

        firebaseManager.updateDatabase(childUpdates);
    }

    void updateUser(User user) {
        user.setIdToken(firebaseManager.getAppIdToken());

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(getUserPath(getCurrentUserUID()) + User.DATABASE_IDTOKEN_CHILD, user.getIdToken());
        childUpdates.put(getUserPath(getCurrentUserUID()) + User.DATABASE_NAME_CHILD, user.getName());
        childUpdates.put(getUserPath(getCurrentUserUID()) + User.DATABASE_DESC_CHILD, user.getDescription());
        childUpdates.put(getUserPath(getCurrentUserUID()) + User.DATABASE_EMAIL_CHILD, user.getEmail());
        childUpdates.put(getUserPath(getCurrentUserUID()) + User.DATABASE_PHOTO_CHILD, user.getPhotoPath());

        firebaseManager.updateDatabase(childUpdates);
    }

    void updateUserIdToken() {
        if(getCurrentUser() != null) {
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(getUserPath(getCurrentUserUID()) + User.DATABASE_IDTOKEN_CHILD, firebaseManager.getAppIdToken());

            firebaseManager.updateDatabase(childUpdates);
        }
    }

    void addCollection(String collectionKey) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(getUserPath(getCurrentUserUID()) + User.DATABASE_OWNED_CHILD + "/" + collectionKey, true);

        firebaseManager.updateDatabase(childUpdates);
    }

    void deleteCollection(String key) {
        getUserReference(getCurrentUserUID()).child(User.DATABASE_OWNED_CHILD).child(key).setValue(null);
    }

    void switchFollowing(String collectionKey, String ownerKey, boolean add) {
        if(add) {
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(getUserPath(getCurrentUserUID()) + User.DATABASE_FOLLOWING_CHILD + "/" + collectionKey, true);
            firebaseManager.updateDatabase(childUpdates);
        } else {
            getUserReference(getCurrentUserUID()).child(User.DATABASE_FOLLOWING_CHILD).child(collectionKey).setValue(null);
        }
        changeFollowers(ownerKey, add);
    }

    void switchLike(String reactionKey, boolean add) {
        if(add) {
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(getUserPath(getCurrentUserUID()) + User.DATABASE_LIKES_CHILD + "/" + reactionKey, true);
            firebaseManager.updateDatabase(childUpdates);
        } else {
            getUserReference(getCurrentUserUID()).child(User.DATABASE_LIKES_CHILD).child(reactionKey).setValue(null);
        }
    }

    private void changeFollowers(String userKey, final boolean add) {
        getUserReference(userKey).child(User.DATABASE_FOLLOWERS_CHILD).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if(mutableData.getValue() == null) {
                    return Transaction.success(mutableData);
                }

                long followers = (long) mutableData.getValue();

                if(add) followers++; else followers--;
                mutableData.setValue(followers);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                // No need
            }
        });
    }

    private String getUserPath(String key) {
        return DATABASE_USERS_PATH + key + "/";
    }

    public DatabaseReference getUsersReference() {
        return firebaseManager.getDatabaseReference(DATABASE_USERS_PATH);
    }

    public DatabaseReference getUserReference(String key) {
        return getUsersReference().child(key);
    }

    public FirebaseUser getCurrentUser() {
        return firebaseManager.firebaseAuth.getCurrentUser();
    }

    String getCurrentUserUID() {
        return getCurrentUser() == null ? null : getCurrentUser().getUid();
    }

    private StorageReference getUsersStorageReference() {
        return firebaseManager.getStorageReference(STORAGE_USERS_PATH);
    }

    private StorageReference getUserStorageReference(String uid) {
        return getUsersStorageReference().child(uid);
    }

    void uploadUserPhoto(Context context, Uri path,
                         @Nullable OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                         @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                         @Nullable OnFailureListener failureListener) {
        firebaseManager.uploadPhoto(context, getUserStorageReference(getCurrentUserUID()), path, successListener, progressListener, failureListener);
    }
}
