package br.com.felipeacerbi.collekt.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;

import org.parceler.Parcels;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.CollectionActivity;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.AdItem;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;

public class AdFragment extends Fragment {

    public static final String ARG_ITEM = "adItem";

    @BindView(R.id.iv_explore_cover)
    ImageView adCover;
    @BindView(R.id.tv_ads)
    TextView adText;

    @State(Collection.class)
    Collection currentCollection;

    private AdItem adItem;
    private DataManager dataManager;

    public static AdFragment newInstance(AdItem item) {
        AdFragment fragment = new AdFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ITEM, Parcels.wrap(item));
        fragment.setArguments(args);
        return fragment;
    }

    public AdFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);

        dataManager = new DataManager(getActivity());

        if (getArguments() != null) {
            adItem = Parcels.unwrap(getArguments().getParcelable(ARG_ITEM));
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ad, container, false);
        ButterKnife.bind(this, view);

        if(currentCollection != null) {
            setUpCollection();
        } else {
            dataManager.requestCollection(adItem.getId(), new RequestCallback<Collection>() {
                @Override
                public void onSuccess(Collection collection) {
                    currentCollection = collection;
                    setUpCollection();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(getActivity(), getString(R.string.collection_load_error) + error, Toast.LENGTH_SHORT).show();
                }
            });
        }

        adText.setText(adItem.getText());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            dataManager.addView(adItem.getId());

            Pair coverPair = Pair.create(adCover, "cover");

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    getActivity(),
                    coverPair);

            Intent startCollectionActivity = new Intent(getActivity(), CollectionActivity.class);
            startCollectionActivity.putExtra(CollectionActivity.COLLECTION_EXTRA, adItem.getId());

            ActivityCompat.startActivity(getActivity(), startCollectionActivity, options.toBundle());
            }
        });

        return view;
    }

    private void setUpCollection() {
        ImageLoader.load(
                getActivity(),
                currentCollection.getCoverPath(),
                R.drawable.collection_placeholder,
                adCover);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}
