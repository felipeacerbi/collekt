package br.com.felipeacerbi.collekt.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.CollectionActivity;
import br.com.felipeacerbi.collekt.activities.ProfileActivity;
import br.com.felipeacerbi.collekt.adapters.CollectionsAdapter;
import br.com.felipeacerbi.collekt.adapters.CollectionsOtherAdapter;
import br.com.felipeacerbi.collekt.listeners.ActionModeListener;
import br.com.felipeacerbi.collekt.listeners.ActionModeProvider;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnCollectionClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.listeners.SwipeRefreshProvider;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.CollectionsArrayList;
import br.com.felipeacerbi.collekt.models.FilterCollections;
import br.com.felipeacerbi.collekt.repository.DataManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;

public class CollectionListFragment extends Fragment implements OnCollectionClickListener, ActionModeProvider, ActionModeListener, EmptyIconClickListener {

    public static final String ARG_KEY = "key";
    public static final String ARG_COLUMN_COUNT = "column_count";
    public static final String ARG_IS_NESTED = "nested";
    public static final String ARG_IS_EXPLORE = "explore";
    public static final String ARG_ORIENTATION = "orientation";
    public static final String ARG_FILTER = "filter";
    public static final String ARG_QUERY = "query";

    private String key = "";
    private int mColumnCount = 2;
    private boolean mIsNested = false;
    private boolean mIsExplore = false;
    private int mOrientation = LinearLayoutManager.VERTICAL;
    private int mFilter = FilterCollections.FILTER_QUERY;
    private String mQuery = "";
    private Context mListener;
    private DataManager dataManager;

    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.cl_empty)
    ConstraintLayout emptyView;
    @BindView(R.id.tv_empty2)
    TextView emptyMessage;
    @BindView(R.id.iv_empty)
    ImageView emptyIcon;

    @State(CollectionsArrayList.class)
    CollectionsArrayList mCollections;

    private RecyclerView.Adapter adapter;

    public CollectionListFragment() {
    }

    @SuppressWarnings("unused")
    public static CollectionListFragment newInstance(String key, int columnCount, boolean isNested, boolean isExplore, int orientation, int filter, String query) {
        CollectionListFragment fragment = new CollectionListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putBoolean(ARG_IS_NESTED, isNested);
        args.putBoolean(ARG_IS_EXPLORE, isExplore);
        args.putInt(ARG_ORIENTATION, orientation);
        args.putInt(ARG_FILTER, filter);
        args.putString(ARG_QUERY, query);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);

        if (getArguments() != null) {
            key = getArguments().getString(ARG_KEY);
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            mIsNested = getArguments().getBoolean(ARG_IS_NESTED);
            mIsExplore = getArguments().getBoolean(ARG_IS_EXPLORE);
            mOrientation = getArguments().getInt(ARG_ORIENTATION);
            mFilter = getArguments().getInt(ARG_FILTER);
            mQuery = getArguments().getString(ARG_QUERY);
        }

        dataManager = new DataManager(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_collection_list, container, false);

        ButterKnife.bind(this, view);

        if(mIsNested) {
            recyclerView.setNestedScrollingEnabled(false);
        }

        setUpList();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setUpList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),
                mOrientation, false);

        if (mIsExplore) {
            adapter = new CollectionsOtherAdapter(this, mOrientation);
            recyclerView.setAdapter(adapter);
        } else {
            adapter = new CollectionsAdapter(this);
            recyclerView.setAdapter(adapter);
        }

        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(linearLayoutManager);
        } else {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), mColumnCount);
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        if(mCollections != null) {
            setUpCollections();
        } else {
            dataManager.requestCollectionsList(key, mFilter, mQuery, new RequestCallback<List<Pair<String, Collection>>>() {
                @Override
                public void onSuccess(List<Pair<String, Collection>> collections) {
                    mCollections = new CollectionsArrayList(collections);
                    setUpCollections();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(getActivity(), getString(R.string.collections_load_error) + error, Toast.LENGTH_SHORT).show();
                    checkEmpty();

                    if (mListener instanceof SwipeRefreshProvider) {
                        ((SwipeRefreshProvider) mListener).endRefresh();
                    }
                }
            });
        }
    }

    private void setUpCollections() {
        if (adapter instanceof CollectionsAdapter) {
            ((CollectionsAdapter) adapter).addItems(mCollections);
        } else {
            if (mIsExplore) Collections.reverse(mCollections);
            ((CollectionsOtherAdapter) adapter).addItems(mCollections);
        }
        checkEmpty();

        if (mListener instanceof SwipeRefreshProvider) {
            ((SwipeRefreshProvider) mListener).endRefresh();
        }
    }

    @Override
    public void checkEmpty() {
        if(adapter.getItemCount() == 0) {
            emptyView.setVisibility(View.VISIBLE);

            if (mListener instanceof EmptyIconClickListener) {
                final EmptyIconClickListener emptyListener = (EmptyIconClickListener) mListener;

                emptyMessage.setText(emptyListener.getEmptyMessage());
                emptyMessage.setVisibility(emptyListener.getEmptyMessage().isEmpty() ? View.GONE : View.VISIBLE);
                emptyIcon.setVisibility(emptyListener.hasIcon() ? View.VISIBLE : View.GONE);
                emptyIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        emptyListener.onEmptyIconClicked();
                    }
                });
            }
        } else {
            emptyView.setVisibility(View.GONE);
        }

        if(mOrientation == LinearLayoutManager.HORIZONTAL) {
            emptyMessage.setVisibility(View.GONE);
            emptyIcon.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCollectionClicked(RecyclerView.ViewHolder holder) {
        FragmentActivity fragmentActivity = getActivity();

        if(fragmentActivity != null) {
            View cover;
            String collectionKey;

            if(mIsExplore) {
                cover = ((CollectionsOtherAdapter.CollectionOtherViewHolder) holder).card;
                collectionKey = ((CollectionsOtherAdapter.CollectionOtherViewHolder) holder).pair.first;
            } else {
                cover = ((CollectionsAdapter.CollectionViewHolder) holder).card;
                collectionKey = ((CollectionsAdapter.CollectionViewHolder) holder).pair.first;
            }

            dataManager.addView(collectionKey);

            Pair coverPair = Pair.create(cover, "cover");

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    fragmentActivity,
                    coverPair);

            Intent startCollectionActivity = new Intent(fragmentActivity, CollectionActivity.class);
            startCollectionActivity.putExtra(CollectionActivity.COLLECTION_EXTRA, collectionKey);

            ActivityCompat.startActivity(fragmentActivity, startCollectionActivity, options.toBundle());
        }
    }

    @Override
    public void onProfileClicked(RecyclerView.ViewHolder holder) {
        FragmentActivity fragmentActivity = getActivity();

        if(fragmentActivity != null) {
            View profile = ((CollectionsOtherAdapter.CollectionOtherViewHolder) holder).profile;
            String userKey = ((CollectionsOtherAdapter.CollectionOtherViewHolder) holder).pair.second.getOwner();

            Pair profilePair = Pair.create(profile, "profile");

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    fragmentActivity,
                    profilePair);


            Intent profileActivity = new Intent(fragmentActivity, ProfileActivity.class);
            profileActivity.putExtra(ProfileActivity.USER_EXTRA, userKey);
            profileActivity.putExtra(ProfileActivity.USE_TRANSITION, false);

            ActivityCompat.startActivity(fragmentActivity, profileActivity, options.toBundle());
        }
    }

    @Override
    public void actionModeEnd() {
        if(adapter instanceof ActionModeListener) {
            ((ActionModeListener) adapter).actionModeEnd();
        }
    }

    @Override
    public void actionModeConfirm(Object item) {
        if(adapter instanceof ActionModeListener) {
            ((ActionModeListener) adapter).actionModeConfirm(item);
        }
        if (adapter instanceof CollectionsAdapter) {
            mCollections = new CollectionsArrayList(((CollectionsAdapter) adapter).getItems());
        } else {
            mCollections = new CollectionsArrayList(((CollectionsOtherAdapter) adapter).getItems());
        }
    }

    @Override
    public void actionModeUndo() {
        if(adapter instanceof ActionModeListener) {
            ((ActionModeListener) adapter).actionModeUndo();
        }
        if (adapter instanceof CollectionsAdapter) {
            mCollections = new CollectionsArrayList(((CollectionsAdapter) adapter).getItems());
        } else {
            mCollections = new CollectionsArrayList(((CollectionsOtherAdapter) adapter).getItems());
        }
    }

    @Override
    public void actionModeClear() {
        if(adapter instanceof ActionModeListener) {
            ((ActionModeListener) adapter).actionModeClear();
        }
    }

    @Override
    public void actionModeStart() {
        if(mListener instanceof ActionModeProvider) {
            ((ActionModeProvider) mListener).actionModeStart();
        }
    }

    @Override
    public void actionModeStop() {
        if(mListener instanceof ActionModeProvider) {
            ((ActionModeProvider) mListener).actionModeStop();
        }
    }

    @Override
    public void addItem(String key) {
        if(mListener instanceof ActionModeProvider) {
            ((ActionModeProvider) mListener).addItem(key);
        }
    }

    @Override
    public void removeItem(String key) {
        if(mListener instanceof ActionModeProvider) {
            ((ActionModeProvider) mListener).removeItem(key);
        }
    }

    @Override
    public boolean isActionMode() {
        return mListener instanceof ActionModeProvider && ((ActionModeProvider) mListener).isActionMode();
    }

    @Override
    public boolean isListActionMode() {
        return mListener instanceof ActionModeProvider && ((ActionModeProvider) mListener).isListActionMode();
    }

    @Override
    public boolean supportActionMode() {
        return mListener instanceof ActionModeProvider && ((ActionModeProvider) mListener).supportActionMode();
    }

    @Override
    public void onEmptyIconClicked() {
        if(mListener instanceof EmptyIconClickListener) {
            ((EmptyIconClickListener) mListener).onEmptyIconClicked();
        }
    }

    @Override
    public String getEmptyMessage() {
        if(mListener instanceof EmptyIconClickListener) {
            return ((EmptyIconClickListener) mListener).getEmptyMessage();
        }
        return "";
    }

    @Override
    public boolean hasIcon() {
        return mListener instanceof EmptyIconClickListener && ((EmptyIconClickListener) mListener).hasIcon();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}
