package br.com.felipeacerbi.collekt.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.adapters.CollectionsAdapter;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnCollectionClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.FilterCollections;
import br.com.felipeacerbi.collekt.repository.DataManager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CollectionWidgetListFragment extends Fragment implements OnCollectionClickListener, EmptyIconClickListener {

    public static final String ARG_KEY = "key";
    public static final String ARG_COLUMN_COUNT = "column_count";

    private String key = "";
    private int mColumnCount = 2;
    private OnCollectionClickListener mListener;
    private DataManager dataManager;

    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.cl_empty)
    ConstraintLayout emptyView;
    @BindView(R.id.tv_empty2)
    TextView emptyMessage;
    @BindView(R.id.iv_empty)
    ImageView emptyIcon;
    private CollectionsAdapter adapter;

    public CollectionWidgetListFragment() {
    }

    @SuppressWarnings("unused")
    public static CollectionWidgetListFragment newInstance(String key, int columnCount) {
        CollectionWidgetListFragment fragment = new CollectionWidgetListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            key = getArguments().getString(ARG_KEY);
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

        dataManager = new DataManager(getContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_collection_list, container, false);

        ButterKnife.bind(this, view);

        setUpList();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnCollectionClickListener) {
            mListener = (OnCollectionClickListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setUpList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(linearLayoutManager);
        } else {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), mColumnCount);
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        adapter = new CollectionsAdapter(this);
        recyclerView.setAdapter(adapter);

        dataManager.requestCollectionsList(key, FilterCollections.FILTER_OWNED, "", new RequestCallback<List<Pair<String, Collection>>>() {
            @Override
            public void onSuccess(List<Pair<String, Collection>> collections) {
                adapter.addItems(collections);
                checkEmpty();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getActivity(), getString(R.string.collections_load_error) + error, Toast.LENGTH_SHORT).show();
                checkEmpty();
            }
        });
    }

    @Override
    public void checkEmpty() {
        if(adapter.getItemCount() == 0) {
            emptyView.setVisibility(View.VISIBLE);

            if (mListener instanceof EmptyIconClickListener) {
                final EmptyIconClickListener emptyListener = (EmptyIconClickListener) mListener;

                emptyMessage.setText(emptyListener.getEmptyMessage());
                emptyMessage.setVisibility(emptyListener.getEmptyMessage().isEmpty() ? View.GONE : View.VISIBLE);
                emptyIcon.setVisibility(emptyListener.hasIcon() ? View.VISIBLE : View.GONE);
                emptyIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        emptyListener.onEmptyIconClicked();
                    }
                });
            }
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCollectionClicked(RecyclerView.ViewHolder holder) {
        mListener.onCollectionClicked(holder);
    }

    @Override
    public void onProfileClicked(RecyclerView.ViewHolder holder) {
        // No Need
    }

    @Override
    public void onEmptyIconClicked() {
        if(mListener instanceof EmptyIconClickListener) {
            ((EmptyIconClickListener) mListener).onEmptyIconClicked();
        }
    }

    @Override
    public String getEmptyMessage() {
        if(mListener instanceof EmptyIconClickListener) {
            return ((EmptyIconClickListener) mListener).getEmptyMessage();
        }
        return "";
    }

    @Override
    public boolean hasIcon() {
        return mListener instanceof EmptyIconClickListener && ((EmptyIconClickListener) mListener).hasIcon();
    }
}
