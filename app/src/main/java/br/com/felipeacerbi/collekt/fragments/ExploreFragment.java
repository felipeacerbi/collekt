package br.com.felipeacerbi.collekt.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.adapters.AdsPagerAdapter;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.AdItem;
import br.com.felipeacerbi.collekt.models.FilterCollections;
import br.com.felipeacerbi.collekt.repository.DataManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class ExploreFragment extends Fragment {

    public static final String ARG_KEY = "key";
    public static final String FOLLOWING_FRAGMENT_TAG = "follow_frag_tag";
    public static final String TRENDING_FRAGMENT_TAG = "trend_frag_tag";
    public static final String RECENT_FRAGMENT_TAG = "recent_frag_tag";

    @BindView(R.id.fl_following_container)
    FrameLayout followingFragment;
    @BindView(R.id.fl_trending_container)
    FrameLayout trendingFragment;
    @BindView(R.id.fl_new_container)
    FrameLayout newFragment;

    @BindView(R.id.vp_ads)
    ViewPager adsPager;
    @BindView(R.id.ci_indicator)
    CircleIndicator indicator;

    private Handler handler;
    private String key = "";
    private DataManager dataManager;

    public static ExploreFragment newInstance(String key) {
        ExploreFragment fragment = new ExploreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    public ExploreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataManager = new DataManager(getActivity());

        if (getArguments() != null) {
            key = getArguments().getString(ARG_KEY);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        ButterKnife.bind(this, view);

        transactFragment(CollectionListFragment.newInstance(key, 1, true, true, LinearLayoutManager.HORIZONTAL, FilterCollections.FILTER_FOLLOWING, ""), followingFragment.getId(), FOLLOWING_FRAGMENT_TAG);
        transactFragment(CollectionListFragment.newInstance("", 1, true, true, LinearLayoutManager.HORIZONTAL, FilterCollections.FILTER_TRENDING, ""), trendingFragment.getId(), TRENDING_FRAGMENT_TAG);
        transactFragment(CollectionListFragment.newInstance("", 1, true, true, LinearLayoutManager.HORIZONTAL, FilterCollections.FILTER_NEWEST, ""), newFragment.getId(), RECENT_FRAGMENT_TAG);

        dataManager.requestFeaturedItems(new RequestCallback<List<AdItem>>() {
            @Override
            public void onSuccess(List<AdItem> adItems) {
                if(getActivity() != null) {
                    adsPager.setAdapter(new AdsPagerAdapter(getActivity().getSupportFragmentManager(), adItems));
                    indicator.setViewPager(adsPager);
                    setUpAutoChangeView(adItems.size());
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getActivity(), getString(R.string.featured_items_load_fail) + error, Toast.LENGTH_SHORT).show();
            }
        });

        return  view;
    }

    private void setUpAutoChangeView(final int size) {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                adsPager.setCurrentItem((adsPager.getCurrentItem() == size - 1) ? 0 : adsPager.getCurrentItem() + 1);
                handler.postDelayed(this, 5000);
            }
        };

        handler = new Handler();
        handler.postDelayed(runnable, 5000);
    }

    private void transactFragment(Fragment fragment, int container, String tag) {
        if(getActivity() != null) {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction
                    .add(container, fragment, tag)
                    .commit();
        }
    }
}
