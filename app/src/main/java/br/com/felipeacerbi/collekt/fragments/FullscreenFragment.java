package br.com.felipeacerbi.collekt.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FullscreenFragment extends Fragment {

    public static final String ARG_PHOTO = "photo";

    @BindView(R.id.iv_photo)
    ImageView photoView;

    private String mPhoto;

    public static FullscreenFragment newInstance(String photo) {
        FullscreenFragment fragment = new FullscreenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PHOTO, photo);
        fragment.setArguments(args);
        return fragment;
    }

    public FullscreenFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mPhoto = getArguments().getString(ARG_PHOTO);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fullscreen, container, false);
        ButterKnife.bind(this, view);

        photoView.setOnTouchListener(new ImageMatrixTouchHandler(photoView.getContext()));

        ImageLoader.load(
                getActivity(),
                mPhoto,
                0,
                photoView);

        return view;
    }

}
