package br.com.felipeacerbi.collekt.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;

import java.util.Arrays;
import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.adapters.SpinnerItemsAdapter;
import br.com.felipeacerbi.collekt.listeners.ActionModeListener;
import br.com.felipeacerbi.collekt.listeners.ActionModeProvider;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.models.FilterOrigin;
import br.com.felipeacerbi.collekt.models.LocaleItem;
import br.com.felipeacerbi.collekt.models.Rarity;
import br.com.felipeacerbi.collekt.models.Value;
import br.com.felipeacerbi.collekt.repository.DataManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;

public class ItemDescriptionFragment extends Fragment implements ActionModeListener {

    private static final String ITEM_EXTRA = "item";

    @BindView(R.id.tv_collections_item_origin_value)
    TextView originValue;
    @BindView(R.id.tv_collection_item_value_value)
    TextView valueValue;
    @BindView(R.id.tv_collection_item_rarity_value)
    TextView rarityValue;

    @BindView(R.id.et_item_description_edit)
    EditText descEdit;
    @BindView(R.id.sp_collections_item_origin_edit)
    AppCompatSpinner spOrigin;
    @BindView(R.id.sp_collections_item_rarity_edit)
    AppCompatSpinner spRarity;
    @BindView(R.id.sp_collections_item_value_edit)
    AppCompatSpinner spValue;

    @BindView(R.id.tv_collection_item_description)
    TextView descriptionValue;

    @State(CollectionItem.class)
    CollectionItem currentCollectionItem;

    private String key;
    private ActionModeProvider mListener;
    private DataManager dataManager;

    public ItemDescriptionFragment() {
        // Required empty public constructor
    }

    public static ItemDescriptionFragment newInstance(String key) {
        ItemDescriptionFragment fragment = new ItemDescriptionFragment();
        Bundle args = new Bundle();
        args.putString(ITEM_EXTRA, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);

        dataManager = new DataManager(getActivity());

        if (getArguments() != null) {
            key = getArguments().getString(ITEM_EXTRA);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item_description, container, false);
        ButterKnife.bind(this, view);

        if(currentCollectionItem != null) {
            setUpItem();
        } else {
            dataManager.requestCollectionItem(key, new RequestCallback<CollectionItem>() {
                @Override
                public void onSuccess(CollectionItem collectionItem) {
                    currentCollectionItem = collectionItem;
                    setUpItem();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(getActivity(), getString(R.string.collection_item_load_error) + error, Toast.LENGTH_SHORT).show();
                }
            });
        }

        return view;
    }

    private void setUpItem() {
        originValue.setText(FilterOrigin.getInstance().getOrigin(getActivity(), currentCollectionItem.getOrigin()));
        valueValue.setText(Value.getValueString(currentCollectionItem.getValue()));
        rarityValue.setText(Rarity.getRarityString(getActivity(), currentCollectionItem.getRarity()));
        descriptionValue.setText(currentCollectionItem.getDescription());

        setUpSpinners();
        descEdit.setText(currentCollectionItem.getDescription());
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mListener.isActionMode()) actionModeStart();
    }

    private void setUpSpinners() {
        if(getActivity() != null) {
            List<LocaleItem> countries = FilterOrigin.getInstance().getCountriesOptions(getActivity());
            SpinnerItemsAdapter<LocaleItem> originAdapter = new SpinnerItemsAdapter<>(getActivity(), R.layout.spinner_item_edit, countries, true);
            originAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

            spOrigin.setAdapter(originAdapter);
            spOrigin.setSelection(FilterOrigin.getInstance().getSelection(getActivity(), currentCollectionItem.getOrigin()));

            SpinnerItemsAdapter<String> rarityAdapter = new SpinnerItemsAdapter<>(getActivity(), R.layout.spinner_item_edit, Arrays.asList(getResources().getStringArray(R.array.rarities_spinner)), true);
            rarityAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            spRarity.setAdapter(rarityAdapter);
            spRarity.setSelection(currentCollectionItem.getRarity());

            SpinnerItemsAdapter<String> valueAdapter = new SpinnerItemsAdapter<>(getActivity(), R.layout.spinner_item_edit, Arrays.asList(getResources().getStringArray(R.array.values_spinner)), true);
            valueAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            spValue.setAdapter(valueAdapter);
            spValue.setSelection(currentCollectionItem.getValue());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActionModeProvider) {
            mListener = (ActionModeProvider) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void actionModeStart() {
        spOrigin.setVisibility(View.VISIBLE);
        spRarity.setVisibility(View.VISIBLE);
        spValue.setVisibility(View.VISIBLE);
        descEdit.setVisibility(View.VISIBLE);

        originValue.setVisibility(View.INVISIBLE);
        valueValue.setVisibility(View.INVISIBLE);
        rarityValue.setVisibility(View.INVISIBLE);
        descriptionValue.setVisibility(View.INVISIBLE);
    }

    @Override
    public void actionModeEnd() {
        endActionMode();
    }

    @Override
    public void actionModeConfirm(Object item) {
        CollectionItem collectionItem = (CollectionItem) item;

        currentCollectionItem.setName(collectionItem.getName());
        currentCollectionItem.setCoverPhoto(collectionItem.getCoverPhoto());
        currentCollectionItem.setExtraPhotoPaths(collectionItem.getExtraPhotoPaths());
        currentCollectionItem.setOrigin(FilterOrigin.getInstance().setOrigin(getActivity(), spOrigin.getSelectedItemPosition()));
        currentCollectionItem.setRarity(spRarity.getSelectedItemPosition());
        currentCollectionItem.setValue(spValue.getSelectedItemPosition());
        currentCollectionItem.setDescription(descEdit.getText().toString());

        dataManager.updateCollectionItem(key, currentCollectionItem);
    }

    private void endActionMode() {
        originValue.setVisibility(View.VISIBLE);
        valueValue.setVisibility(View.VISIBLE);
        rarityValue.setVisibility(View.VISIBLE);
        descriptionValue.setVisibility(View.VISIBLE);

        originValue.setText(FilterOrigin.getInstance().getOrigin(getActivity(), currentCollectionItem.getOrigin()));
        valueValue.setText(Value.getValueString(currentCollectionItem.getValue()));
        rarityValue.setText(Rarity.getRarityString(getActivity(), currentCollectionItem.getRarity()));
        descriptionValue.setText(currentCollectionItem.getDescription());

        spOrigin.setVisibility(View.INVISIBLE);
        spRarity.setVisibility(View.INVISIBLE);
        spValue.setVisibility(View.INVISIBLE);
        descEdit.setVisibility(View.INVISIBLE);
    }

    @Override
    public void actionModeUndo() {

    }

    @Override
    public void actionModeClear() {

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}
