package br.com.felipeacerbi.collekt.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.CollectionCreatorHelper;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewCollectionCoverFragment extends Fragment {

    private static final int RC_PHOTO_PICKER = 1;

    @BindView(R.id.iv_cover_photo)
    ImageView photoView;
    @BindView(R.id.bt_done)
    Button doneButton;
    @BindView(R.id.rl_cover_edit)
    RelativeLayout editPhoto;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pb_progress)
    ProgressBar indefProgress;
    @BindView(R.id.cpb_progress)
    CircularProgressBar progressBar;

    private CollectionCreatorHelper helper;
    private DataManager dataManager;

    public NewCollectionCoverFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_collection_cover, null);
        ButterKnife.bind(this, view);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.close();
            }
        });

        dataManager = new DataManager(getActivity());

        editPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.createCollection();
            }
        });

        return view;
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.pick_image_title)), RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK && requestCode == RC_PHOTO_PICKER) {
            if(data != null && data.getData() != null) {
                editPhoto.setEnabled(false);
                doneButton.setEnabled(false);
                progressBar.setProgress(0);
                progressBar.setVisibility(View.VISIBLE);
                indefProgress.setVisibility(View.VISIBLE);

                ImageLoader.load(
                        getActivity(),
                        data.getData(),
                        R.drawable.collection_placeholder,
                        photoView);

                photoView.setAlpha(0.9f);

                dataManager.updateCollectionPhoto(helper.getKey(), data.getData(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                dataManager.deletePhoto(helper.getCoverPhoto());
                                helper.setCoverPhoto(uri.toString());

                                editPhoto.setEnabled(true);
                                doneButton.setEnabled(true);
                                progressBar.setVisibility(View.GONE);
                                indefProgress.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), getString(R.string.upload_complete), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }, new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        progressBar.setProgressWithAnimation(((float) taskSnapshot.getBytesTransferred() * 100 / (float) taskSnapshot.getTotalByteCount()));
                    }
                }, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), getString(R.string.upload_picture_fail), Toast.LENGTH_SHORT).show();
                        editPhoto.setEnabled(true);
                        doneButton.setEnabled(true);
                        indefProgress.setVisibility(View.GONE);
                    }
                });
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CollectionCreatorHelper) {
            helper = (CollectionCreatorHelper) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        helper = null;
    }
}
