package br.com.felipeacerbi.collekt.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.utils.CollectionCreatorHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewCollectionFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.actv_name)
    AutoCompleteTextView nameView;
    @BindView(R.id.sw_share)
    AppCompatCheckBox shareBox;
    @BindView(R.id.bt_next)
    Button nextButton;

    private CollectionCreatorHelper helper;

    public NewCollectionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_collection, null);
        ButterKnife.bind(this, view);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.close();
            }
        });

        nameView.setOnEditorActionListener(new AutoCompleteTextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent keyEvent) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    hideKeyboard(view);
                    handled = true;
                }
                return handled;
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nameView.getText().toString();

                if(name.isEmpty()) {
                    nameView.requestFocus();
                    Toast.makeText(getActivity(), R.string.empty_collection_name_warning, Toast.LENGTH_SHORT).show();
                } else {
                    Boolean shared = shareBox.isChecked();

                    if (helper != null) {
                        helper.setName(name);
                        helper.setIsShared(shared);
                        helper.next();
                    }
                }
            }
        });

        return view;
    }

    public void hideKeyboard(View view) {
        try {
            if(getActivity() != null) {
                InputMethodManager imm = (InputMethodManager) getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CollectionCreatorHelper) {
            helper = (CollectionCreatorHelper) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        helper = null;
    }
}
