package br.com.felipeacerbi.collekt.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.utils.CollectionItemCreatorHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewCollectionItemFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.actv_name)
    AutoCompleteTextView nameView;
    @BindView(R.id.actv_about)
    AutoCompleteTextView aboutView;
    @BindView(R.id.bt_next)
    Button nextButton;

    private CollectionItemCreatorHelper helper;

    public NewCollectionItemFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_collection_item, null);
        ButterKnife.bind(this, view);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.close();
            }
        });

        nameView.setOnEditorActionListener(new AutoCompleteTextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent keyEvent) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    aboutView.requestFocus();
                    handled = true;
                }
                return handled;
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nameView.getText().toString();

                if(name.isEmpty()) {
                    nameView.requestFocus();
                    Toast.makeText(getActivity(), R.string.empty_collection_item_warning, Toast.LENGTH_SHORT).show();
                } else {
                    String about = aboutView.getText().toString();

                    if (helper != null) {
                        helper.setName(name);
                        helper.setDescription(about);
                        helper.next();
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CollectionItemCreatorHelper) {
            helper = (CollectionItemCreatorHelper) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        helper = null;
    }
}
