package br.com.felipeacerbi.collekt.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.utils.CollectionItemCreatorHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewCollectionItemOwnedFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rb_owned_option)
    RadioButton ownedButton;
    @BindView(R.id.rb_not_owned_option)
    RadioButton notOwnedButton;
    @BindView(R.id.bt_done)
    Button doneButton;

    private CollectionItemCreatorHelper helper;

    public NewCollectionItemOwnedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_collection_item_own, null);
        ButterKnife.bind(this, view);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.close();
            }
        });

        ownedButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked) {
                    notOwnedButton.setChecked(false);
                    helper.setMissing(false);
                }
            }
        });

        notOwnedButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked) {
                    ownedButton.setChecked(false);
                    helper.setMissing(true);
                }
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(helper != null) {
                    helper.createCollectionItem();
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CollectionItemCreatorHelper) {
            helper = (CollectionItemCreatorHelper) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        helper = null;
    }
}
