package br.com.felipeacerbi.collekt.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.CollectionItemCreatorHelper;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewCollectionItemPhotosFragment extends Fragment {

    private static final int RC_PHOTO1_PICKER = 1;
    private static final int RC_PHOTO2_PICKER = 2;
    private static final int RC_PHOTO3_PICKER = 3;
    private static final int RC_PHOTO4_PICKER = 4;

    @BindView(R.id.iv_photo1)
    ImageView photoView1;
    @BindView(R.id.iv_photo2)
    ImageView photoView2;
    @BindView(R.id.iv_photo3)
    ImageView photoView3;
    @BindView(R.id.iv_photo4)
    ImageView photoView4;
    private ImageView[] photoViews;

    @BindView(R.id.cl_photo1)
    ConstraintLayout photoButton1;
    @BindView(R.id.cl_photo2)
    ConstraintLayout photoButton2;
    @BindView(R.id.cl_photo3)
    ConstraintLayout photoButton3;
    @BindView(R.id.cl_photo4)
    ConstraintLayout photoButton4;
    private ConstraintLayout[] photoButtons;

    @BindView(R.id.pb_progress1)
    ProgressBar indefProgress1;
    @BindView(R.id.pb_progress2)
    ProgressBar indefProgress2;
    @BindView(R.id.pb_progress3)
    ProgressBar indefProgress3;
    @BindView(R.id.pb_progress4)
    ProgressBar indefProgress4;
    private ProgressBar[] indefProgresses;

    @BindView(R.id.cpb_progress1)
    CircularProgressBar progressBar1;
    @BindView(R.id.cpb_progress2)
    CircularProgressBar progressBar2;
    @BindView(R.id.cpb_progress3)
    CircularProgressBar progressBar3;
    @BindView(R.id.cpb_progress4)
    CircularProgressBar progressBar4;
    private CircularProgressBar[] progressBars;

    @BindView(R.id.bt_next)
    Button nextButton;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private CollectionItemCreatorHelper helper;
    private DataManager dataManager;

    public NewCollectionItemPhotosFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_collection_item_photos, null);
        ButterKnife.bind(this, view);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.close();
            }
        });

        dataManager = new DataManager(getActivity());

        photoButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage(RC_PHOTO1_PICKER);
            }
        });
        photoButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage(RC_PHOTO2_PICKER);
            }
        });
        photoButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage(RC_PHOTO3_PICKER);
            }
        });
        photoButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage(RC_PHOTO4_PICKER);
            }
        });

        photoViews = new ImageView[]{photoView1, photoView2, photoView3, photoView4};
        photoButtons = new ConstraintLayout[]{photoButton1, photoButton2, photoButton3, photoButton4};
        indefProgresses = new ProgressBar[]{indefProgress1, indefProgress2, indefProgress3, indefProgress4};
        progressBars = new CircularProgressBar[]{progressBar1, progressBar2, progressBar3, progressBar4};

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(helper != null) {
                    helper.next();
                }
            }
        });

        return view;
    }

    private void pickImage(int requestCode) {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.pick_image_title)), requestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {

            if (data != null && data.getData() != null) {
                switch (requestCode) {
                    case RC_PHOTO1_PICKER:
                        applyPhoto(data, 0);
                        break;
                    case RC_PHOTO2_PICKER:
                        applyPhoto(data, 1);
                        break;
                    case RC_PHOTO3_PICKER:
                        applyPhoto(data, 2);
                        break;
                    case RC_PHOTO4_PICKER:
                        applyPhoto(data, 3);
                        break;
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void applyPhoto(Intent data, final int position) {
        photoButtons[position].setEnabled(false);
        progressBars[position].setProgress(0);
        progressBars[position].setVisibility(View.VISIBLE);
        indefProgresses[position].setVisibility(View.VISIBLE);
        nextButton.setEnabled(false);

        ImageLoader.load(
                getActivity(),
                data.getData(),
                R.drawable.collection_placeholder,
                photoViews[position]);

        photoViews[position].setAlpha(0.9f);

        helper.addPhoto(data.getData().toString(), position);

        dataManager.updateCollectionItemPhoto(helper.getKey(), data.getData(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        dataManager.deletePhoto(helper.getPhoto(position));
                        helper.addPhoto(uri.toString(), position);

                        photoButtons[position].setEnabled(true);
                        progressBars[position].setVisibility(View.GONE);
                        indefProgresses[position].setVisibility(View.GONE);
                        nextButton.setEnabled(true);
                        Toast.makeText(getActivity(), getString(R.string.upload_complete), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }, new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                progressBars[position].setProgressWithAnimation(((float) taskSnapshot.getBytesTransferred() * 100 / (float) taskSnapshot.getTotalByteCount()));
            }
        }, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), getString(R.string.upload_picture_fail), Toast.LENGTH_SHORT).show();
                photoButtons[position].setEnabled(true);
                indefProgresses[position].setVisibility(View.GONE);
                nextButton.setEnabled(true);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CollectionItemCreatorHelper) {
            helper = (CollectionItemCreatorHelper) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        helper = null;
    }
}
