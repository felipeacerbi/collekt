package br.com.felipeacerbi.collekt.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.models.FilterOrigin;
import br.com.felipeacerbi.collekt.models.LocaleItem;
import br.com.felipeacerbi.collekt.utils.CollectionItemCreatorHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewCollectionItemStoryFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.sp_origin)
    Spinner originSpinner;

    @BindView(R.id.sb_value)
    AppCompatSeekBar valueBar;
    @BindView(R.id.sb_rarity)
    AppCompatSeekBar rarityBar;

    @BindView(R.id.tv_value_1_mark)
    TextView valueMark1;
    @BindView(R.id.tv_value_2_mark)
    TextView valueMark2;
    @BindView(R.id.tv_value_3_mark)
    TextView valueMark3;
    @BindView(R.id.tv_value_4_mark)
    TextView valueMark4;
    @BindView(R.id.tv_value_5_mark)
    TextView valueMark5;

    @BindView(R.id.tv_rarity_1_mark)
    TextView rarityMark1;
    @BindView(R.id.tv_rarity_2_mark)
    TextView rarityMark2;
    @BindView(R.id.tv_rarity_3_mark)
    TextView rarityMark3;
    @BindView(R.id.tv_rarity_4_mark)
    TextView rarityMark4;
    @BindView(R.id.tv_rarity_5_mark)
    TextView rarityMark5;

    @BindView(R.id.tv_value_1_title)
    TextView valueTitle1;
    @BindView(R.id.tv_value_2_title)
    TextView valueTitle2;
    @BindView(R.id.tv_value_3_title)
    TextView valueTitle3;
    @BindView(R.id.tv_value_4_title)
    TextView valueTitle4;
    @BindView(R.id.tv_value_5_title)
    TextView valueTitle5;

    @BindView(R.id.tv_rarity_1_title)
    TextView rarityTitle1;
    @BindView(R.id.tv_rarity_2_title)
    TextView rarityTitle2;
    @BindView(R.id.tv_rarity_3_title)
    TextView rarityTitle3;
    @BindView(R.id.tv_rarity_4_title)
    TextView rarityTitle4;
    @BindView(R.id.tv_rarity_5_title)
    TextView rarityTitle5;

    @BindView(R.id.bt_next)
    Button nextButton;

    private TextView[] valueMarks;
    private TextView[] rarityMarks;
    private TextView[] valueTitles;
    private TextView[] rarityTitles;

    private CollectionItemCreatorHelper helper;

    public NewCollectionItemStoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_collection_item_story, null);
        ButterKnife.bind(this, view);

        valueMarks = new TextView[]{valueMark1, valueMark2, valueMark3, valueMark4, valueMark5};
        rarityMarks = new TextView[]{rarityMark1, rarityMark2, rarityMark3, rarityMark4, rarityMark5};
        valueTitles = new TextView[]{valueTitle1, valueTitle2, valueTitle3, valueTitle4, valueTitle5};
        rarityTitles = new TextView[]{rarityTitle1, rarityTitle2, rarityTitle3, rarityTitle4, rarityTitle5};

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.close();
            }
        });

        List<LocaleItem> countries = FilterOrigin.getInstance().getCountriesOptions(getActivity());
        ArrayAdapter<LocaleItem> originAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, countries);
        originAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        originSpinner.setAdapter(originAdapter);

        originSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                helper.setOrigin(FilterOrigin.getInstance().setOrigin(getActivity(), i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        valueBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                setProgress(i, valueMarks);
                setProgress(i, valueTitles);
                helper.setValue(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        rarityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                setProgress(i, rarityMarks);
                setProgress(i, rarityTitles);
                helper.setRarity(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(helper != null) {
                    helper.next();
                }
            }
        });

        setProgress(2, valueMarks);
        setProgress(2, valueTitles);
        setProgress(2, rarityMarks);
        setProgress(2, rarityTitles);

        return view;
    }

    public void setProgress(int progress, TextView[] array) {
        for(TextView view : array) {
            view.setTextColor(getResources().getColor(android.R.color.white));
        }

        do {
            array[progress--].setTextColor(getResources().getColor(R.color.colorAccent));
        } while(progress >= 0);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CollectionItemCreatorHelper) {
            helper = (CollectionItemCreatorHelper) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        helper = null;
    }
}
