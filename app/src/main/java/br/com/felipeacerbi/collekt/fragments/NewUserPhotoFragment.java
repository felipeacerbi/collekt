package br.com.felipeacerbi.collekt.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import br.com.felipeacerbi.collekt.utils.ProfileCreatorHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class NewUserPhotoFragment extends Fragment {

    private static final int RC_PHOTO_PICKER = 1;

    @BindView(R.id.civ_profile_picture)
    CircleImageView photoView;
    @BindView(R.id.bt_done)
    Button doneButton;
    @BindView(R.id.rl_profile_edit)
    RelativeLayout editPhoto;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cpb_progress)
    CircularProgressBar progressBar;
    @BindView(R.id.pb_progress)
    ProgressBar indefProgressBar;

    private ProfileCreatorHelper helper;
    private DataManager dataManager;

    public NewUserPhotoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_user_photo, null);
        ButterKnife.bind(this, view);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.close();
            }
        });

        dataManager = new DataManager(getActivity());

        editPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });

        ImageLoader.load(
                getActivity(),
                helper.getUserPhoto(),
                R.drawable.profile_image_placeholder,
                photoView);

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.createUser(null);
            }
        });

        return view;
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.pick_image_title)), RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK && requestCode == RC_PHOTO_PICKER) {
            if(data != null && data.getData() != null) {
                editPhoto.setEnabled(false);
                doneButton.setEnabled(false);
                progressBar.setProgressWithAnimation(0);
                indefProgressBar.setVisibility(View.VISIBLE);

                ImageLoader.load(
                        getActivity(),
                        data.getData(),
                        R.drawable.profile_image_placeholder,
                        photoView);

                photoView.setAlpha(0.9f);

                dataManager.updateUserPhoto(data.getData(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                dataManager.deletePhoto(helper.getUserPhoto());
                                helper.setUserPhoto(uri.toString());

                                editPhoto.setEnabled(true);
                                doneButton.setEnabled(true);
                                indefProgressBar.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), getString(R.string.upload_complete), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }, new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        progressBar.setProgressWithAnimation(((float) taskSnapshot.getBytesTransferred() * 100 / (float) taskSnapshot.getTotalByteCount()));
                    }
                }, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), getString(R.string.upload_picture_fail), Toast.LENGTH_SHORT).show();
                        editPhoto.setEnabled(true);
                        doneButton.setEnabled(true);
                        indefProgressBar.setVisibility(View.GONE);
                    }
                });
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ProfileCreatorHelper) {
            helper = (ProfileCreatorHelper) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        helper = null;
    }
}
