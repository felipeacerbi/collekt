package br.com.felipeacerbi.collekt.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.List;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.adapters.ReactionsListAdapter;
import br.com.felipeacerbi.collekt.listeners.EmptyIconClickListener;
import br.com.felipeacerbi.collekt.listeners.OnReactionClickListener;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.models.Reaction;
import br.com.felipeacerbi.collekt.models.ReactionsArrayList;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.widget.CollectionWidget;
import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;

public class ReactionsFragment extends Fragment implements OnReactionClickListener, EmptyIconClickListener {

    private static final String ITEM_EXTRA = "item";

    @BindView(R.id.rv_reactions_list)
    RecyclerView reactionsList;

    @BindView(R.id.iv_send_button)
    ImageView sendButton;
    @BindView(R.id.et_answer)
    EditText answerBox;

    @BindView(R.id.cl_empty)
    ConstraintLayout emptyView;
    @BindView(R.id.tv_empty2)
    TextView emptyMessage;

    @State(ReactionsArrayList.class)
    ReactionsArrayList mReactions;
    @State(CollectionItem.class)
    CollectionItem currentItem;

    private String key;
    DataManager dataManager;
    private ReactionsListAdapter adapter;

    public ReactionsFragment() {
        // Required empty public constructor
    }

    public static ReactionsFragment newInstance(String key) {
        ReactionsFragment fragment = new ReactionsFragment();
        Bundle args = new Bundle();
        args.putString(ITEM_EXTRA, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);

        if (getArguments() != null) {
            key = getArguments().getString(ITEM_EXTRA);
        }

        dataManager = new DataManager(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_reactions, container, false);
        ButterKnife.bind(this, view);

        setUpUI();

        return view;
    }

    private void setUpUI() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        adapter = new ReactionsListAdapter(ReactionsFragment.this);
        reactionsList.setLayoutManager(linearLayoutManager);
        reactionsList.setAdapter(adapter);

        if(mReactions != null) {
            setUpReactions();
        } else {
            dataManager.requestItemReactions(key, new RequestCallback<List<Pair<String, Reaction>>>() {
                @Override
                public void onSuccess(List<Pair<String, Reaction>> reactions) {
                    mReactions = new ReactionsArrayList(reactions);
                    setUpReactions();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(getActivity(), getString(R.string.reaction_load_fail) + error, Toast.LENGTH_SHORT).show();
                    checkEmpty();
                }
            });
        }

        sendButton.setEnabled(false);

        if(currentItem != null) {
            setUpItem();
        } else {
            dataManager.requestCollectionItem(key, new RequestCallback<CollectionItem>() {
                @Override
                public void onSuccess(CollectionItem collectionItem) {
                    if (collectionItem != null) {
                        currentItem = collectionItem;
                        setUpItem();
                    }
                }

                @Override
                public void onError(String error) {

                }
            });
        }

        answerBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                sendButton.setEnabled(count != 0 && currentItem != null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String newKey = dataManager.getNewReactionKey();

                Reaction reaction = new Reaction();
                reaction.setItem(key);
                reaction.setReacter(dataManager.getCurrentUid());
                reaction.setComment(answerBox.getText().toString());
                reaction.setCreated(Calendar.getInstance().getTimeInMillis());

                dataManager.createReaction(newKey, reaction);
                adapter.addItem(new Pair<>(newKey, reaction));
                checkEmpty();

                answerBox.setText("");
                CollectionWidget.updateWidgets(getActivity());
            }
        });
    }

    private void setUpReactions() {
        adapter.addItems(mReactions);
        checkEmpty();
    }

    private void setUpItem() {
        answerBox.setHint(isOwner() ? getString(R.string.answer_reactions_hint) : getString(R.string.react_item_hint));
    }

    @Override
    public void onPause() {
        super.onPause();
        dataManager.clearSubscriptions();
    }

    private boolean isOwner() {
        return currentItem.getOwner().equals(dataManager.getCurrentUid());
    }

    @Override
    public void onReactionClicked(final RecyclerView.ViewHolder holder, boolean isOwner) {
        final ReactionsListAdapter.ReactionViewHolder viewHolder = (ReactionsListAdapter.ReactionViewHolder) holder;

        PopupMenu popup = new PopupMenu(getActivity(), holder.itemView);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_edit:

                        View inputView = getLayoutInflater().inflate(R.layout.input_dialog, null);
                        final EditText editText = inputView.findViewById(R.id.input_field);
                        editText.setText(viewHolder.pair.second.getComment());

                        new AlertDialog.Builder(getActivity())
                                .setView(inputView)
                                .setTitle(R.string.edit_comment_title)
                                .setPositiveButton(R.string.save_button, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dataManager.updateReaction(viewHolder.pair.first, viewHolder.pair.second);
                                        viewHolder.pair.second.setComment(editText.getText().toString());
                                        adapter.editItem(viewHolder.pair);
                                    }
                                })
                                .setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .show();

                        break;
                    case R.id.action_remove:
                        dataManager.deleteReaction(viewHolder.pair.first, key);
                        adapter.removeItem(viewHolder.pair);
                        mReactions = new ReactionsArrayList(adapter.getItems());
                        checkEmpty();
                        break;
                }
                return false;
            }
        });

        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(isOwner ? R.menu.menu_reaction_options_owner : R.menu.menu_reaction_options, popup.getMenu());
        popup.show();
    }

    @Override
    public void onEmptyIconClicked() {
        // No need
    }

    @Override
    public String getEmptyMessage() {
        return "";
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    public void checkEmpty() {
        if(adapter.getItemCount() == 0) {
            emptyView.setVisibility(View.VISIBLE);

            emptyMessage.setText(getEmptyMessage());
            emptyMessage.setVisibility(getEmptyMessage().isEmpty() ? View.GONE : View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}
