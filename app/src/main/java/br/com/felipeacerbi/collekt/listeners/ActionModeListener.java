package br.com.felipeacerbi.collekt.listeners;

public interface ActionModeListener {
    void actionModeStart();
    void actionModeEnd();
    void actionModeConfirm(Object item);
    void actionModeUndo();
    void actionModeClear();
}
