package br.com.felipeacerbi.collekt.listeners;

public interface ActionModeProvider {
    void actionModeStart();
    void actionModeStop();
    void addItem(String item);
    void removeItem(String item);
    boolean isActionMode();
    boolean isListActionMode();
    boolean supportActionMode();
}
