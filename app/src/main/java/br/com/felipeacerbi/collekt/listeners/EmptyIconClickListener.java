package br.com.felipeacerbi.collekt.listeners;

public interface EmptyIconClickListener {
    void onEmptyIconClicked();
    String getEmptyMessage();
    boolean hasIcon();
    void checkEmpty();
}
