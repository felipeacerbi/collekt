package br.com.felipeacerbi.collekt.listeners;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

public interface OnCollectionClickListener {
    void onCollectionClicked(RecyclerView.ViewHolder holder);
    void onProfileClicked(RecyclerView.ViewHolder holder);
    Context getContext();
}
