package br.com.felipeacerbi.collekt.listeners;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

public interface OnCollectionItemClickListener {
    void onCollectionItemClicked(RecyclerView.ViewHolder holder);
    void onCollectionItemFlip(boolean missing);
    boolean isCompleteMode();
    Context getContext();
}
