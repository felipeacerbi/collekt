package br.com.felipeacerbi.collekt.listeners;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

public interface OnPhotoClickListener {
    String getKey();
    void onPhotoClicked(RecyclerView.ViewHolder holder);
    boolean isCoverPath(String path);
    void setCoverPath(String path);
    void setPhoto(RecyclerView.ViewHolder holder, String path);
    Context getContext();
}
