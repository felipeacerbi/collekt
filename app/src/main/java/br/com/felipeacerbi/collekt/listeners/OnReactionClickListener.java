package br.com.felipeacerbi.collekt.listeners;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

public interface OnReactionClickListener {
    void onReactionClicked(RecyclerView.ViewHolder holder, boolean isOwner);
    Context getContext();
}
