package br.com.felipeacerbi.collekt.listeners;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

public interface OnReportClickListener {
    void onReportClicked(RecyclerView.ViewHolder holder);
    Context getContext();
}
