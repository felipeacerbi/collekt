package br.com.felipeacerbi.collekt.listeners;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

public interface OnUserClickListener {
    void onUserClicked(RecyclerView.ViewHolder holder);
    Context getContext();
}
