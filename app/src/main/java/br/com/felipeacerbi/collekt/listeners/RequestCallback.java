package br.com.felipeacerbi.collekt.listeners;

public interface RequestCallback<T> {
    void onSuccess(T object);
    void onError(String error);
}