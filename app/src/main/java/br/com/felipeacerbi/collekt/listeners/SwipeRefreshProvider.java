package br.com.felipeacerbi.collekt.listeners;

public interface SwipeRefreshProvider {
    void startRefresh();
    void endRefresh();
}
