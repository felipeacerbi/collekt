package br.com.felipeacerbi.collekt.models;

import android.text.TextUtils;

import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;

import java.util.Locale;

@Parcel
public class AdItem {

    public static final String DATABASE_ID_CHILD = "id";
    public static final String DATABASE_TEXT_CHILD = "text";

    String id;
    String text;

    public AdItem() {
    }

    public AdItem(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public AdItem(DataSnapshot snapshot) {
        id = (String) snapshot.child(DATABASE_ID_CHILD).getValue();
        checkLocaleText(snapshot);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private void checkLocaleText(DataSnapshot snapshot) {
        String localeText =
                DATABASE_TEXT_CHILD
                        + "-"
                        + Locale.getDefault().toString()
                        .replace("_", "")
                        .replace("#", "")
                        .replace("-", "");
        text = (String) snapshot.child(localeText).getValue();

        if(TextUtils.isEmpty(text)) {
            text = (String) snapshot.child(DATABASE_TEXT_CHILD).getValue();
        }
    }
}
