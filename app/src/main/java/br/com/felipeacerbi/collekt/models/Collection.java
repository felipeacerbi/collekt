package br.com.felipeacerbi.collekt.models;

import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.HashMap;
import java.util.Map;

import icepick.Bundler;

@Parcel
public class Collection implements Bundler<Collection> {

    public static final String DATABASE_NAME_CHILD = "name";
    public static final String DATABASE_COVER_CHILD  = "coverPath";
    public static final String DATABASE_CREATED_CHILD  = "created";
    public static final String DATABASE_PUBLIC_CHILD = "public";
    public static final String DATABASE_OWNER_CHILD  = "owner";
    public static final String DATABASE_VIEWS_CHILD  = "views";
    public static final String DATABASE_REPORT_CHILD  = "report";
    public static final String DATABASE_ITEMS_CHILD  = "items";
    public static final String DATABASE_FOLLOWERS_CHILD  = "followers";

    String name;
    String coverPath;
    long created;
    boolean isPublic;
    String owner;
    long views;
    String report;
    Map<String, Boolean> items;
    Map<String, Boolean> followers;

    public Collection() {
        items = new HashMap<>();
        followers = new HashMap<>();
        views = 0;
    }

    public Collection(String name, String coverPath, long created, boolean isPublic, String owner, long views, String report, Map<String, Boolean> items, Map<String, Boolean> followers) {
        this.name = name;
        this.coverPath = coverPath;
        this.created = created;
        this.isPublic = isPublic;
        this.owner = owner;
        this.views = views;
        this.report = report;
        this.items = items;
        this.followers = followers;
    }

    public Collection(DataSnapshot snapshot) {
        name = (String) snapshot.child(DATABASE_NAME_CHILD).getValue();
        coverPath = (String) snapshot.child(DATABASE_COVER_CHILD).getValue();
        created = (long) snapshot.child(DATABASE_CREATED_CHILD).getValue();
        isPublic = (boolean) snapshot.child(DATABASE_PUBLIC_CHILD).getValue();
        owner = (String) snapshot.child(DATABASE_OWNER_CHILD).getValue();
        views = (long) snapshot.child(DATABASE_VIEWS_CHILD).getValue();

        Object isReported = snapshot.child(DATABASE_REPORT_CHILD).getValue();
        report = (isReported == null) ? null : (String) isReported;

        items = (Map<String, Boolean>) snapshot.child(DATABASE_ITEMS_CHILD).getValue();
        if(items == null) items = new HashMap<>();
        followers = (Map<String, Boolean>) snapshot.child(DATABASE_FOLLOWERS_CHILD).getValue();
        if(followers == null) followers = new HashMap<>();
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(DATABASE_NAME_CHILD, name);
        result.put(DATABASE_COVER_CHILD, coverPath);
        result.put(DATABASE_CREATED_CHILD, created);
        result.put(DATABASE_PUBLIC_CHILD, isPublic);
        result.put(DATABASE_OWNER_CHILD, owner);
        result.put(DATABASE_VIEWS_CHILD, views);
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoverPath() {
        return coverPath;
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public Map<String, Boolean> getItems() {
        return items;
    }

    public void setItems(Map<String, Boolean> items) {
        this.items = items;
    }

    public Map<String, Boolean> getFollowers() {
        return followers;
    }

    public void setFollowers(Map<String, Boolean> followers) {
        this.followers = followers;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        this.isPublic = aPublic;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public void put(String s, Collection collection, Bundle bundle) {
        bundle.putParcelable(s, Parcels.wrap(collection));
    }

    @Override
    public Collection get(String s, Bundle bundle) {
        return Parcels.unwrap(bundle.getParcelable(s));
    }
}
