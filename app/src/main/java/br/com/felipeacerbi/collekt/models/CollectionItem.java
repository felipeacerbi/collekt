package br.com.felipeacerbi.collekt.models;

import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import icepick.Bundler;

@Parcel
public class CollectionItem implements Bundler<CollectionItem> {

    public static final String DATABASE_NAME_CHILD = "name";
    public static final String DATABASE_DESC_CHILD = "description";
    public static final String DATABASE_COVER_CHILD  = "coverPhoto";
    public static final String DATABASE_ORIGIN_CHILD  = "origin";
    public static final String DATABASE_RARITY_CHILD  = "rarity";
    public static final String DATABASE_VALUE_CHILD  = "value";
    public static final String DATABASE_PARENT_CHILD  = "parent";
    public static final String DATABASE_OWNER_CHILD  = "owner";
    public static final String DATABASE_MISSING_CHILD  = "missing";
    public static final String DATABASE_CREATED_CHILD  = "created";
    public static final String DATABASE_EXTRA_CHILD  = "extraPhotoPaths";
    public static final String DATABASE_REACTIONS_CHILD  = "reactions";

    String name;
    String description;
    String coverPhoto;
    String origin;
    int rarity;
    int value;
    String parent;
    String owner;
    long created;
    boolean missing;
    List<String> extraPhotoPaths;
    Map<String, Boolean> reactions;

    public CollectionItem() {
        extraPhotoPaths = new ArrayList<>();
        reactions = new HashMap<>();
        rarity = 2;
        value = 2;
    }

    public CollectionItem(String name, String description, String coverPhoto, String origin, int rarity, int value, String parent, String owner, long created, boolean missing, List<String> extraPhotoPaths, Map<String, Boolean> reactions) {
        this.name = name;
        this.description = description;
        this.coverPhoto = coverPhoto;
        this.origin = origin;
        this.rarity = rarity;
        this.value = value;
        this.parent = parent;
        this.owner = owner;
        this.created = created;
        this.missing = missing;
        this.extraPhotoPaths = extraPhotoPaths;
        this.reactions = reactions;
    }

    public CollectionItem(DataSnapshot snapshot) {
        name = (String) snapshot.child(DATABASE_NAME_CHILD).getValue();
        description = (String) snapshot.child(DATABASE_DESC_CHILD).getValue();
        coverPhoto = (String) snapshot.child(DATABASE_COVER_CHILD).getValue();

        Object originValue = snapshot.child(DATABASE_ORIGIN_CHILD).getValue();
        if(originValue instanceof Long) { // Retro compatibility
            origin = String.valueOf(((Long) originValue).intValue());
        } else if(originValue instanceof String) {
            origin = (String) originValue;
        }

        rarity = ((Long) snapshot.child(DATABASE_RARITY_CHILD).getValue()).intValue();
        value = ((Long) snapshot.child(DATABASE_VALUE_CHILD).getValue()).intValue();
        parent = (String) snapshot.child(DATABASE_PARENT_CHILD).getValue();
        owner = (String) snapshot.child(DATABASE_OWNER_CHILD).getValue();
        created = (long) snapshot.child(DATABASE_CREATED_CHILD).getValue();

        Object isMissing = snapshot.child(DATABASE_MISSING_CHILD).getValue();
        missing = isMissing != null && (boolean) isMissing;

        extraPhotoPaths = (List<String>) snapshot.child(DATABASE_EXTRA_CHILD).getValue();
        if(extraPhotoPaths == null) extraPhotoPaths = new ArrayList<>();
        reactions = (Map<String, Boolean>) snapshot.child(DATABASE_REACTIONS_CHILD).getValue();
        if(reactions == null) reactions = new HashMap<>();
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(DATABASE_NAME_CHILD, name);
        result.put(DATABASE_DESC_CHILD, description);
        result.put(DATABASE_COVER_CHILD, coverPhoto);
        result.put(DATABASE_ORIGIN_CHILD, origin);
        result.put(DATABASE_RARITY_CHILD, rarity);
        result.put(DATABASE_VALUE_CHILD, value);
        result.put(DATABASE_PARENT_CHILD, parent);
        result.put(DATABASE_OWNER_CHILD, owner);
        result.put(DATABASE_CREATED_CHILD, created);
        result.put(DATABASE_MISSING_CHILD, missing);
        result.put(DATABASE_EXTRA_CHILD, removeEmpties(extraPhotoPaths));
        result.put(DATABASE_REACTIONS_CHILD, reactions);
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getRarity() {
        return rarity;
    }

    public void setRarity(int rarity) {
        this.rarity = rarity;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public List<String> getExtraPhotoPaths() {
        return extraPhotoPaths;
    }

    public void setExtraPhotoPaths(List<String> extraPhotoPaths) {
        this.extraPhotoPaths = extraPhotoPaths;
    }

    public boolean isMissing() {
        return missing;
    }

    public void setMissing(boolean missing) {
        this.missing = missing;
    }

    public Map<String, Boolean> getReactions() {
        return reactions;
    }

    public void setReactions(Map<String, Boolean> reactions) {
        this.reactions = reactions;
    }

    public static List<String> removeEmpties(List<String> list) {
        List<String> validList = new ArrayList<>();

        for(int i = 0; i < list.size(); i++) {
            if(!list.get(i).isEmpty()) validList.add(list.get(i));
        }

        return validList;
    }

    @Override
    public void put(String s, CollectionItem collectionItem, Bundle bundle) {
        bundle.putParcelable(s, Parcels.wrap(collectionItem));
    }

    @Override
    public CollectionItem get(String s, Bundle bundle) {
        return Parcels.unwrap(bundle.getParcelable(s));
    }
}
