package br.com.felipeacerbi.collekt.models;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.ArrayList;

import icepick.Bundler;

@Parcel
public class CollectionItemsArrayList extends ArrayList<Pair<String, CollectionItem>> implements Bundler<ArrayList<Pair<String, CollectionItem>>> {

    public CollectionItemsArrayList() {
    }

    public CollectionItemsArrayList(@NonNull java.util.Collection<? extends Pair<String, CollectionItem>> c) {
        super(c);
    }

    @Override
    public void put(String key, ArrayList<Pair<String, CollectionItem>> items, Bundle bundle) {
        bundle.putParcelable(key, Parcels.wrap(items));
    }

    @Override
    public ArrayList<Pair<String, CollectionItem>> get(String key, Bundle bundle) {
        return Parcels.unwrap(bundle.getParcelable(key));
    }
}
