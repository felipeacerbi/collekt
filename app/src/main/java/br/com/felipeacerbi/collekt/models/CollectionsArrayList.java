package br.com.felipeacerbi.collekt.models;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.ArrayList;

import icepick.Bundler;

@Parcel
public class CollectionsArrayList extends ArrayList<Pair<String, Collection>> implements Bundler<ArrayList<Pair<String, Collection>>> {

    public CollectionsArrayList() {
    }

    public CollectionsArrayList(@NonNull java.util.Collection<? extends Pair<String, Collection>> c) {
        super(c);
    }

    @Override
    public void put(String key, ArrayList<Pair<String, Collection>> collections, Bundle bundle) {
        bundle.putParcelable(key, Parcels.wrap(collections));
    }

    @Override
    public ArrayList<Pair<String, Collection>> get(String key, Bundle bundle) {
        return Parcels.unwrap(bundle.getParcelable(key));
    }
}
