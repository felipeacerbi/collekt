package br.com.felipeacerbi.collekt.models;

import android.content.Context;

import androidx.core.util.Pair;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class FilterCollectionItems {

    public static final int FILTER_ORIGIN = 0;
    public static final int FILTER_SORT = 1;
    public static final int FILTER_RARITY = 2;
    public static final int FILTER_VALUE = 3;

    public static boolean filterCollectionItem(Context context, int[] filter, CollectionItem collectionItem) {
        int filterOrigin = filter[FilterCollectionItems.FILTER_ORIGIN];
        int filterRarity = filter[FilterCollectionItems.FILTER_RARITY];
        int filterValue = filter[FilterCollectionItems.FILTER_VALUE];

        String origin = FilterOrigin.getInstance().getSelectedFilter(context, filterOrigin);

        return (filterOrigin == 0 || origin.equals(collectionItem.getOrigin())) &&
                (filterRarity == 0 || filterRarity == collectionItem.getRarity() + 1) &&
                (filterValue == 0 || filterValue == collectionItem.getValue() + 1);
    }

    public static void sortCollectionItems(int[] filter, List<Pair<String, CollectionItem>> collectionItems) {
        int filterSort = filter[FilterCollectionItems.FILTER_SORT];

        switch (filterSort) {
            case FilterSort.SORT_A_TO_Z:
                Collections.sort(collectionItems, new Comparator<Pair<String, CollectionItem>>() {
                    @Override
                    public int compare(Pair<String, CollectionItem> pair1, Pair<String, CollectionItem> pair2) {
                        return pair1.second.getName().compareTo(pair2.second.getName());
                    }
                });
                break;
            case FilterSort.SORT_Z_TO_A:
                Collections.sort(collectionItems, new Comparator<Pair<String, CollectionItem>>() {
                    @Override
                    public int compare(Pair<String, CollectionItem> pair1, Pair<String, CollectionItem> pair2) {
                        return pair2.second.getName().compareTo(pair1.second.getName());
                    }
                });
                break;
            case FilterSort.SORT_MOST_RARE:
                Collections.sort(collectionItems, new Comparator<Pair<String, CollectionItem>>() {
                    @Override
                    public int compare(Pair<String, CollectionItem> pair1, Pair<String, CollectionItem> pair2) {
                        if(pair2.second.getRarity() >= pair1.second.getRarity()) return 1;
                        if(pair2.second.getRarity() == pair1.second.getRarity()) return 0;
                        if(pair2.second.getRarity() < pair1.second.getRarity()) return -1;
                        return 0;
                    }
                });
                break;
            case FilterSort.SORT_LEAST_RARE:
                Collections.sort(collectionItems, new Comparator<Pair<String, CollectionItem>>() {
                    @Override
                    public int compare(Pair<String, CollectionItem> pair1, Pair<String, CollectionItem> pair2) {
                        if(pair1.second.getRarity() >= pair2.second.getRarity()) return 1;
                        if(pair1.second.getRarity() == pair2.second.getRarity()) return 0;
                        if(pair1.second.getRarity() < pair2.second.getRarity()) return -1;
                        return 0;
                    }
                });
                break;
            case FilterSort.SORT_MOST_VALUE:
                Collections.sort(collectionItems, new Comparator<Pair<String, CollectionItem>>() {
                    @Override
                    public int compare(Pair<String, CollectionItem> pair1, Pair<String, CollectionItem> pair2) {
                        if(pair2.second.getValue() >= pair1.second.getValue()) return 1;
                        if(pair2.second.getValue() == pair1.second.getValue()) return 0;
                        if(pair2.second.getValue() < pair1.second.getValue()) return -1;
                        return 0;
                    }
                });
                break;
            case FilterSort.SORT_LEAST_VALUE:
                Collections.sort(collectionItems, new Comparator<Pair<String, CollectionItem>>() {
                    @Override
                    public int compare(Pair<String, CollectionItem> pair1, Pair<String, CollectionItem> pair2) {
                        if(pair1.second.getValue() >= pair2.second.getValue()) return 1;
                        if(pair1.second.getValue() == pair2.second.getValue()) return 0;
                        if(pair1.second.getValue() < pair2.second.getValue()) return -1;
                        return 0;
                    }
                });
                break;
        }
    }
}
