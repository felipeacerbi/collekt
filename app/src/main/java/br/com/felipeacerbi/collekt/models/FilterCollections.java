package br.com.felipeacerbi.collekt.models;

public class FilterCollections {

    public static final int FILTER_QUERY = 0;
    public static final int FILTER_OWNED = 1;
    public static final int FILTER_FOLLOWING = 2;
    public static final int FILTER_TRENDING = 3;
    public static final int FILTER_NEWEST = 4;
}
