package br.com.felipeacerbi.collekt.models;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import br.com.felipeacerbi.collekt.R;

import static br.com.felipeacerbi.collekt.models.LocaleItem.UNKNOWN_ORIGIN;

public class FilterOrigin extends FilterCollectionItems {

    private static FilterOrigin filterOrigin;

    private List<LocaleItem> localeItems;
    private List<LocaleItem> countries;
    private List<LocaleItem> countriesFilter;

    public static FilterOrigin getInstance() {
        if(filterOrigin == null) {
            filterOrigin = new FilterOrigin();
        }

        return filterOrigin;
    }

    private FilterOrigin() {
    }

    private List<LocaleItem> getAllCountries(Context context) {
        List<String> countries = new ArrayList<>();
        Locale[] locales = Locale.getAvailableLocales();

        if(localeItems == null) {
            localeItems = new ArrayList<>();

            for (Locale locale : locales) {
                String country = locale.getDisplayCountry();
                if (country.trim().length() > 0 && !countries.contains(country)) {

                    countries.add(country);
                    LocaleItem localeItem = new LocaleItem(context, locale);
                    localeItems.add(localeItem);
                }
            }

            Collections.sort(localeItems);
        }

        return localeItems;
    }

    public List<LocaleItem> getCountriesOptions(Context context) {
        if(countries == null) {
            countries = new ArrayList<>();
            countries.addAll(getAllCountries(context));
            countries.add(0, new LocaleItem(context, UNKNOWN_ORIGIN));
        }

        return countries;
    }

    public List<LocaleItem> getCountriesFilter(Context context) {
        if(countriesFilter == null) {
            countriesFilter = new ArrayList<>();
            countriesFilter.addAll(getCountriesOptions(context));
            countriesFilter.add(0, new LocaleItem(context, LocaleItem.ALL_ORIGIN));
        }

        return countriesFilter;
    }

    public List<String> getCountriesFilterAsStrings(Context context) {
        List<LocaleItem> countries = getCountriesFilter(context);

        List<String> countryStrings = new ArrayList<>(countries.size());
        for(LocaleItem country : countries) {
            countryStrings.add(country.toString());
        }

        return countryStrings;
    }

    public String getSelectedFilter(Context context, int position) {
        return getCountriesFilter(context).get(position).getCode();
    }

    private String codeToDisplay(Context context, String code) {
        if(code.equals(UNKNOWN_ORIGIN)) return context == null ? "" : context.getString(R.string.unknown_origin);
        return Locale.forLanguageTag(code).getDisplayCountry();
    }

    public String getOrigin(Context context, String origin) {
        try {
            int intOrigin = Integer.parseInt(origin);
            return getOrigin(context, intOrigin);
        } catch(Exception e) {
            return codeToDisplay(context, origin);
        }
    }

    // Retro compatibility
    private String getOrigin(Context context, int origin) {
        return getAllCountries(context).get(origin).toString();
    }

    public String setOrigin(Context context, int position) {
        if(position == 0) return UNKNOWN_ORIGIN;
        return getCountriesOptions(context).get(position).getCode();
    }

    public int getSelection(Context context, String code) {
        try {
            return Integer.parseInt(code);
        } catch(Exception e) {
            if (code.equals(UNKNOWN_ORIGIN)) return 0;

            List<LocaleItem> selectionList = getCountriesOptions(context);
            for(LocaleItem item : selectionList) {
                if(item.getCode().equals(code)) return selectionList.indexOf(item);
            }

            return 0;
        }
    }
}
