package br.com.felipeacerbi.collekt.models;

public class FilterSort extends FilterCollectionItems {

    public static final int SORT_A_TO_Z = 0;
    public static final int SORT_Z_TO_A = 1;
    public static final int SORT_MOST_RARE = 2;
    public static final int SORT_LEAST_RARE = 3;
    public static final int SORT_MOST_VALUE = 4;
    public static final int SORT_LEAST_VALUE = 5;

}
