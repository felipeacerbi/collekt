package br.com.felipeacerbi.collekt.models;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.util.Locale;

import br.com.felipeacerbi.collekt.R;

public class LocaleItem implements Comparable{

    public static final String ALL_ORIGIN = "all";
    public static final String UNKNOWN_ORIGIN = "unknown";

    private Context mContext;
    private Locale locale;
    private String alternative;

    public LocaleItem(Context context, Locale locale) {
        mContext = context;
        this.locale = locale;
    }

    public LocaleItem(Context context, String alternative) {
        mContext = context;
        this.alternative = alternative;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    private String getCountry() {
        if(alternative == null) return locale.getDisplayCountry();

        switch (alternative) {
            case ALL_ORIGIN:
                return mContext.getString(R.string.all_countries);
            case UNKNOWN_ORIGIN:
                return mContext.getString(R.string.unknown_origin);
            default:
                return locale.getDisplayCountry();
        }
    }

    public String getCode() {
        if(!TextUtils.isEmpty(alternative)) return alternative;
        return locale.toLanguageTag();
    }

    @Override
    public String toString() {
        return getCountry();
    }

    @Override
    public int compareTo(@NonNull Object o) {
        LocaleItem other = (LocaleItem) o;
        return toString().compareTo(other.toString());
    }
}
