package br.com.felipeacerbi.collekt.models;

import android.content.Context;

import br.com.felipeacerbi.collekt.R;

public class Rarity {

    public static String getRarityString(Context context, int value) {
        String[] values = context.getResources().getStringArray(R.array.rarities_spinner);

        return values[value];
    }
}
