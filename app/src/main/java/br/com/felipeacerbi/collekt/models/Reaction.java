package br.com.felipeacerbi.collekt.models;

import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.HashMap;
import java.util.Map;

import icepick.Bundler;

@Parcel
public class Reaction implements Bundler<Reaction> {

    public static final String DATABASE_ITEM_CHILD = "item";
    public static final String DATABASE_REACTER_CHILD = "reacter";
    public static final String DATABASE_COMMENT_CHILD = "comment";
    public static final String DATABASE_CREATED_CHILD = "created";
    public static final String DATABASE_LIKES_CHILD = "likes";

    String item;
    String reacter;
    String comment;
    long created;
    Map<String, Boolean> likes;

    public Reaction() {
        likes = new HashMap<>();
    }

    public Reaction(String item, String reacter, String comment, long created, Map<String, Boolean> likes) {
        this.item = item;
        this.reacter = reacter;
        this.comment = comment;
        this.created = created;
        this.likes = likes;
    }

    public Reaction(DataSnapshot snapshot) {
        item = (String) snapshot.child(DATABASE_ITEM_CHILD).getValue();
        reacter = (String) snapshot.child(DATABASE_REACTER_CHILD).getValue();
        comment = (String) snapshot.child(DATABASE_COMMENT_CHILD).getValue();
        created = (long) snapshot.child(DATABASE_CREATED_CHILD).getValue();

        likes = (Map<String, Boolean>) snapshot.child(DATABASE_LIKES_CHILD).getValue();
        if(likes == null) likes = new HashMap<>();
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(DATABASE_ITEM_CHILD, item);
        result.put(DATABASE_REACTER_CHILD, reacter);
        result.put(DATABASE_COMMENT_CHILD, comment);
        result.put(DATABASE_CREATED_CHILD, created);
        result.put(DATABASE_LIKES_CHILD, likes);
        return result;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getReacter() {
        return reacter;
    }

    public void setReacter(String reacter) {
        this.reacter = reacter;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public Map<String, Boolean> getLikes() {
        return likes;
    }

    public void setLikes(Map<String, Boolean> likes) {
        this.likes = likes;
    }

    @Override
    public void put(String s, Reaction reaction, Bundle bundle) {
        bundle.putParcelable(s, Parcels.wrap(reaction));
    }

    @Override
    public Reaction get(String s, Bundle bundle) {
        return Parcels.unwrap(bundle.getParcelable(s));
    }
}
