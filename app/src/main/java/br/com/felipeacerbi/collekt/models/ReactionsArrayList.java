package br.com.felipeacerbi.collekt.models;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.ArrayList;

import icepick.Bundler;

@Parcel
public class ReactionsArrayList extends ArrayList<Pair<String, Reaction>> implements Bundler<ArrayList<Pair<String, Reaction>>> {

    public ReactionsArrayList() {
    }

    public ReactionsArrayList(@NonNull java.util.Collection<? extends Pair<String, Reaction>> c) {
        super(c);
    }

    @Override
    public void put(String key, ArrayList<Pair<String, Reaction>> reactions, Bundle bundle) {
        bundle.putParcelable(key, Parcels.wrap(reactions));
    }

    @Override
    public ArrayList<Pair<String, Reaction>> get(String key, Bundle bundle) {
        return Parcels.unwrap(bundle.getParcelable(key));
    }
}
