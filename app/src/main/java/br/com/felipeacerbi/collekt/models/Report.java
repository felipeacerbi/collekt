package br.com.felipeacerbi.collekt.models;

import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.HashMap;
import java.util.Map;

import icepick.Bundler;

@Parcel
public class Report implements Bundler<Report> {

    public static final int REPORT_TYPE_SPAM = 0;
    public static final int REPORT_TYPE_INAPPROPRIATE = 1;

    public static final int REPORT_STATUS_PENDING = 0;
    public static final int REPORT_STATUS_APPLIED = 1;
    public static final int REPORT_STATUS_IGNORED = 2;

    public static final String DATABASE_REPORTED_CHILD = "reported";
    public static final String DATABASE_REPORTED_EXTRA_CHILD = "reportedExtra";
    public static final String DATABASE_REPORTER_CHILD = "reporter";
    public static final String DATABASE_REPORTER_MESSAGE_CHILD = "reporterMessage";
    public static final String DATABASE_STATUS_CHILD = "status";
    public static final String DATABASE_TYPE_CHILD = "type";
    public static final String DATABASE_CREATED_CHILD = "created";

    String reported;
    String reportedExtra;
    String reporter;
    String reporterMessage;
    int status;
    int type;
    long created;

    public Report() {
        type = REPORT_TYPE_SPAM;
        status = REPORT_STATUS_PENDING;
    }

    public Report(String reported, String reportedExtra, String reporter, String reporterMessage, int type) {
        this.reported = reported;
        this.reportedExtra = reportedExtra;
        this.reporter = reporter;
        this.reporterMessage = reporterMessage;
        this.type = type;
    }

    public Report(DataSnapshot snapshot) {
        reported = (String) snapshot.child(DATABASE_REPORTED_CHILD).getValue();
        reportedExtra = (String) snapshot.child(DATABASE_REPORTED_EXTRA_CHILD).getValue();
        reporter = (String) snapshot.child(DATABASE_REPORTER_CHILD).getValue();
        reporterMessage = (String) snapshot.child(DATABASE_REPORTER_MESSAGE_CHILD).getValue();
        status = ((Long) snapshot.child(DATABASE_STATUS_CHILD).getValue()).intValue();
        type = ((Long) snapshot.child(DATABASE_TYPE_CHILD).getValue()).intValue();
        created = (long) snapshot.child(DATABASE_CREATED_CHILD).getValue();
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(DATABASE_REPORTED_CHILD, reported);
        result.put(DATABASE_REPORTED_EXTRA_CHILD, reportedExtra);
        result.put(DATABASE_REPORTER_CHILD, reporter);
        result.put(DATABASE_REPORTER_MESSAGE_CHILD, reporterMessage);
        result.put(DATABASE_TYPE_CHILD, type);
        result.put(DATABASE_CREATED_CHILD, created);
        result.put(DATABASE_STATUS_CHILD, status);
        return result;
    }

    public String getReported() {
        return reported;
    }

    public void setReported(String reported) {
        this.reported = reported;
    }

    public String getReportedExtra() {
        return reportedExtra;
    }

    public void setReportedExtra(String reportedExtra) {
        this.reportedExtra = reportedExtra;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public String getReporterMessage() {
        return reporterMessage;
    }

    public void setReporterMessage(String reporterMessage) {
        this.reporterMessage = reporterMessage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    @Override
    public void put(String s, Report report, Bundle bundle) {
        bundle.putParcelable(s, Parcels.wrap(report));
    }

    @Override
    public Report get(String s, Bundle bundle) {
        return Parcels.unwrap(bundle.getParcelable(s));
    }
}
