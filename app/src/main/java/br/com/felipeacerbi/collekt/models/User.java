package br.com.felipeacerbi.collekt.models;

import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.HashMap;
import java.util.Map;

import icepick.Bundler;

@Parcel
public class User implements Bundler<User> {

    public static final String DATABASE_IDTOKEN_CHILD = "idToken";
    public static final String DATABASE_NAME_CHILD = "name";
    public static final String DATABASE_DESC_CHILD = "description";
    public static final String DATABASE_EMAIL_CHILD = "email";
    public static final String DATABASE_PHOTO_CHILD  = "photoPath";
    public static final String DATABASE_CREATED_CHILD  = "created";
    public static final String DATABASE_FOLLOWERS_CHILD  = "followers";
    public static final String DATABASE_OWNED_CHILD  = "owned";
    public static final String DATABASE_FOLLOWING_CHILD = "following";
    public static final String DATABASE_LIKES_CHILD = "likes";

    String idToken;
    String name;
    String description;
    String email;
    String photoPath;
    long created;
    long followers;
    Map<String, Boolean> owned;
    Map<String, Boolean> following;
    Map<String, Boolean> likes;

    public User() {
    }

    public User(String name, String description, String email, String photoPath) {
        this.name = name;
        this.description = description;
        this.email = email;
        this.photoPath = photoPath;
        owned = new HashMap<>();
        following = new HashMap<>();
        likes = new HashMap<>();
        followers = 0;
    }

    public User(DataSnapshot snapshot) {
        idToken = (String) snapshot.child(DATABASE_IDTOKEN_CHILD).getValue();
        name = (String) snapshot.child(DATABASE_NAME_CHILD).getValue();
        description = (String) snapshot.child(DATABASE_DESC_CHILD).getValue();
        email = (String) snapshot.child(DATABASE_EMAIL_CHILD).getValue();
        photoPath = (String) snapshot.child(DATABASE_PHOTO_CHILD).getValue();

        Object createdObject = snapshot.child(DATABASE_CREATED_CHILD).getValue();
        created = (createdObject == null) ? null : (long) createdObject;
        Object followersObject = snapshot.child(DATABASE_FOLLOWERS_CHILD).getValue();
        followers = (followersObject == null) ? null : (long) followersObject;

        owned = (Map<String, Boolean>) snapshot.child(DATABASE_OWNED_CHILD).getValue();
        if(owned == null) owned = new HashMap<>();
        following = (Map<String, Boolean>) snapshot.child(DATABASE_FOLLOWING_CHILD).getValue();
        if(following == null) following = new HashMap<>();
        likes = (Map<String, Boolean>) snapshot.child(DATABASE_LIKES_CHILD).getValue();
        if(likes == null) likes = new HashMap<>();
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(DATABASE_IDTOKEN_CHILD, idToken);
        result.put(DATABASE_NAME_CHILD, name);
        result.put(DATABASE_DESC_CHILD, description);
        result.put(DATABASE_EMAIL_CHILD, email);
        result.put(DATABASE_PHOTO_CHILD, photoPath);
        result.put(DATABASE_CREATED_CHILD, created);
        result.put(DATABASE_FOLLOWERS_CHILD, followers);
        result.put(DATABASE_LIKES_CHILD, likes);
        return result;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getFollowers() {
        return followers;
    }

    public void setFollowers(long followers) {
        this.followers = followers;
    }

    public Map<String, Boolean> getOwned() {
        return owned;
    }

    public void setOwned(Map<String, Boolean> owned) {
        this.owned = owned;
    }

    public Map<String, Boolean> getFollowing() {
        return following;
    }

    public void setFollowing(Map<String, Boolean> following) {
        this.following = following;
    }

    public long getCreated() {
        return created;
    }

    public Map<String, Boolean> getLikes() {
        return likes;
    }

    public void setLikes(Map<String, Boolean> likes) {
        this.likes = likes;
    }

    @Override
    public void put(String s, User user, Bundle bundle) {
        bundle.putParcelable(s, Parcels.wrap(user));
    }

    @Override
    public User get(String s, Bundle bundle) {
        return Parcels.unwrap(bundle.getParcelable(s));
    }
}
