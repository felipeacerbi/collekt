package br.com.felipeacerbi.collekt.models;

import br.com.felipeacerbi.collekt.R;

public class Value {

    public static String getValueString(int value) {
        StringBuilder result = new StringBuilder("$");

        for(; value > 0; value --) {
            result.append("$");
        }

        return result.toString();
    }
}
