package br.com.felipeacerbi.collekt.repository;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;

import androidx.annotation.Nullable;
import androidx.core.util.Pair;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;

import java.util.List;

import br.com.felipeacerbi.collekt.firebase.FirebaseManager;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.AdItem;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.models.Reaction;
import br.com.felipeacerbi.collekt.models.Report;
import br.com.felipeacerbi.collekt.models.User;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DataManager {

    private final Context mContext;
    private FirebaseManager firebaseManager;
    private CompositeDisposable subscriptions;

    public DataManager(Context context) {
        mContext = context;
        firebaseManager = FirebaseManager.getInstance();
        subscriptions = new CompositeDisposable();
    }

    public FirebaseAuth getAuth() {
        return firebaseManager.getAuth();
    }

    public void requestProfile(String key, RequestCallback<User> request) {
        if(key == null) {
            key = getCurrentUid();
        }

        getUser(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestCollection(String key, RequestCallback<Collection> request) {
        getCollection(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestCollectionsList(String key, int filter, String query, RequestCallback<List<Pair<String, Collection>>> request) {
        getCollectionsList(key, filter, query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestCollectionItem(String key, RequestCallback<CollectionItem> request) {
        getCollectionItem(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestCollectionItemsList(String key, RequestCallback<List<Pair<String, CollectionItem>>> request) {
        getCollectionItemsList(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestCollectionItemPhotos(String key, RequestCallback<List<String>> request) {
        getCollectionItemPhotos(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestItemReactions(String key, RequestCallback<List<Pair<String, Reaction>>> request) {
        getReactionsList(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestFollowersList(String key, RequestCallback<List<Pair<String, User>>> request) {
        getFollowersList(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestFeaturedItems(RequestCallback<List<AdItem>> request) {
        getFeaturedList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestReportsList(RequestCallback<List<Pair<String, Report>>> request) {
        getReportsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void requestReport(String key, RequestCallback<Report> request) {
        getReport(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createObserver(request));
    }

    public void clearSubscriptions() {
        firebaseManager.stopReactions();
        subscriptions.dispose();
    }

    public String getCurrentUid() {
        return firebaseManager.getCurrentUid();
    }
    public void createUser(User user) {
        firebaseManager.createUser(user);
    }
    public void updateUser(User user) {firebaseManager.updateUserInfo(user);}

    public boolean isOwner(String uid) {
        return uid != null && !uid.isEmpty() && uid.equals(getCurrentUid());
    }

    public boolean isGoogleSignedIn() {
        for(UserInfo profile : firebaseManager.getUserProviders()) {
            if(profile.getProviderId().equals(GoogleAuthProvider.PROVIDER_ID)) return true;
        }
        return false;
    }

    public boolean isFacebookSignedIn() {
        for(UserInfo profile : firebaseManager.getUserProviders()) {
            if(profile.getProviderId().equals(FacebookAuthProvider.PROVIDER_ID)) return true;
        }
        return false;
    }

    public boolean isUserAnonymous() {
        return firebaseManager.isUserAnonymous();
    }

    public void checkLogin(boolean validateUser, final RequestCallback<Boolean> callback) {
        firebaseManager.checkLogin(mContext, validateUser, callback);
    }

    public void linkNewUserCredential(Activity activity, AuthCredential credential, RequestCallback<AuthResult> resultRequestCallback) {
        firebaseManager.linkNewUserCredential(activity, credential, resultRequestCallback);
    }

    public void updateUserPhoto(Uri photo,
                                @Nullable OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                                @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                                @Nullable OnFailureListener failureListener) {
        firebaseManager.updateUserPhoto(mContext, photo, successListener, progressListener, failureListener);
    }

    private Observable<User> getUser(final String uid) {
        return Observable.create(new ObservableOnSubscribe<User>() {
            @Override
            public void subscribe(final ObservableEmitter<User> subscriber) throws Exception {
                firebaseManager.requestUser(mContext, uid, new RequestCallback<User>() {
                    @Override
                    public void onSuccess(User user) {
                        subscriber.onNext(user);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    public String getNewCollectionKey() {
        return firebaseManager.getNewCollectionKey();
    }

    public void createCollection(String key, Collection collection) {
        firebaseManager.createCollection(key, collection);
    }

    public void updateCollection(String key, Collection collection) {
        firebaseManager.updateCollectionInfo(key, collection);
    }

    public void deleteCollection(final String key) {
        requestCollection(key, new RequestCallback<Collection>() {
            @Override
            public void onSuccess(Collection collection) {
                for(String itemKey : collection.getItems().keySet()) {
                    firebaseManager.deleteCollectionItem(itemKey, key);
                }

                firebaseManager.deleteCollection(key);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public void switchFollowingCollection(String collectionKey, String ownerKey, boolean add) {
        firebaseManager.switchFollowing(collectionKey, ownerKey, add);
    }

    public void switchLikeReaction(String reactionKey, boolean add) {
        firebaseManager.switchLike(reactionKey, add);
    }

    public void addView(String collectionKey) {
        firebaseManager.addView(collectionKey);
    }

    private Observable<Collection> getCollection(final String key) {
        return Observable.create(new ObservableOnSubscribe<Collection>() {
            @Override
            public void subscribe(final ObservableEmitter<Collection> subscriber) throws Exception {
                firebaseManager.requestCollection(mContext, key, new RequestCallback<Collection>() {
                    @Override
                    public void onSuccess(Collection collection) {
                        subscriber.onNext(collection);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    private Observable<List<Pair<String, Collection>>> getCollectionsList(final String userKey, final int filter, final String query) {
        return Observable.create(new ObservableOnSubscribe<List<Pair<String, Collection>>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<Pair<String, Collection>>> subscriber) throws Exception {
                firebaseManager.getCollectionsList(mContext, userKey, filter, query, new RequestCallback<List<Pair<String, Collection>>>() {
                    @Override
                    public void onSuccess(List<Pair<String, Collection>> collections) {
                        subscriber.onNext(collections);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    public void updateCollectionPhoto(String key, Uri photo,
                                @Nullable OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                                @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                                @Nullable OnFailureListener failureListener) {
        firebaseManager.updateCollectionPhoto(mContext, key, photo, successListener, progressListener, failureListener);
    }

    public void deletePhoto(String path) {
        firebaseManager.deletePhoto(mContext, path);
    }

    public String getNewCollectionItemKey() {
        return firebaseManager.getNewCollectionItemKey();
    }

    public void createCollectionItem(String key, CollectionItem collectionItem) {
        firebaseManager.createCollectionItem(key, collectionItem);
    }

    public void updateCollectionItem(String key, CollectionItem collectionItem) {
        firebaseManager.updateCollectionItemInfo(key, collectionItem);
    }

    public void deleteCollectionItem(String itemKey, String collectionKey) {
        firebaseManager.deleteCollectionItem(itemKey, collectionKey);
    }

    public void createReaction(String key, Reaction reaction) {
        firebaseManager.createReaction(key, reaction);
    }

    public String getNewReactionKey() {
        return firebaseManager.getNewReactionKey();
    }

    public void updateReaction(String key, Reaction reaction) {
        firebaseManager.updateReactionInfo(key, reaction);
    }

    public void deleteReaction(String reactionKey, String collectionItemKey) {
        firebaseManager.deleteReaction(reactionKey, collectionItemKey);
    }

    public void createReport(Report report) {
        firebaseManager.createReport(report);
    }

    public void changeReportStatus(String reportKey, int status, String collectionKey) {
        firebaseManager.changeReportStatus(reportKey, status, collectionKey);
    }

    private Observable<CollectionItem> getCollectionItem(final String key) {
        return Observable.create(new ObservableOnSubscribe<CollectionItem>() {
            @Override
            public void subscribe(final ObservableEmitter<CollectionItem> subscriber) throws Exception {
                firebaseManager.requestCollectionItem(mContext, key, new RequestCallback<CollectionItem>() {
                    @Override
                    public void onSuccess(CollectionItem collectionItem) {
                        subscriber.onNext(collectionItem);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    private Observable<List<String>> getCollectionItemPhotos(final String collectionKey) {
        return Observable.create(new ObservableOnSubscribe<List<String>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<String>> subscriber) throws Exception {
                firebaseManager.getCollectionItemPhotos(mContext, collectionKey, new RequestCallback<List<String>>() {
                    @Override
                    public void onSuccess(List<String> photos) {
                        subscriber.onNext(photos);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    private Observable<List<Pair<String, CollectionItem>>> getCollectionItemsList(final String collectionKey) {
        return Observable.create(new ObservableOnSubscribe<List<Pair<String, CollectionItem>>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<Pair<String, CollectionItem>>> subscriber) throws Exception {
                firebaseManager.getCollectionItemsList(mContext, collectionKey, new RequestCallback<List<Pair<String, CollectionItem>>>() {
                    @Override
                    public void onSuccess(List<Pair<String, CollectionItem>> collectionItems) {
                        subscriber.onNext(collectionItems);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    private Observable<List<Pair<String, Reaction>>> getReactionsList(final String collectionItemKey) {
        return Observable.create(new ObservableOnSubscribe<List<Pair<String, Reaction>>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<Pair<String, Reaction>>> subscriber) throws Exception {
                firebaseManager.getItemReactionsList(mContext, collectionItemKey, new RequestCallback<List<Pair<String, Reaction>>>() {
                    @Override
                    public void onSuccess(List<Pair<String, Reaction>> reactions) {
                        subscriber.onNext(reactions);
//                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    private Observable<List<Pair<String, User>>> getFollowersList(final String collectionKey) {
        return Observable.create(new ObservableOnSubscribe<List<Pair<String, User>>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<Pair<String, User>>> subscriber) throws Exception {
                firebaseManager.getFollowersList(mContext, collectionKey, new RequestCallback<List<Pair<String, User>>>() {
                    @Override
                    public void onSuccess(List<Pair<String, User>> users) {
                        subscriber.onNext(users);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    private Observable<List<AdItem>> getFeaturedList() {
        return Observable.create(new ObservableOnSubscribe<List<AdItem>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<AdItem>> subscriber) throws Exception {
                firebaseManager.getFeaturedCollectionsList(mContext, new RequestCallback<List<AdItem>>() {
                    @Override
                    public void onSuccess(List<AdItem> ads) {
                        subscriber.onNext(ads);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    private Observable<List<Pair<String, Report>>> getReportsList() {
        return Observable.create(new ObservableOnSubscribe<List<Pair<String, Report>>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<Pair<String, Report>>> subscriber) throws Exception {
                firebaseManager.getReportsList(mContext, new RequestCallback<List<Pair<String, Report>>>() {
                    @Override
                    public void onSuccess(List<Pair<String, Report>> reports) {
                        subscriber.onNext(reports);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    private Observable<Report> getReport(final String key) {
        return Observable.create(new ObservableOnSubscribe<Report>() {
            @Override
            public void subscribe(final ObservableEmitter<Report> subscriber) throws Exception {
                firebaseManager.requestReport(mContext, key, new RequestCallback<Report>() {
                    @Override
                    public void onSuccess(Report report) {
                        subscriber.onNext(report);
                        subscriber.onComplete();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        });
    }

    public void updateCollectionItemPhoto(String key, Uri photo,
                                      @Nullable OnSuccessListener<UploadTask.TaskSnapshot> successListener,
                                      @Nullable OnProgressListener<UploadTask.TaskSnapshot> progressListener,
                                      @Nullable OnFailureListener failureListener) {
        firebaseManager.updateCollectionItemPhoto(mContext, key, photo, successListener, progressListener, failureListener);
    }

    private <T> Observer<T> createObserver(final RequestCallback<T> request) {
        return new Observer<T>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                subscriptions.add(d);
            }

            @Override
            public void onNext(@NonNull T object) { request.onSuccess(object); }

            @Override
            public void onError(@NonNull Throwable e) { request.onError(e.getMessage()); }

            @Override
            public void onComplete() {}
        };
    }

}
