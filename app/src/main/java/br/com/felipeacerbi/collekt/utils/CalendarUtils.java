package br.com.felipeacerbi.collekt.utils;

import android.content.Context;
import android.text.format.DateFormat;

import java.util.Calendar;

import br.com.felipeacerbi.collekt.R;

public class CalendarUtils {

    public static String convertDate(long timeInMillis) {
        return DateFormat.format("MMM dd, yyyy", timeInMillis).toString();
    }

    public static String convertDateTime(Context context, long timeInMillis) {
        Calendar currentTime = Calendar.getInstance();
        Calendar reqTime = Calendar.getInstance();
        reqTime.setTimeInMillis(timeInMillis);

        if(reqTime.get(Calendar.YEAR) != currentTime.get(Calendar.YEAR)) {
            return DateFormat.format("MMM dd, yyyy 'at' h:mm a", reqTime).toString();
        }

        if(reqTime.get(Calendar.DAY_OF_MONTH) != currentTime.get(Calendar.DAY_OF_MONTH)) {
            return DateFormat.format("MMM dd 'at' h:mm a", reqTime).toString();
        }

        if(reqTime.get(Calendar.HOUR_OF_DAY) != currentTime.get(Calendar.HOUR_OF_DAY)) {
            int hoursPassed = currentTime.get(Calendar.HOUR_OF_DAY) - reqTime.get(Calendar.HOUR_OF_DAY);
            String suffix = (hoursPassed == 1) ? " h" : " hrs";
            return hoursPassed + suffix;
        }

        if(reqTime.get(Calendar.MINUTE) != currentTime.get(Calendar.MINUTE)) {
            int minsPassed = currentTime.get(Calendar.MINUTE) - reqTime.get(Calendar.MINUTE);
            String suffix = (minsPassed == 1) ? " min" : " mins";
            return minsPassed  + suffix;
        }

        return context.getString(R.string.just_now_time);
    }
}
