package br.com.felipeacerbi.collekt.utils;

public interface CollectionCreatorHelper {
    void setName(String name);
    void setCoverPhoto(String photoPath);
    String getCoverPhoto();
    void setIsShared(Boolean shared);
    void createCollection();
    String getKey();
    void next();
    void close();
}
