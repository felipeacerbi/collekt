package br.com.felipeacerbi.collekt.utils;

public interface CollectionItemCreatorHelper {
    void setName(String name);
    void setDescription(String description);
    void setOrigin(String origin);
    void setValue(int value);
    void setRarity(int rarity);
    void addPhoto(String photo, int place);
    void setMissing(boolean missing);
    String getPhoto(int position);
    void createCollectionItem();
    String getKey();
    void next();
    void close();
}
