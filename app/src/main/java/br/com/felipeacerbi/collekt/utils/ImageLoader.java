package br.com.felipeacerbi.collekt.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;

import br.com.felipeacerbi.collekt.R;

public class ImageLoader {

    private static boolean isValidContextForGlide(final Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            if (activity.isDestroyed() || activity.isFinishing()) {
                return false;
            }
        }
        return true;
    }

    public static void load(final Context context, String photoPath, int errorImage, ImageView intoView) {
        if(isValidContextForGlide(context)) {
            Glide.with(context)
                    .load(photoPath)
                    .apply(RequestOptions.errorOf(errorImage))
                    .transition(GenericTransitionOptions.with(R.anim.glide_item_fade))
                    .into(intoView);
        }
    }

    public static void loadNoTransition(final Context context, String photoPath, int errorImage, ImageView intoView) {
        if(isValidContextForGlide(context)) {
            Glide.with(context)
                    .load(photoPath)
                    .apply(RequestOptions.errorOf(errorImage))
                    .into(intoView);
        }
    }

    public static void load(Context context, Uri photoPath, int errorImage, ImageView intoView) {
        if(isValidContextForGlide(context)) {
            Glide.with(context)
                    .load(photoPath)
                    .apply(RequestOptions.errorOf(errorImage))
                    .into(intoView);
        }
    }

    public static RequestBuilder<Bitmap> loadBuilder(Context context, String photoPath, int errorImage) {
        if(isValidContextForGlide(context)) {
            return Glide.with(context)
                    .asBitmap()
                    .load(photoPath)
                    .apply(RequestOptions.errorOf(errorImage));
        } else {
            return null;
        }
    }
}
