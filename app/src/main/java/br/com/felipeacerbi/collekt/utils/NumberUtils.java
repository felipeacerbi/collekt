package br.com.felipeacerbi.collekt.utils;

import java.util.Locale;

public class NumberUtils {

    public static String formatNumber(int number) {
        return formatNumber((long) number);
    }

    public static String formatNumber(long number) {
        String result = String.valueOf(number);

        if(number > 999999) {
            result = "999k+";
        } else if(number > 999) {
            boolean hasDecimal = (float) number % 1000 > 0.5;
            result = String.format(Locale.getDefault(), hasDecimal ? "%.1fk" : "%.0fk", (float) number / 1000);
        }

        return result;
    }

}
