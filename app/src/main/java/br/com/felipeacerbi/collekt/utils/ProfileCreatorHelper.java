package br.com.felipeacerbi.collekt.utils;

import br.com.felipeacerbi.collekt.models.User;

public interface ProfileCreatorHelper {
    void setUserName(String name);
    void setUserDescription(String description);
    void setUserPhoto(String photoPath);
    String getUserName();
    String getUserPhoto();
    void createUser(User user);
    void next();
    void close();
}
