package br.com.felipeacerbi.collekt.utils;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.felipeacerbi.collekt.R;

public class SharedPrefsUtils {

    private static final String APP_PREFERENCES = "app_preferences";
    private static final String COLLECTION_VIEW_MODE = "collection_view_mode";
    private static final String COLLECTION_ITEM_VIEW_MODE = "collection_item_view_mode";
    private static final String COLLECTION_ITEM_SPAM_COUNT = "collection_item_spam_count";
    private static final String SEARCH_VIEW_MODE = "search_view_mode";
    private static final String COLLECTION_ID_PREFERENCE = "collection_id_preference";
    private static final String COMPLETE_MODE_NEW_FEATURE = "complete_mode_new_feature";
    private static final String INVALID_COLLECTION_ID = "-1";

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static boolean getCompleteModeNewFeature(Context context) {
        if(context != null) {
            return getPreferences(context).getBoolean(COMPLETE_MODE_NEW_FEATURE, true);
        }

        return false;
    }

    public static void setCompleteModeNewFeature(Context context) {
        if(context != null) {
            getPreferences(context).edit()
                    .putBoolean(COMPLETE_MODE_NEW_FEATURE, false)
                    .apply();
        }
    }

    public static int getCollectionViewMode(Context context) {
        if(context != null) {
            return getPreferences(context).getInt(COLLECTION_VIEW_MODE, 1);
        }

        return 1;
    }

    public static void setCollectionViewMode(Context context, int viewMode) {
        if(context != null) {
            getPreferences(context).edit()
                    .putInt(COLLECTION_VIEW_MODE, viewMode)
                    .apply();
        }
    }

    public static int getCollectionItemViewMode(Context context) {
        if(context != null) {
            return getPreferences(context).getInt(COLLECTION_ITEM_VIEW_MODE, 1);
        }

        return 1;
    }

    public static void setCollectionItemViewMode(Context context, int viewMode) {
        if(context != null) {
            getPreferences(context).edit()
                    .putInt(COLLECTION_ITEM_VIEW_MODE, viewMode)
                    .apply();
        }
    }

    public static int getCollectionItemSpamCount(Context context) {
        if(context != null) {
            return getPreferences(context).getInt(COLLECTION_ITEM_SPAM_COUNT, context.getResources().getInteger(R.integer.grid_list_spam_count));
        }

        return 1;
    }

    public static void setCollectionItemSpamCount(Context context, int spamCount) {
        if(context != null) {
            getPreferences(context).edit()
                    .putInt(COLLECTION_ITEM_SPAM_COUNT, spamCount)
                    .apply();
        }
    }

    public static int getSearchViewMode(Context context) {
        if(context != null) {
            return getPreferences(context).getInt(SEARCH_VIEW_MODE, 1);
        }

        return 1;
    }

    public static void setSearchViewMode(Context context, int viewMode) {
        if(context != null) {
            getPreferences(context).edit()
                    .putInt(SEARCH_VIEW_MODE, viewMode)
                    .apply();
        }
    }

    public static void storeCollectionId(Context context, int appWidgetId, String collectionId) {
        if(context != null) {
            getPreferences(context)
                    .edit()
                    .putString(COLLECTION_ID_PREFERENCE + appWidgetId, collectionId)
                    .apply();
        }
    }

    public static String loadCollectionId(Context context, int appWidgetId) {
        if(context != null) {
            return getPreferences(context)
                    .getString(
                            COLLECTION_ID_PREFERENCE + appWidgetId,
                            INVALID_COLLECTION_ID);
        }

        return INVALID_COLLECTION_ID;
    }

    public static void removeCollectionId(Context context, int appWidgetId) {
        if(context != null) {
            getPreferences(context)
                    .edit()
                    .remove(COLLECTION_ID_PREFERENCE + appWidgetId)
                    .apply();
        }
    }
}
