package br.com.felipeacerbi.collekt.views;

import android.view.GestureDetector;
import android.view.MotionEvent;

public abstract class FlipGestureListener implements GestureDetector.OnGestureListener {

    public abstract boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY, int position);

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return true;
    }
}
