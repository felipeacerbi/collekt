package br.com.felipeacerbi.collekt.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

import br.com.felipeacerbi.collekt.R;

public class StaticViewPager extends ViewPager {

    private boolean swipeable;

    public StaticViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.StaticViewPager);
        try {
            swipeable = typedArray.getBoolean(R.styleable.StaticViewPager_swipeable, true);
        } finally {
            typedArray.recycle();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.swipeable && super.onTouchEvent(event);

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.swipeable && super.onInterceptTouchEvent(event);

    }

    public void setSwipeable(boolean swipeable) {
        this.swipeable = swipeable;
    }
}
