package br.com.felipeacerbi.collekt.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import java.util.Locale;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.activities.CollectionActivity;
import br.com.felipeacerbi.collekt.activities.CollectionItemActivity;
import br.com.felipeacerbi.collekt.activities.FullscreenActivity;
import br.com.felipeacerbi.collekt.activities.NewCollectionItemActivity;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.Collection;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.NumberUtils;
import br.com.felipeacerbi.collekt.utils.SharedPrefsUtils;

import static br.com.felipeacerbi.collekt.activities.NewCollectionItemActivity.COLLECTION_EXTRA;

public class CollectionWidget extends AppWidgetProvider {

    public static final String SHOW_ITEM_ACTION = "br.com.felipeacerbi.collekt.SHOW_ITEM_ACTION";
    public static final String ITEM_EXTRA = "item";
    public static final String ITEM_EXTRA_GALLERY = "item_gallery";

    public static void updateWidgets(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        ComponentName thisWidget = new ComponentName(context, CollectionWidget.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.stack_view);

        for(int widgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, widgetId);
        }
    }

    // Called when the BroadcastReceiver receives an Intent broadcast.
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null && intent.getAction().equals(SHOW_ITEM_ACTION)) {
            String itemKey = intent.getStringExtra(ITEM_EXTRA);
            boolean isGallery = intent.getBooleanExtra(ITEM_EXTRA_GALLERY, false);

            if(isGallery) {
                Intent showGalleryIntent = new Intent(context, FullscreenActivity.class);
                showGalleryIntent.putExtra(FullscreenActivity.FULLSCREEN_PHOTO_KEY, itemKey);
                showGalleryIntent.putExtra(FullscreenActivity.FULLSCREEN_TYPE, FullscreenActivity.TYPE_COLLECTION_ITEM);
                context.startActivity(showGalleryIntent);
            } else {
                Intent showItemIntent = new Intent(context, CollectionItemActivity.class);
                showItemIntent.putExtra(CollectionItemActivity.COLLECTION_ITEM_EXTRA, itemKey);
                context.startActivity(showItemIntent);
            }

        }
        super.onReceive(context, intent);
    }


    public static void updateAppWidget(final Context context, final AppWidgetManager appWidgetManager,
                                       final int appWidgetId) {

        DataManager dataManager = new DataManager(context);
        final String collectionKey = SharedPrefsUtils.loadCollectionId(context, appWidgetId);

        dataManager.requestCollection(collectionKey, new RequestCallback<Collection>() {
            @Override
            public void onSuccess(Collection collection) {
                if(collection != null) {
                    // Set up the intent that starts the StackViewService, which will
                    // provide the views for this collection.
                    Intent intent = new Intent(context, StackWidgetService.class);
                    // Add the app widget ID to the intent extras.
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
                    intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

                    // Instantiate the RemoteViews object for the app widget layout.
                    RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.collection_widget);

                    // Set up the RemoteViews object to use a RemoteViews adapter.
                    // This adapter connects
                    // to a RemoteViewsService  through the specified intent.
                    // This is how you populate the data.
                    rv.setRemoteAdapter(R.id.stack_view, intent);
                    // The empty view is displayed when the collection has no items.
                    // It should be in the same layout used to instantiate the RemoteViews
                    // object above.
                    rv.setEmptyView(R.id.stack_view, R.id.empty_view);

                    rv.setTextViewText(R.id.tv_title, collection.getName());
                    rv.setTextViewText(R.id.tv_items_number, String.format(Locale.getDefault(), collection.getItems().size() == 1 ? context.getString(R.string.collection_items_number_singular) : context.getString(R.string.collection_items_number_plural), NumberUtils.formatNumber(collection.getItems().size()), ""));
                    if(collection.isPublic()) {
                        rv.setTextViewText(R.id.tv_followers, String.format(Locale.getDefault(), collection.getFollowers().size() == 1 ? context.getString(R.string.collection_followers_singular) : context.getString(R.string.collection_followers_plural), NumberUtils.formatNumber(collection.getFollowers().size())));
                    } else {
                        rv.setTextViewText(R.id.tv_followers, context.getResources().getString(R.string.private_collection_title));
                    }

                    Intent showCollectionIntent = new Intent(context, CollectionActivity.class);
                    showCollectionIntent.putExtra(COLLECTION_EXTRA, collectionKey);
                    PendingIntent showCollectionPendingIntent = PendingIntent.getActivity(
                            context,
                            appWidgetId,
                            showCollectionIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);

                    Intent newItemIntent = new Intent(context, NewCollectionItemActivity.class);
                    newItemIntent.putExtra(COLLECTION_EXTRA, collectionKey);
                    PendingIntent newItemPendingIntent = PendingIntent.getActivity(
                            context,
                            appWidgetId,
                            newItemIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);

                    Intent showItemIntent = new Intent(context, CollectionWidget.class);
                    showItemIntent.setAction(SHOW_ITEM_ACTION);
                    showItemIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
                    PendingIntent showItemPendingIntent = PendingIntent.getBroadcast(
                            context,
                            1,
                            showItemIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);

                    rv.setOnClickPendingIntent(R.id.iv_add_item, newItemPendingIntent);
                    rv.setOnClickPendingIntent(R.id.rl_title_bar, showCollectionPendingIntent);
                    rv.setOnClickPendingIntent(R.id.empty_view, showCollectionPendingIntent);
                    rv.setPendingIntentTemplate(R.id.stack_view, showItemPendingIntent);

                    // Instruct the widget manager to update the widget
                    appWidgetManager.updateAppWidget(appWidgetId, rv);
                }
            }

            @Override
            public void onError(String error) {

            }
        });

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            SharedPrefsUtils.removeCollectionId(context, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}
