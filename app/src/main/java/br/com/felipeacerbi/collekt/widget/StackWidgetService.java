package br.com.felipeacerbi.collekt.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import android.widget.Toast;

import androidx.core.util.Pair;

import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.FutureTarget;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import br.com.felipeacerbi.collekt.R;
import br.com.felipeacerbi.collekt.listeners.RequestCallback;
import br.com.felipeacerbi.collekt.models.CollectionItem;
import br.com.felipeacerbi.collekt.repository.DataManager;
import br.com.felipeacerbi.collekt.utils.ImageLoader;
import br.com.felipeacerbi.collekt.utils.SharedPrefsUtils;

public class StackWidgetService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new StackRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}

class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context mContext;
    private String mCollectionId;
    private List<Pair<String, CollectionItem>> mItems;
    private DataManager dataManager;
    private final int mAppWidgetId;

    private boolean sync = true;

    StackRemoteViewsFactory(Context mContext, Intent intent) {
        this.mContext = mContext;
        mAppWidgetId = intent.getIntExtra(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        mCollectionId = SharedPrefsUtils.loadCollectionId(mContext, mAppWidgetId);
    }

    private void fetchData() {
        dataManager = new DataManager(mContext);
        dataManager.requestCollectionItemsList(mCollectionId, new RequestCallback<List<Pair<String, CollectionItem>>>() {
            @Override
            public void onSuccess(List<Pair<String, CollectionItem>> items) {
                mItems = items;
                sync = false;
                AppWidgetManager.getInstance(mContext).notifyAppWidgetViewDataChanged(mAppWidgetId, R.id.stack_view);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(mContext, mContext.getString(R.string.collection_items_load_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCreate() {
        // No need
    }

    @Override
    public void onDataSetChanged() {
        if(sync) {
            fetchData();
        } else {
            sync = true;
        }
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        if(mItems == null) return 0;
        return mItems.size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_collection_item);

        if(mItems == null) {
            fetchData();
        } else {
            CollectionItem item = mItems.get(i).second;

            if (item != null) {

                RequestBuilder<Bitmap> requestBuilder = ImageLoader.loadBuilder(
                        mContext,
                        item.getCoverPhoto(),
                        R.drawable.collection_placeholder);

                rv.setTextViewText(R.id.tv_item_title, item.getName());
                rv.setTextViewText(R.id.tv_item_comments, String.format(Locale.getDefault(), "%d", item.getReactions().size()));

                try {
                    if(requestBuilder != null) {
                        FutureTarget<Bitmap> futureTarget = requestBuilder.submit();
//                        Single<Bitmap> bitmapSingle = Single.fromFuture(futureTarget);
//
//                        bitmapSingle
//                                .subscribeOn(Schedulers.io())
//                                .observeOn(AndroidSchedulers.mainThread())
//                                .subscribe(new Consumer<Bitmap>() {
//                                    @Override
//                                    public void accept(Bitmap bitmap) {
//                                        rv.setImageViewBitmap(R.id.iv_item_cover, bitmap);
//                                        rv.apply(mContext, mContext.getApplicationContext().conte);
//                                    }
//                                });
//
                        rv.setImageViewBitmap(R.id.iv_item_cover, futureTarget.get());
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }

                // Next, set a fill-intent, which will be used to fill in the pending intent template
                // that is set on the collection view in StackWidgetProvider.
                Bundle extras = new Bundle();
                extras.putString(CollectionWidget.ITEM_EXTRA, mItems.get(i).first);

                Intent itemFillInIntent = new Intent();
                itemFillInIntent.putExtras(extras);

                Bundle extras2 = new Bundle();
                extras2.putString(CollectionWidget.ITEM_EXTRA, mItems.get(i).first);
                extras2.putBoolean(CollectionWidget.ITEM_EXTRA_GALLERY, true);

                Intent galleryFillInIntent = new Intent();
                galleryFillInIntent.putExtras(extras2);
                // Make it possible to distinguish the individual on-click
                // action of a given item
                rv.setOnClickFillInIntent(R.id.iv_item_cover, itemFillInIntent);
                rv.setOnClickFillInIntent(R.id.tv_item_title, itemFillInIntent);
                rv.setOnClickFillInIntent(R.id.iv_gallery_button, galleryFillInIntent);
            }

        }

        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}