const functions = require('firebase-functions');
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);

function createRemoveFollowingPromise(userKey, collectionKey) {
	var userFollowingRef = admin.database().ref('users/' + userKey + "/following/" + collectionKey);
	var removeFollowing = userFollowingRef.set(null);

	return Promise.all([removeFollowing]);
}

function createRemoveFollowersPromise(ownerKey, followers) {
	var userFollowersRef = admin.database().ref('users/' + ownerKey + "/followers");
	var userFollowersPromise = userFollowersRef.once('value');
	
	return Promise.all([userFollowersPromise]).then(
		results => {
			const followersSnapshot = results[0];
			
			var promises = [];
			
			var currentNumber = followersSnapshot.val();
			var updatedNumber = currentNumber - followers;

			var removeFollowersPromise = userFollowersRef.set(updatedNumber);
			
			promises[promises.length] = removeFollowersPromise;
			
			return Promise.all(promises);
		})
}

function sendMessagePromise(ownerId, payload) {
	const getOwner = admin.database().ref('users/' + ownerId).once('value');
	
	return Promise.all([getOwner]).then(
		results => {
			const ownerSnapshot = results[0];
			
			var promises = [];
			
			var idToken = ownerSnapshot.child("idToken").val();
	
			console.log("Sending message to " + idToken + "...");
			sendPromise = admin.messaging().sendToDevice(idToken, payload).then(
				response => {
					console.log("Successfully sent message:", response);
					return Promise.all([]);
				}).catch(
					error => {
					console.log("Error sending message:", error);
				});
				
			promises[promises.length] = sendPromise;
			
			return Promise.all(promises);
		})
}

exports.AddAndNotifyNewFollower =
	functions.database.ref('collections/{collectionId}/followers/{followerId}')
	.onWrite(event => {
		if (!event.data.val()) {
            console.log("not a valid event");
            return;
        }
		
		var collectionId = event.params.collectionId;
		var followerId = event.params.followerId;
		
		const getCollection = admin.database().ref('collections/' + collectionId).once('value');
		const getFollower = admin.database().ref('users/' + followerId).once('value');
		
		return Promise.all([getCollection, getFollower]).then(
			results => {
				const collectionSnapshot = results[0];
				const followerSnapshot = results[1];
				
				var promises = [];
				
				var collectionName = collectionSnapshot.child("name").val();
				var ownerId = collectionSnapshot.child("owner").val();
				var followerName = followerSnapshot.child("name").val();
				
				console.log("New follow", followerName + " is following " + collectionName);
				
				var payload = {
					data: {
						collectionId: collectionId,
						collection: collectionName,
						follower: followerName,
						type: "follow"
					}
				};
				
				promises[promises.length] = sendMessagePromise(ownerId, payload);
					
				return Promise.all(promises);
			})
	});
	
exports.NotifyNewReaction =
	functions.database.ref('items/{itemId}/reactions/{reactionId}')
	.onWrite(event => {
		if (!event.data.val()) {
            console.log("not a valid event");
            return;
        }
		
		var itemId = event.params.itemId;
		var reactionId = event.params.reactionId;
		
		const getItem = admin.database().ref('items/' + itemId).once('value');
		const getReaction = admin.database().ref('reactions/' + reactionId).once('value');
		
		return Promise.all([getItem, getReaction]).then(
			results => {
				const itemSnapshot = results[0];
				const reactionSnapshot = results[1];
				
				var promises = [];
				
				var itemName = itemSnapshot.child("name").val();
				var ownerId = itemSnapshot.child("owner").val();
				var reacterId = reactionSnapshot.child("reacter").val();
				
				if(ownerId === reacterId) {
					return;
				}
				
				const getUser = admin.database().ref('users/' + reacterId).once('value');
				
				return Promise.all([getUser]).then(
					results => {
						const userSnapshot = results[0];
				
						var promises = [];
						
						var userName = userSnapshot.child("name").val();
				
						console.log("New reaction", userName + " reacted to " + itemName);
				
						var payload = {
							data: {
								itemId: itemId,
								item: itemName,
								reacter: userName,
								type: "reaction"
							}
						};
				
						promises[promises.length] = sendMessagePromise(ownerId, payload);
					
						return Promise.all(promises);
					})
			})
	});

exports.DeleteCollection =
	functions.database.ref('collections/{collectionId}')
	.onWrite(event => {
		if (event.data.val()) {
            console.log("not a valid event");
            return;
        }
		
		var collectionId = event.params.collectionId;
		
		const getCollection = admin.database().ref('collections/' + collectionId).once('value');
		
		return Promise.all([getCollection]).then(
			results => {
				const collectionSnapshot = results[0];
				
				var promises = [];
				
				var followers = collectionSnapshot.child("followers");
				var followNumber = followers.numChildren();
				
				console.log("Delete collection", "Follow number: " + followNumber);
				
				followers.forEach (
					followerSnapshot => {
						promises[promises.length] = createRemoveFollowingPromise(followerSnapshot.key, collectionId);
					})
				
				promises[promises.length] = createRemoveFollowersPromise(collectionSnapshot.child("owner").val(), followNumber);
					
				return Promise.all(promises);
			})
	});